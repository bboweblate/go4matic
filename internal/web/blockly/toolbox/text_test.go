// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

import "testing"

func TestText(t *testing.T) {
	equalXML(t, `<block type="text">
  <field name="TEXT"></field>
</block>`, Text())
}

func TestTextJoin(t *testing.T) {
	equalXML(t, `<block type="text_join">
  <mutation items="2"></mutation>
</block>`, TextJoin())
}

func TestTextAppend(t *testing.T) {
	equalXML(t, `<block type="text_append">
  <field name="VAR">item</field>
  <value name="TEXT">
    <shadow type="text">
      <field name="TEXT"></field>
    </shadow>
  </value>
</block>`, TextAppend())
}

func TestTextLength(t *testing.T) {
	equalXML(t, `<block type="text_length">
  <value name="VALUE">
    <shadow type="text">
      <field name="TEXT"></field>
    </shadow>
  </value>
</block>`, TextLength())
}

func TestTextIsEmpty(t *testing.T) {
	equalXML(t, `<block type="text_isEmpty">
  <value name="VALUE">
    <shadow type="text">
      <field name="TEXT"></field>
    </shadow>
  </value>
</block>`, TextIsEmpty())
}

func TestTextIndexOf(t *testing.T) {
	equalXML(t, `<block type="text_indexOf">
  <field name="END">FIRST</field>
  <value name="VALUE">
    <block type="variables_get">
      <field name="VAR">text</field>
    </block>
  </value>
  <value name="FIND">
    <shadow type="text">
      <field name="TEXT"></field>
    </shadow>
  </value>
</block>`, TextIndexOf())
}

func TestTextCharAt(t *testing.T) {
	equalXML(t, `<block type="text_charAt">
  <mutation at="true"></mutation>
  <field name="WHERE">FROM_START</field>
  <value name="VALUE">
    <block type="variables_get">
      <field name="VAR">text</field>
    </block>
  </value>
</block>`, TextCharAt())
}

func TestTextGetSubstring(t *testing.T) {
	equalXML(t, `<block type="text_getSubstring">
  <mutation at1="true" at2="true"></mutation>
  <field name="WHERE1">FROM_START</field>
  <field name="WHERE2">FROM_START</field>
  <value name="STRING">
    <block type="variables_get">
      <field name="VAR">text</field>
    </block>
  </value>
</block>`, TextGetSubstring())
}

func TestTextChangeCase(t *testing.T) {
	equalXML(t, `<block type="text_changeCase">
  <field name="CASE">UPPERCASE</field>
  <value name="TEXT">
    <shadow type="text">
      <field name="TEXT"></field>
    </shadow>
  </value>
</block>`, TextChangeCase())
}

func TestTextTrim(t *testing.T) {
	equalXML(t, `<block type="text_trim">
  <field name="MODE">BOTH</field>
  <value name="TEXT">
    <shadow type="text">
      <field name="TEXT"></field>
    </shadow>
  </value>
</block>`, TextTrim())
}

func TestTextPrint(t *testing.T) {
	equalXML(t, `<block type="text_print">
  <value name="TEXT">
    <shadow type="text">
      <field name="TEXT"></field>
    </shadow>
  </value>
</block>`, TextPrint())
}

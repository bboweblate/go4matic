// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package lua

import (
	"sync/atomic"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/go4matic/go4matic/pkg/core"
)

func TestCreateCoreModule(t *testing.T) {
	bus := new(core.TestBus)
	module := CreateCoreModule(bus)
	defer close(module.eventBuffer)

	ass := assert.New(t)
	ass.Equal(bus, module.bus)
	ass.NotNil(module.eventBuffer)
	ass.NotNil(module.eventCallbacks)

	ass.Equal("core", module.Name())
	ass.Len(module.Def().Functions, 2)
}

func Test_handleEvents(t *testing.T) {
	bus := new(core.TestBus)

	module := CreateCoreModule(bus)
	defer close(module.eventBuffer)

	ch := make(chan func(), 1)
	module.registerCallbackChannel(nil, ch)

	ass := assert.New(t)

	var called int32
	callback := func([]interface{}) []interface{} {
		atomic.AddInt32(&called, 1)
		return nil
	}
	module.eventCallbacks = map[core.Script]map[string]func([]interface{}) []interface{}{
		nil: {
			"aaa": callback,
		},
	}
	module.eventBuffer <- core.Event{
		ID: "aaa",
	}

	f := <-ch
	f()

	time.Sleep(time.Millisecond * 100)
	ass.Equal(int32(1), atomic.LoadInt32(&called))

	ass.Contains(module.eventCallbacks, nil)
	module.cleanup(nil)
	ass.NotContains(module.eventCallbacks, nil)
}

func Test_registerListener(t *testing.T) {
	bus := new(core.TestBus)
	module := CreateCoreModule(bus)
	defer close(module.eventBuffer)

	ass := assert.New(t)

	called := false
	bus.RegisterEventListenerFunc = func(id string, listener chan core.Event) error {
		ass.Equal("aaa", id)
		ass.NotNil(listener)
		called = true
		return nil
	}
	callback := func([]interface{}) []interface{} { return nil }
	ass.Nil(module.registerListener(nil, []interface{}{
		"aaa", callback,
	}))
	ass.NotNil(module.eventCallbacks[nil]["aaa"])
	ass.True(called)

	called = false
	callback2 := func([]interface{}) []interface{} { return nil }
	ass.Nil(module.registerListener(nil, []interface{}{
		"aaa", callback2,
	}))
	ass.NotNil(module.eventCallbacks[nil]["aaa"])
	ass.True(called)
}

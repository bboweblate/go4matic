// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package http

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	testifyHttp "github.com/stretchr/testify/http"

	"gitlab.com/go4matic/go4matic/internal/version"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

func TestApiSpec_handleIndex(t *testing.T) {
	gin.SetMode(gin.TestMode)
	writer := testifyHttp.TestResponseWriter{}
	ctx, _ := gin.CreateTestContext(&writer)

	spec := apiSpec{}
	spec.handleIndex(ctx)

	ass := assert.New(t)
	ass.Equal(http.StatusOK, writer.StatusCode)
	ass.NotEmpty(writer.Output)
}

func TestApiSpec_handleSpec(t *testing.T) {
	gin.SetMode(gin.TestMode)
	writer := testifyHttp.TestResponseWriter{}
	ctx, _ := gin.CreateTestContext(&writer)

	spec := apiSpec{&Handler{
		apiHandler: []core.PathEntry{},
	}}
	spec.handleSpec(ctx)

	json := fmt.Sprintf(
		`{"openapi": "3.0.0", "info": {"title": "GO Home API", "version": "%s"}, "paths": {}}`,
		version.Version)

	ass := assert.New(t)
	ass.Equal(http.StatusOK, writer.StatusCode)
	ass.JSONEq(json, writer.Output)
}

func Test_specEndpoints(t *testing.T) {
	ass := assert.New(t)

	data := specEndpoints(map[string][]core.PathEntry{
		"/:aaa/": {{
			Def: api.ExtendedHandler{
				Description: "test",
				Parameters: api.FieldMap{
					"aaa": {},
				},
			},
			Method: "GET",
			Group:  "111",
		}, {
			Def: api.ExtendedHandler{
				Parameters: api.FieldMap{
					"aaa": {},
				},
			},
			Method: "PUT",
		}},
		"/bbb/": {{
			Def: api.ExtendedHandler{
				Parameters: api.FieldMap{
					"aaa": {},
				},
			},
			Method: "GET",
			Group:  "222",
		}},
	})

	ass.Len(data, 2)
	ass.Contains(data, "/{aaa}/")
	ass.Contains(data, "/bbb/")

	aOperations := data["/{aaa}/"].(map[string]interface{})
	ass.Len(aOperations, 2)
	ass.Contains(aOperations, "get")
	ass.Contains(aOperations, "put")

	aGet := aOperations["get"].(map[string]interface{})
	ass.Contains(aGet, "summary")
	ass.Equal("test", aGet["summary"])
	ass.Contains(aGet, "tags")
	ass.Contains(aGet["tags"], "111")
	ass.Contains(aGet, "responses")
	ass.Contains(aGet, "parameters")
	ass.NotContains(aGet, "requestBody")

	bOperations := data["/bbb/"].(map[string]interface{})
	ass.Len(bOperations, 1)
	ass.Contains(bOperations, "get")

	bGet := bOperations["get"].(map[string]interface{})
	ass.Contains(bGet, "summary")
	ass.Equal("", bGet["summary"])
	ass.Contains(bGet, "tags")
	ass.Contains(bGet["tags"], "222")
	ass.Contains(bGet, "responses")
	ass.NotContains(bGet, "parameters")
	ass.Contains(bGet, "requestBody")
}

func Test_specParameter(t *testing.T) {
	ass := assert.New(t)

	keys, parameters, body := specParameter("/api/:aaa/:bbb/", api.FieldMap{
		"aaa": {
			Description: "test",
		},
		"bbb": {
			Type: api.Int,
		},
		"ccc": {
			Type: api.Int,
		},
	})

	ass.Contains(keys, "aaa")
	ass.Contains(keys, "bbb")
	ass.Len(parameters, 2)
	if parameters[0]["name"].(string) == "aaa" {
		ass.Equal("test", parameters[0]["description"])
		ass.Empty(parameters[0]["schema"])

		ass.Equal("", parameters[1]["description"])
		ass.NotEmpty(parameters[1]["schema"])
	} else {
		ass.Equal("", parameters[0]["description"])
		ass.NotEmpty(parameters[0]["schema"])

		ass.Equal("test", parameters[1]["description"])
		ass.Empty(parameters[1]["schema"])
	}
	ass.Contains(body, "required")
	ass.Contains(body, "content")

	keys, parameters, body = specParameter("/api/:aaa/", api.FieldMap{
		"aaa": {
			Description: "test",
		},
	})
	ass.Contains(keys, "aaa")
	ass.Len(parameters, 1)
	ass.Empty(body)
}

func Test_specResponses(t *testing.T) {
	ass := assert.New(t)

	data := specResponses(nil)
	ass.Equal(map[string]interface{}{
		"200": map[string]interface{}{
			"description": "OK",
		},
	}, data)

	data = specResponses(map[string]api.Response{
		"100": {
			Data: api.FieldMap{
				"test": {},
			},
		},
		"200": {
			Description: "aaa",
		},
	})

	ass.Len(data, 2)

	data1 := data["100"].(map[string]interface{})
	ass.Empty(data1["description"])
	ass.NotEmpty(data1["content"])

	data2 := data["200"].(map[string]interface{})
	ass.Equal("aaa", data2["description"])
	ass.NotContains(data2, "content")
}

func Test_specFieldMap(t *testing.T) {
	data := specFieldMap(api.FieldMap{
		"aaa": {
			Required:    true,
			Description: "test",
			Type:        api.Int,
		},
		"bbb": {
			Type: api.FieldMap{
				"ccc": {},
			},
		},
	})

	assert.Equal(t, map[string]interface{}{
		"type": "object",
		"properties": map[string]interface{}{
			"aaa": map[string]interface{}{
				"type":        "integer",
				"description": "test",
			},
			"bbb": map[string]interface{}{
				"type":        "object",
				"description": "",
				"properties": map[string]interface{}{
					"ccc": map[string]interface{}{
						"description": "",
					},
				},
			},
		},
		"required": []string{"aaa"},
	}, data)

	data = specFieldMap(api.FieldMap{
		"{aaa}": {
			Description: "test",
			Type:        api.Int,
		},
	})

	assert.Equal(t, map[string]interface{}{
		"type": "object",
		"additionalProperties": map[string]interface{}{
			"type":        "integer",
			"description": "test",
		},
	}, data)

	data = specFieldMap(api.FieldMap{
		"aaa": {
			Type: api.Map(api.String, api.Any),
		},
	})

	assert.Equal(t, map[string]interface{}{
		"type": "object",
		"properties": map[string]interface{}{
			"aaa": map[string]interface{}{
				"description":          "",
				"type":                 "object",
				"additionalProperties": map[string]interface{}{},
			},
		},
	}, data)
}

func Test_getEndpoints(t *testing.T) {
	endpoints := getEndpoints("", []core.PathEntry{{
		Path: "/aaa",
	}, {
		Path:   "/aaa",
		Group:  "Test",
		Method: "PUT",
	}, {
		Path: "/bbb",
		SubEntries: []core.PathEntry{{
			Path: "/ccc",
		}},
	}, {
		Path:    "/ddd",
		Handler: dummyHandler,
		SubEntries: []core.PathEntry{{
			Path: "/eee",
		}},
	}})

	ass := assert.New(t)
	ass.Len(endpoints, 4)
	ass.Len(endpoints["/aaa"], 2)
	ass.Len(endpoints["/bbb/ccc"], 1)
	ass.Len(endpoints["/ddd"], 1)
	ass.Len(endpoints["/ddd/eee"], 1)

	ass.Equal("GET", endpoints["/aaa"][0].Method)
	ass.Equal("Unknown", endpoints["/aaa"][0].Group)
	ass.Equal("PUT", endpoints["/aaa"][1].Method)
	ass.Equal("Test", endpoints["/aaa"][1].Group)
}

func Test_addEndpointToMap(t *testing.T) {
	data := make(map[string][]core.PathEntry)
	addEndpointToMap(data, "aaa",
		core.PathEntry{Path: "111"},
		core.PathEntry{Path: "222"})
	addEndpointToMap(data, "bbb",
		core.PathEntry{Path: "333"})

	ass := assert.New(t)
	ass.Len(data, 2)
	ass.Len(data["aaa"], 2)
	ass.Len(data["bbb"], 1)
	ass.Equal("111", data["aaa"][0].Path)
	ass.Equal("222", data["aaa"][1].Path)
	ass.Equal("333", data["bbb"][0].Path)
}

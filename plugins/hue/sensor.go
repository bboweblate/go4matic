// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hue

import (
	"strings"
	"sync"

	"github.com/amimof/huego"
	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

// Sensor instance
type Sensor struct {
	sensor huego.Sensor

	trigger func(action string, data map[string]interface{})

	mutex sync.RWMutex
}

// NewSensor create new sensor instance
func NewSensor(sensor huego.Sensor) *Sensor {
	return &Sensor{
		sensor: sensor,
	}
}

// ID of item
func (s *Sensor) ID() string {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	return "plugin.hue." + strings.Replace(s.sensor.UniqueID, ":", "_", -1)
}

// Name of item
func (s *Sensor) Name() string {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	return s.sensor.Name
}

// Events returns all events that can be emitted
func (s *Sensor) Events() map[string]api.SimpleHandler {
	return map[string]api.SimpleHandler{
		"button_event": {
			Description: assets.L.Get("Triggered if button pressed"),
			Parameters: map[string]api.Field{
				"type": {
					Description: assets.L.Get("Type of event"),
					Type:        api.String,
				},
				"updated": {
					Description: assets.L.Get("Time of event occur"),
					//Type: api.String,
				},
			},
		},
	}
}

// SetEventTrigger for item event
func (s *Sensor) SetEventTrigger(trigger func(action string, data map[string]interface{})) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.trigger = trigger
}

// update sensor state
func (s *Sensor) update(sensor huego.Sensor) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if s.sensor.State["lastupdated"] != sensor.State["lastupdated"] ||
		s.sensor.State["buttonevent"] != sensor.State["buttonevent"] {
		s.sensor.State = sensor.State

		s.trigger("button_event", map[string]interface{}{
			"type":    cast.ToInt(s.sensor.State["buttonevent"]),
			"updated": cast.ToTime(s.sensor.State["lastupdated"]),
		})
	}

	s.sensor.Name = sensor.Name
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mqtt

import (
	"sync"

	"gitlab.com/go4matic/go4matic/pkg/api"
)

// Value based on MQTT topic
type Value struct {
	broker string

	key         string
	topic       string
	setTopic    string
	description string
	valueType   api.Type

	value interface{}
	mutex sync.RWMutex

	p *Plugin
}

// getValue of value entry
func (v *Value) getValue() interface{} {
	v.mutex.RLock()
	defer v.mutex.RUnlock()

	return v.value
}

// update value
func (v *Value) update(value string) {
	v.mutex.Lock()
	defer v.mutex.Unlock()

	v.value = value
	go v.p.triggerItemChange(v.key, v.value)
}

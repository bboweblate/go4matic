// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package admin

import (
	"fmt"
	"time"

	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/internal/web/helper"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

func (w *Web) adminScripts(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/scripts")
	ctx.Set("title", "Scripts/Admin - Go4Matic")

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.scripts#list",
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["scripts"] = response.Data
}

func (w *Web) adminScriptsPost(ctx core.HTTPContext) {
	var request core.Request
	action := ctx.PostForm("action")
	switch action {
	case "add":
		request = core.Request{
			ID: "core.scripts#add",
			Parameters: map[string]interface{}{
				"id":     ctx.PostForm("id"),
				"script": "",
			},
		}
	case "remove":
		request = core.Request{
			ID: "core.scripts#remove",
			Parameters: map[string]interface{}{
				"id": ctx.PostForm("id"),
			},
		}
	default:
		helper.AddMessage(ctx, "danger",
			fmt.Sprintf("unknown action %s", action))
		return
	}

	response := w.Bus.HandleRequestWait(request, time.Second)
	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
	}
}

func (w *Web) adminScript(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/script")
	id := ctx.Param("id")
	ctx.Set("title", fmt.Sprintf("%s/Scripts/Admin - Go4Matic", id))

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.scripts#get",
		Parameters: map[string]interface{}{
			"id": id,
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["id"] = id
	vars["script"] = response.Data
}
func (w *Web) adminScriptPost(ctx core.HTTPContext) {
	id := ctx.Param("id")
	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.scripts#update",
		Parameters: map[string]interface{}{
			"id":              id,
			"script":          ctx.PostForm("script"),
			"enabled":         ctx.PostForm("enabled") != "",
			"blockly_enabled": ctx.PostForm("blockly_enabled") != "",
			"blockly_xml":     ctx.PostForm("blockly_xml"),
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}
}

func (w *Web) adminScriptToolboxXML(ctx core.HTTPContext) {
	ctx.XML(200, w.blocklyToolbox)
	ctx.Abort()
}

func (w *Web) adminScriptBlocksJs(ctx core.HTTPContext) {
	ctx.Header("Content-Type", "application/javascript; charset=utf-8")
	_, err := ctx.Writer().Write([]byte(w.blocklyJs))
	if err != nil {
		zap.L().Error(err.Error())
	}
	ctx.Abort()
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package service

import (
	"errors"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/go4matic/go4matic/pkg/core"
)

func repeat2Str(repeat int) string {
	list := make([]string, 0, 7)
	if repeat&(0x01) > 0 {
		list = append(list, "Sun")
	}
	if repeat&(0x02) > 0 {
		list = append(list, "Mon")
	}
	if repeat&(0x04) > 0 {
		list = append(list, "Tue")
	}
	if repeat&(0x08) > 0 {
		list = append(list, "Wed")
	}
	if repeat&(0x10) > 0 {
		list = append(list, "Thu")
	}
	if repeat&(0x20) > 0 {
		list = append(list, "Fri")
	}
	if repeat&(0x40) > 0 {
		list = append(list, "Sat")
	}

	return strings.Join(list, "-")
}

var daysToWaitData = []struct {
	today time.Weekday

	days map[int]int
}{
	{time.Sunday, map[int]int{
		Sunday:    0,
		Monday:    1,
		Tuesday:   2,
		Wednesday: 3,
		Thursday:  4,
		Friday:    5,
		Saturday:  6,
	}},
	{time.Monday, map[int]int{
		Sunday:    6,
		Monday:    0,
		Tuesday:   1,
		Wednesday: 2,
		Thursday:  3,
		Friday:    4,
		Saturday:  5,
	}},
	{time.Tuesday, map[int]int{
		Sunday:    5,
		Monday:    6,
		Tuesday:   0,
		Wednesday: 1,
		Thursday:  2,
		Friday:    3,
		Saturday:  4,
	}},
	{time.Wednesday, map[int]int{
		Sunday:    4,
		Monday:    5,
		Tuesday:   6,
		Wednesday: 0,
		Thursday:  1,
		Friday:    2,
		Saturday:  3,
	}},
	{time.Thursday, map[int]int{
		Sunday:    3,
		Monday:    4,
		Tuesday:   5,
		Wednesday: 6,
		Thursday:  0,
		Friday:    1,
		Saturday:  2,
	}},
	{time.Friday, map[int]int{
		Sunday:    2,
		Monday:    3,
		Tuesday:   4,
		Wednesday: 5,
		Thursday:  6,
		Friday:    0,
		Saturday:  1,
	}},
	{time.Saturday, map[int]int{
		Sunday:    1,
		Monday:    2,
		Tuesday:   3,
		Wednesday: 4,
		Thursday:  5,
		Friday:    6,
		Saturday:  0,
	}},
}

func TestTimeEntry_DaysToWait(t *testing.T) {
	for _, entry := range daysToWaitData {
		t.Run(entry.today.String(), func(st *testing.T) {
			for repeat, days := range entry.days {
				st.Run(repeat2Str(repeat), func(sst *testing.T) {
					ass := assert.New(sst)

					e := TimeEntry{
						Repeat: repeat,
					}
					ass.Equal(days, e.DaysToWait(entry.today))
				})
			}
		})
	}
}

func TestTimeEntry_DaysToWaitZero(t *testing.T) {
	entry := TimeEntry{}
	for i := time.Sunday; i <= time.Saturday; i++ {
		t.Run(i.String(), func(st *testing.T) {
			ass := assert.New(st)

			entry.Repeat = 0
			ass.Equal(0, entry.DaysToWait(i))

			entry.Repeat = Everyday
			ass.Equal(0, entry.DaysToWait(i))
		})
	}

}

// note: 2000-01-02 -> sunday
var nextTimeEntryData = []struct {
	name        string
	now         string
	entryRepeat int
	entryTime   int
	nextTime    string
}{
	{
		"next_now",
		"2000-01-02T00:00:00",
		0, 0,
		"2000-01-02T00:00:00",
	},
	{
		"next_today_12",
		"2000-01-02T00:00:00",
		0, 60 * 60 * 12,
		"2000-01-02T12:00:00",
	},
	{
		"next_day",
		"2000-01-02T00:00:00",
		Monday, 0,
		"2000-01-03T00:00:00",
	},
	{
		"next_day_12",
		"2000-01-02T00:00:00",
		Monday, 60 * 60 * 12,
		"2000-01-03T12:00:00",
	},
	{
		"next_day_midnight",
		"2000-01-02T12:00:00",
		0, 0,
		"2000-01-03T00:00:00",
	},
	{
		"next_week_midnight",
		"2000-01-02T12:00:00",
		Sunday, 0,
		"2000-01-09T00:00:00",
	},
	{
		"next_day_new_month",
		"2000-01-31T12:00:00",
		0, 0,
		"2000-02-01T00:00:00",
	},
}

func TestTimeEntry_Next(t *testing.T) {
	timeFormat := "2006-01-02T15:04:05"

	for _, entry := range nextTimeEntryData {
		t.Run(entry.name, func(st *testing.T) {
			ass := assert.New(st)

			now, err := time.Parse(timeFormat, entry.now)
			ass.NoError(err)

			e := TimeEntry{
				Active: true,
				Repeat: entry.entryRepeat,
				Time:   entry.entryTime,
			}
			ass.Equal(entry.nextTime, e.Next(now).Format(timeFormat))
		})
	}
}

func TestTimeEntry_NextInvalid(t *testing.T) {
	ass := assert.New(t)

	e := TimeEntry{}
	ass.Equal(int64(0), e.Next(time.Now()).Unix())
}

func TestTimer_setState(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	service := &TimerService{
		bus: bus,
	}
	timer := service.newTimer("", nil, nil)

	ass.EqualError(timer.setState("test", nil),
		"invalid item_value should be [ITEM_ID]#[VALUE] given test")

	// will fail because missing service
	ass.EqualError(timer.setState("test#aaa", nil),
		"no service for core.items")
}

func TestTimer_trigger(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	var eventID string
	var eventData map[string]interface{}

	service := new(TimerService)
	service.SetEventTrigger(func(id string, data map[string]interface{}) {
		eventID = id
		eventData = data
	})

	entry := &TimeEntry{
		Active: true,
		Repeat: 0,
		State:  "test",
	}

	timer := service.newTimer("aaa", nil, []string{"bbb"})
	timer.triggerEntries([]*TimeEntry{entry})

	ass.Equal("core.timers#trigger", eventID)
	ass.Equal(map[string]interface{}{
		"key":   "aaa",
		"state": "test",
	}, eventData)
	ass.False(entry.Active)
}

func TestTimer_StartStop(t *testing.T) {
	ass := assert.New(t)

	service := new(TimerService)
	timer := service.newTimer("", nil, nil)

	ass.False(timer.running)
	timer.Start()
	ass.True(timer.running)
	timer.Start()
	ass.True(timer.running)

	timer.Stop()
	ass.False(timer.running)
	timer.Stop()
	ass.False(timer.running)
}

func TestTimer_Entry(t *testing.T) {
	ass := assert.New(t)

	service := new(TimerService)
	service.SetEventTrigger(func(id string, data map[string]interface{}) {})

	timer := service.newTimer("", nil, nil)

	timer.Start()
	ass.True(timer.running)
	defer timer.Stop()

	timer.AddEntry(TimeEntry{})
	ass.Len(timer.ListEntries(), 1)

	timer.AddEntry(TimeEntry{
		Active: true,
		Time:   100,
	})
	ass.Len(timer.ListEntries(), 2)

	timer.AddEntry(TimeEntry{
		Time: 100,
	})
	ass.Len(timer.ListEntries(), 3)

	timer.RemoveEntry(1)
	ass.Len(timer.ListEntries(), 2)
}

func TestTimer_run_trigger(t *testing.T) {
	ass := assert.New(t)

	var triggerTime time.Time
	service := new(TimerService)
	mutex := sync.Mutex{}
	service.SetEventTrigger(func(id string, data map[string]interface{}) {
		mutex.Lock()
		defer mutex.Unlock()
		triggerTime = time.Now()
	})

	now := time.Now()
	timer := service.newTimer("", []*TimeEntry{
		{
			Active: true,
			Time:   (now.Hour()*60+now.Minute())*60 + now.Second() + 1,
		},
	}, nil)

	timer.Start()
	ass.True(timer.running)
	defer timer.Stop()

	time.Sleep(time.Second * 3)
	mutex.Lock()
	defer mutex.Unlock()
	ass.True(triggerTime.Sub(now) <= time.Second*2)
}

func TestTimer_ItemValues(t *testing.T) {
	ass := assert.New(t)

	service := new(TimerService)
	timer := service.newTimer("", nil, nil)

	timer.AddItemValue("aaa")
	ass.Equal([]string{"aaa"}, timer.itemValues)
	timer.AddItemValue("bbb")
	ass.Equal([]string{"aaa", "bbb"}, timer.itemValues)
	timer.AddItemValue("bbb")
	ass.Equal([]string{"aaa", "bbb"}, timer.itemValues)

	timer.RemoveItemValue("bbb")
	ass.Equal([]string{"aaa"}, timer.itemValues)
	timer.RemoveItemValue("bbb")
	ass.Equal([]string{"aaa"}, timer.itemValues)
}

func TestTimer_Dump(t *testing.T) {
	ass := assert.New(t)

	service := new(TimerService)
	timer := service.newTimer("", []*TimeEntry{
		{
			Time:   42,
			Active: true,
			State:  "test",
			Repeat: 2,
		},
	}, []string{"aaa", "bbb"})

	timer.Start()
	ass.True(timer.running)
	defer timer.Stop()

	ass.Equal(map[string]interface{}{
		"item_values": []string{"aaa", "bbb"},
		"times": []TimeEntry{
			{Active: true, Time: 42, Repeat: 2, State: "test"},
		},
	}, timer.Dump())
}

func TestTimerService_Key(t *testing.T) {
	ass := assert.New(t)
	service := new(TimerService)
	ass.Equal("core.timers", service.Key())
}

func TestTimerService_Events(t *testing.T) {
	ass := assert.New(t)
	service := new(TimerService)
	ass.Len(service.Events(), 2)
}

func TestTimerService_Handlers(t *testing.T) {
	ass := assert.New(t)
	service := new(TimerService)
	ass.Len(service.Handlers(), 9)
}

func TestTimerService_APIEndpoints(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	service := new(TimerService)
	ass.Len(service.APIEndpoints(bus), 1)
}

func TestTimerService_InitStartShutdown(t *testing.T) {
	ass := assert.New(t)

	bus := new(core.TestBus)

	err := errors.New("test")
	data := map[string]interface{}{}
	bus.HandleRequestWaitFunc = func(req core.Request, timeout time.Duration) core.Response {
		ass.Equal("core.config#get", req.ID)
		ass.Equal(map[string]interface{}{
			"key": "core.timers",
		}, req.Parameters)
		return core.Response{
			OrgRequest: req,
			Error:      err,
			Data: map[string]interface{}{
				"value": data,
			},
		}
	}

	timerService := new(TimerService)
	ass.EqualError(timerService.Init(bus), "test")

	err = nil

	timerService = new(TimerService)
	ass.NoError(timerService.Init(bus))
	ass.Empty(timerService.timers)

	data = map[string]interface{}{
		"aaa": map[string]interface{}{
			"times": []interface{}{
				map[string]interface{}{
					"active": true,
					"time":   42,
					"repeat": 2,
					"state":  "test",
				},
			},
			"item_values": []string{
				"aaa", "bbb",
			},
		},
	}

	timerService = new(TimerService)
	timerService.trigger = func(id string, data map[string]interface{}) {}
	ass.NoError(timerService.Init(bus))
	ass.Len(timerService.timers, 1)

	ass.Equal([]string{"aaa", "bbb"}, timerService.timers["aaa"].itemValues)
	ass.Equal([]*TimeEntry{
		{
			Active: true,
			Time:   42,
			Repeat: 2,
			State:  "test",
		},
	}, timerService.timers["aaa"].times)

	// write new content to check save function on stop
	timerService.timers["aaa"].itemValues = []string{"ccc"}
	timerService.timers["aaa"].times = []*TimeEntry{
		{
			Active: false,
			Time:   32,
			Repeat: 3,
			State:  "fff",
		},
	}

	ass.NoError(timerService.Start(bus))
	ass.True(timerService.timers["aaa"].running)

	bus.HandleRequestWaitFunc = func(req core.Request, timeout time.Duration) core.Response {
		ass.Equal("core.config#set", req.ID)
		ass.Equal(map[string]interface{}{
			"key": "core.timers",
			"value": map[string]interface{}{
				"aaa": map[string]interface{}{
					"times": []TimeEntry{
						{
							Active: false,
							Time:   32,
							Repeat: 3,
							State:  "fff",
						},
					},
					"item_values": []string{
						"ccc",
					},
				},
			},
		}, req.Parameters)
		return core.Response{
			OrgRequest: req,
			Error:      err,
		}
	}

	timerService.Shutdown(bus)
	ass.False(timerService.timers["aaa"].running)

}

func TestTimerService_handler_Timer(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	timerService := &TimerService{
		timers: make(map[string]*Timer),
	}

	// add
	_, err := timerService.addTimer(map[string]interface{}{
		"key": "aaa",
	})
	ass.NoError(err)

	_, err = timerService.addTimer(map[string]interface{}{
		"key": "aaa",
	})
	ass.EqualError(err, "timer with key aaa already exist")

	// get
	data, err := timerService.listTimer(map[string]interface{}{})
	ass.NoError(err)
	ass.Equal(map[string]interface{}{
		"aaa": map[string]interface{}{
			"item_values": []string{},
			"times":       []TimeEntry{},
		},
	}, data)

	data, err = timerService.getTimer(map[string]interface{}{
		"key": "aaa",
	})
	ass.NoError(err)
	ass.Equal(map[string]interface{}{
		"item_values": []string{},
		"times":       []TimeEntry{},
	}, data)

	_, err = timerService.getTimer(map[string]interface{}{
		"key": "bbb",
	})
	ass.EqualError(err, "no timer with key bbb")

	// remove
	_, err = timerService.removeTimer(map[string]interface{}{
		"key": "aaa",
	})
	ass.NoError(err)

	_, err = timerService.removeTimer(map[string]interface{}{
		"key": "aaa",
	})
	ass.EqualError(err, "no timer with key aaa")
}

func Test_convertTime(t *testing.T) {
	ass := assert.New(t)

	ass.Equal(5*60*60, convertTime("05:00"))
	ass.Equal(5*60, convertTime("00:05:00"))
	ass.Equal(1234, convertTime("1234"))
}

func TestTimerService_handler_TimeEntry(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	timerService := &TimerService{
		trigger: func(id string, data map[string]interface{}) {},
	}

	timer := timerService.newTimer("aaa", nil, nil)
	timerService.timers = map[string]*Timer{"aaa": timer}

	timer.Start()
	defer timer.Stop()

	// add
	_, err := timerService.addTimeEntry(map[string]interface{}{
		"key": "bbb",
	})
	ass.EqualError(err, "no timer with key bbb")

	_, err = timerService.addTimeEntry(map[string]interface{}{
		"key":    "aaa",
		"active": true,
		"time":   42,
		"repeat": 2,
		"state":  "test",
	})
	ass.NoError(err)

	ass.Len(timer.ListEntries(), 1)
	ass.Equal(TimeEntry{
		Active: true,
		Time:   42,
		Repeat: 2,
		State:  "test",
	}, timer.ListEntries()[0])

	// edit
	_, err = timerService.editTimeEntry(map[string]interface{}{
		"key": "bbb",
	})
	ass.EqualError(err, "no timer with key bbb")

	_, err = timerService.editTimeEntry(map[string]interface{}{
		"key": "aaa",
		"idx": 1,
	})
	ass.EqualError(err, "invalid time entry index 1")

	_, err = timerService.editTimeEntry(map[string]interface{}{
		"key": "aaa",
		"idx": 0,

		"active": false,
		"time":   32,
		"repeat": 3,
		"state":  "bbb",
	})
	ass.NoError(err)

	ass.Len(timer.ListEntries(), 1)
	ass.Equal(TimeEntry{
		Active: false,
		Time:   32,
		Repeat: 3,
		State:  "bbb",
	}, timer.ListEntries()[0])

	// remove
	_, err = timerService.removeTimeEntry(map[string]interface{}{
		"key": "bbb",
	})
	ass.EqualError(err, "no timer with key bbb")

	_, err = timerService.removeTimeEntry(map[string]interface{}{
		"key": "aaa",
		"idx": 1,
	})
	ass.EqualError(err, "invalid time entry index 1")

	_, err = timerService.removeTimeEntry(map[string]interface{}{
		"key": "aaa",
		"idx": 0,
	})
	ass.NoError(err)
	ass.Len(timer.ListEntries(), 0)
}

func TestTimerService_handler_ItemValues(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	timerService := &TimerService{
		trigger: func(id string, data map[string]interface{}) {},
	}

	timer := timerService.newTimer("aaa", nil, nil)
	timerService.timers = map[string]*Timer{"aaa": timer}

	timer.Start()
	defer timer.Stop()

	// add
	_, err := timerService.addItemValue(map[string]interface{}{
		"key": "bbb",
	})
	ass.EqualError(err, "no timer with key bbb")

	_, err = timerService.addItemValue(map[string]interface{}{
		"key":        "aaa",
		"item_value": "test",
	})
	ass.NoError(err)

	ass.Equal([]string{"test"}, timer.itemValues)

	// remove
	_, err = timerService.removeItemValue(map[string]interface{}{
		"key": "bbb",
	})
	ass.EqualError(err, "no timer with key bbb")

	_, err = timerService.removeItemValue(map[string]interface{}{
		"key":        "aaa",
		"item_value": "test",
	})
	ass.NoError(err)
	ass.Len(timer.itemValues, 0)
}

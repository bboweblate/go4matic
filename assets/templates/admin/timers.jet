{{extends "base.jet"}}
{{import "utils/modal.jet"}}

{{block body()}}
<div class="row">
    <div class="col-md-6 col-md-offset-1">
        <div class="mt-3 card card-default">
            <div class="card-header">
                <h3>{{Locale.Get("Timers")}}</h3>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-hover">
                    <tbody>
                    <tr>
                        <th>{{Locale.Get("Key")}}</th>
                        <th style="width: 100px">
                            <div class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-success"
                                        data-toggle="modal" data-target="#addModal">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </th>
                    </tr>
                    {{range key := sort(timers)}}
                    <tr>
                        <td>{{key}}</td>
                        <td>
                            <div class="btn-group btn-group-sm">
                                <a type="button" class="btn btn-warning" href="./timers/{{key}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <button type="button" class="btn btn-danger"
                                        data-toggle="modal" data-target="#removeModal"
                                        data-key="{{ key }}">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    {{end}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


{{yield formModal(modalId="addModal", title=Locale.Get("Add Timer"), submit=Locale.Get("Add")) content}}
    <div class="form-group">
        <label for="key" class="col-form-label">{{Locale.Get("Key:")}}</label>
        <input type="text" class="form-control" id="key" name="key"/>
    </div>
    <input type="hidden" name="action" value="add"/>
{{end}}

{{yield formModal(modalId="removeModal", submit=Locale.Get("Remove"), submitType="danger") content}}
    <p><b>{{Locale.Get("This action cannot be undone.")}}</b></p>
    <input type="hidden" name="action" value="remove"/>
    <input type="hidden" name="key" id="remove_timer_key"/>
{{end}}

<script>
    $("#removeModal").on("show.bs.modal", function (event) {
        let button = $(event.relatedTarget);

        let modal = $(this);
        modal.find("#removeModal_label").text('{{Locale.Get("Remove Timer {0}?")}}'.format(button.data("key")));
        modal.find("#remove_timer_key").val(button.data("key"));
    });
</script>
{{end}}
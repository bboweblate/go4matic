// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"errors"
	"reflect"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
)

// Type defines base interface of types
type Type interface {
	// Convert given value to type if possible
	Convert(value interface{}) (interface{}, error)

	// Check if the value matches the type
	Check(value interface{}) bool

	// TypeName return the name of the type
	TypeName() string

	// BaseName return the name of the type category
	BaseName() string
}

// Bool represents basic bool types
var Bool = &basicType{boolT, "bool", "boolean"}

// Int represents basic int types
var Int = &basicType{intT, "int", "integer"}

// Int8 represents basic int8 types
var Int8 = &basicType{int8T, "int8", "integer"}

// Int16 represents basic int16 types
var Int16 = &basicType{int16T, "int16", "integer"}

// Int32 represents basic int32 types
var Int32 = &basicType{int32T, "int32", "integer"}

// Int64 represents basic int64 types
var Int64 = &basicType{int64T, "int64", "integer"}

// Uint represents basic uint types
var Uint = &basicType{uintT, "uint", "integer"}

// Uint8 represents basic uint8 types
var Uint8 = &basicType{uint8T, "uint8", "integer"}

// Uint16 represents basic uint16 types
var Uint16 = &basicType{uint16T, "uint16", "integer"}

// Uint32 represents basic uint32 types
var Uint32 = &basicType{uint32T, "uint32", "integer"}

// Uint64 represents basic uint64 types
var Uint64 = &basicType{uint64T, "uint64", "integer"}

// Float32 represents basic float32 types
var Float32 = &basicType{float32T, "float32", "number"}

// Float64 represents basic float64 types
var Float64 = &basicType{float64T, "float64", "number"}

// String represents basic string types
var String = &basicType{stringT, "string", "string"}

// basic type value kinds
type basicKind int

const (
	boolT basicKind = iota
	intT
	int8T
	int16T
	int32T
	int64T
	uintT
	uint8T
	uint16T
	uint32T
	uint64T
	float32T
	float64T
	stringT
)

// basicType implements the type check for basic types
type basicType struct {
	kind     basicKind
	name     string
	baseType string
}

// Convert given value to type if possible
func (t *basicType) Convert(value interface{}) (interface{}, error) {
	switch t.kind {
	case boolT:
		return cast.ToBoolE(value)
	case intT:
		return cast.ToIntE(value)
	case int8T:
		return cast.ToInt8E(value)
	case int16T:
		return cast.ToInt16E(value)
	case int32T:
		return cast.ToInt32E(value)
	case int64T:
		return cast.ToInt64E(value)
	case uintT:
		return cast.ToUintE(value)
	case uint8T:
		return cast.ToUint8E(value)
	case uint16T:
		return cast.ToUint16E(value)
	case uint32T:
		return cast.ToUint32E(value)
	case uint64T:
		return cast.ToUint64E(value)
	case float32T:
		return cast.ToFloat32E(value)
	case float64T:
		return cast.ToFloat64E(value)
	case stringT:
		return cast.ToStringE(value)
	default:
		return value, errors.New("unknown type")
	}
}

// Check if the value matches the type
func (t *basicType) Check(value interface{}) bool {
	_, err := t.Convert(value)
	return err == nil
}

// TypeName return the name of the type
func (t *basicType) TypeName() string {
	return t.name
}

// BaseName return the name of the type category
func (t *basicType) BaseName() string {
	return t.baseType
}

// Error is used for errors types
var Error = &errorType{}

// errorType implements a error type (string or go error)
type errorType struct{}

// Convert given value to type if possible
func (e *errorType) Convert(value interface{}) (interface{}, error) {
	if _, ok := value.(error); ok {
		return value, nil
	}

	str, err := cast.ToStringE(value)
	if err != nil {
		return nil, err
	}
	return errors.New(str), nil
}

// Check if the value matches the type
func (e *errorType) Check(value interface{}) bool {
	_, err := e.Convert(value)
	return err == nil
}

// TypeName return the name of the type
func (e *errorType) TypeName() string {
	return "error"
}

// BaseName return the name of the type category
func (e *errorType) BaseName() string {
	return "error"
}

// Any is used for undefined/irrelevant types
var Any = &anyType{}

// anyType implements a type that accept any type
type anyType struct{}

// Convert given value to type if possible
func (t *anyType) Convert(value interface{}) (interface{}, error) {
	return value, nil
}

// Check if the value matches the type
func (t *anyType) Check(value interface{}) bool {
	return true
}

// TypeName return the name of the type
func (t *anyType) TypeName() string {
	return "*"
}

// BaseName return the name of the type category
func (t *anyType) BaseName() string {
	return "AnyValue"
}

// Slice returns a slice type with the given sub element
func Slice(t Type) Type {
	return &sliceType{t}
}

// sliceType implements a slice type
type sliceType struct {
	subType Type
}

// Convert given value to type if possible
func (t *sliceType) Convert(value interface{}) (interface{}, error) {
	s := reflect.ValueOf(value)
	if s.Kind() != reflect.Slice {
		return nil, errors.New(assets.L.Get("value is not a slice"))
	}

	list := make([]interface{}, s.Len())
	for i := 0; i < s.Len(); i++ {
		v, err := t.subType.Convert(s.Index(i).Interface())
		if err != nil {
			return nil, err
		}

		list[i] = v
	}
	return list, nil
}

// Check if the value matches the type
func (t *sliceType) Check(value interface{}) bool {
	_, err := t.Convert(value)
	return err == nil
}

// TypeName return the name of the type
func (t *sliceType) TypeName() string {
	return "[]" + t.subType.TypeName()
}

// BaseName return the name of the type category
func (t *sliceType) BaseName() string {
	return "array"
}

// Map returns a map with the given types
func Map(key, value Type) Type {
	return &mapType{key, value}
}

// mapType implements a map type
type mapType struct {
	key   Type
	value Type
}

// Convert given value to type if possible
func (t *mapType) Convert(value interface{}) (interface{}, error) {
	m := reflect.ValueOf(value)
	if m.Kind() != reflect.Map {
		return nil, errors.New("value is not a map")
	}

	data := make(map[interface{}]interface{}, m.Len())
	for _, mapKey := range m.MapKeys() {
		key, err := t.key.Convert(mapKey.Interface())
		if err != nil {
			return nil, err
		}
		value, err := t.value.Convert(m.MapIndex(mapKey).Interface())
		if err != nil {
			return nil, err
		}

		data[key] = value
	}
	return data, nil
}

// Check if the value matches the type
func (t *mapType) Check(value interface{}) bool {
	_, err := t.Convert(value)
	return err == nil
}

// TypeName return the name of the type
func (t *mapType) TypeName() string {
	return "map[" + t.key.TypeName() + "]" + t.value.TypeName()
}

// BaseName return the name of the type category
func (t *mapType) BaseName() string {
	return "object"
}

// TypeFromValue returns type from value
func TypeFromValue(val interface{}) Type {
	switch val.(type) {
	case bool:
		return Bool
	case int:
		return Int
	case int8:
		return Int8
	case int16:
		return Int16
	case int32:
		return Int32
	case int64:
		return Int64
	case uint:
		return Uint
	case uint8:
		return Uint8
	case uint16:
		return Uint16
	case uint32:
		return Uint32
	case uint64:
		return Uint64
	case float32:
		return Float32
	case float64:
		return Float64
	case string:
		return String

	case []string:
		return Slice(String)
	case []interface{}:
		return Slice(Any)

	case map[string]string:
		return Map(String, String)
	case map[string]interface{}:
		return Map(String, Any)

	default:
		return Any
	}
}

// TypeFromTypeName returns type from type name
func TypeFromTypeName(name string) Type {
	switch name {
	case "bool":
		return Bool
	case "int":
		return Int
	case "int8":
		return Int8
	case "int16":
		return Int16
	case "int32":
		return Int32
	case "int64":
		return Int64
	case "uint":
		return Uint
	case "uint8":
		return Uint8
	case "uint16":
		return Uint16
	case "uint32":
		return Uint32
	case "uint64":
		return Uint64
	case "float32":
		return Float32
	case "float64":
		return Float64
	case "string":
		return String

	case "[]string":
		return Slice(String)

	case "map[string]string":
		return Map(String, String)

	default:
		return Any
	}
}

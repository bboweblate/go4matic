// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package core

import (
	"errors"
	"sort"
	"strings"
	"sync"
	"time"

	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

// The Bus is the main communication interface between multiple components
type Bus interface {
	// Start sends start event to all services
	Start() error

	// Shutdown bus and stop all services in reverse order of registration
	Shutdown()

	// RegisterService to bus instance
	RegisterService(service Service) error

	// HandleRequest forwards the request to the assigned service
	HandleRequest(request Request)

	// HandleRequestWait handles request and wait for response
	//
	// Note: This will overwrite the response channel if set
	HandleRequestWait(request Request, timeout time.Duration) Response

	// RegisterEventListener registers a new listener for the given key
	RegisterEventListener(id string, listener chan Event) error

	// UnregisterEventListener remove the registered listener
	UnregisterEventListener(id string, listener chan Event)
}

// Request that is handled by a service
type Request struct {
	// Identifier of request handler
	ID string

	// Parameters of action
	Parameters map[string]interface{}

	// Channel for response
	ResponseChannel chan Response
}

// sendResponse for this request
func (r Request) sendResponse(data map[string]interface{}, err error) {
	// no response channel -> no response
	if r.ResponseChannel == nil {
		return
	}

	// response data should never be nil
	if data == nil {
		data = make(map[string]interface{})
	}

	// create response object
	response := Response{
		OrgRequest: r,
		Error:      err,
		Data:       data,
	}

	select {
	case r.ResponseChannel <- response:
	case <-time.After(time.Millisecond):
		// response channel busy -> drop response
	}
}

// Response of a handled request
type Response struct {
	// Original response
	OrgRequest Request

	// Error if not successful
	Error error

	Data map[string]interface{}
}

// Event triggered from service
type Event struct {
	// Identifier of event
	ID string

	Data map[string]interface{}
}

// Service that can be registered to a bus
type Service interface {
	// Key returns a unique identifier for this Service
	Key() string
}

// InitService -> a service that handle init event
type InitService interface {
	Service

	// called if base initialization is done
	Init(bus Bus) error
}

// StartService -> a service that handle start event
type StartService interface {
	Service

	// called if all plugins and services are initialized
	Start(bus Bus) error
}

// UpdateService -> a service that handle update events
type UpdateService interface {
	Service

	// called periodically (default: each minute)
	Update(bus Bus)
}

// UpdateIntervalService -> defines a custom interval for update events
type UpdateIntervalService interface {
	UpdateService

	// duration for update calls
	UpdateInterval() time.Duration
}

// ShutdownService -> a service that handle shutdown event
type ShutdownService interface {
	Service

	// called if service should be stopped
	Shutdown(bus Bus)
}

// RequestHandler will handle a request
type RequestHandler struct {
	Def api.SimpleHandler

	// Function that handles the request
	Func func(map[string]interface{}) (map[string]interface{}, error)
}

// RequestService represent an endpoint for the bus that handles received requests
type RequestService interface {
	Service

	// Handlers returns a map with all supported action and their handle functions
	Handlers() map[string]RequestHandler
}

// EventTrigger will trigger an event and forward to listener
type EventTrigger func(id string, data map[string]interface{})

// EventService represent an endpoint that emit events
type EventService interface {
	Service

	// Events returns all events that can be emitted
	Events() map[string]api.SimpleHandler

	// SetEventTrigger for service
	SetEventTrigger(trigger EventTrigger)
}

// serviceHelper will run the goroutines used to forward requests and events
type serviceHelper struct {
	// bus instance
	bus *bus
	// service instance
	service Service

	// buffer for requests
	requestBuffer chan Request

	// True if running
	running bool

	// Wait group for shutdown
	waitGroup *sync.WaitGroup

	// start index of service
	startIndex uint16
}

// callHandler and catch panic if something goes really wrong
func (s *serviceHelper) callHandler(request Request, handler RequestHandler) {
	defer func() {
		if err := recover(); err != nil {
			zap.L().Error(assets.L.Get("request handling failed: %s", err))
			request.sendResponse(nil, errors.New(assets.L.Get("failed to handle request")))
		}
	}()

	parameters, err := handler.Def.ValidateParameters(request.Parameters)
	if err != nil {
		request.sendResponse(nil,
			errors.New(assets.L.Get("invalid parameters: %v", err)))
	} else {
		request.sendResponse(handler.Func(parameters))
	}
}

// run the handling loop for service
func (s *serviceHelper) run() {
	zap.L().Debug(assets.L.Get("Service %s loaded", s.service.Key()))

	// handlers for request services
	handlers := make(map[string]RequestHandler)
	if rs, ok := s.service.(RequestService); ok {
		handlers = rs.Handlers()
	}

	// prepare update services
	var updateTicker *time.Ticker
	if us, ok := s.service.(UpdateService); ok {
		updateInterval := time.Minute
		if uis, ok := s.service.(UpdateIntervalService); ok {
			updateInterval = uis.UpdateInterval()
		}
		updateTicker = time.NewTicker(updateInterval)
		go func() {
			for range updateTicker.C {
				us.Update(s.bus)
			}
		}()
	}

	for request := range s.requestBuffer {
		if request.ID == "" {
			break
		}
		_, _, action := splitID(request.ID)
		if handler, ok := handlers[action]; ok {
			s.callHandler(request, handler)
		} else {
			request.sendResponse(nil,
				errors.New(assets.L.Get("unknown action %s", request.ID)))
		}
	}

	zap.L().Debug(assets.L.Get("Service %s stopping", s.service.Key()))
	if updateTicker != nil {
		updateTicker.Stop()
	}

	// run shutdown of service
	service, ok := s.service.(ShutdownService)
	if ok {
		service.Shutdown(s.bus)
	}

	zap.L().Debug(assets.L.Get("Service %s finished", s.service.Key()))
	s.waitGroup.Done()
}

// triggerEvent callback for event service to trigger an event
func (s *serviceHelper) triggerEvent(id string, data map[string]interface{}) {
	if id != "" {
		select {
		case s.bus.eventBuffer <- Event{ID: id, Data: data}:
			// event transferred to listener
		case <-time.After(time.Microsecond * 100):
			// buffer full
			// -> skip it
		}
	}
}

// The Bus is the main communication interface between multiple components
type bus struct {
	// true if the shutdown process was started
	shutdownInProgress bool

	// list of registered services
	services      map[string]*serviceHelper
	servicesMutex sync.RWMutex

	// buffer for triggered events
	eventBuffer chan Event

	// list of registered event listeners
	eventListeners      map[string][]chan Event
	eventListenersMutex sync.RWMutex

	// True if running
	running bool

	// index of last started service
	indexCounter uint16

	// Wait group for shutdown
	waitGroup sync.WaitGroup
}

// CreateBus instance and prepare for usage
func CreateBus() Bus {
	bus := bus{
		services:       make(map[string]*serviceHelper),
		eventBuffer:    make(chan Event, 20),
		eventListeners: make(map[string][]chan Event),
		running:        true,
	}

	go bus.run()

	return &bus
}

// Shutdown bus and stop all services in reverse order of registration
func (b *bus) run() {
	for event := range b.eventBuffer {
		if event.ID == "" {
			break
		}
		b.eventListenersMutex.RLock()

		// check if listener for event exist
		listeners, ok := b.eventListeners[event.ID]
		if !ok {
			b.eventListenersMutex.RUnlock()
			continue
		}

		for _, listener := range listeners {
			select {
			case listener <- event: // forward event
			default: // drop event if not listening
			}
		}
		b.eventListenersMutex.RUnlock()
	}
	b.waitGroup.Done()
}

// Start sends start event to all services
func (b *bus) Start() error {
	zap.L().Debug(assets.L.Get("Start services"))

	b.servicesMutex.RLock()
	defer b.servicesMutex.RUnlock()

	for _, s := range b.services {
		service, ok := s.service.(StartService)
		if ok {
			err := service.Start(b)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// Shutdown bus and stop all services in reverse order of registration
func (b *bus) Shutdown() {
	zap.L().Debug(assets.L.Get("Start service shutdown"))

	// shutdown already in progress?
	if b.shutdownInProgress {
		return
	}

	b.shutdownInProgress = true

	// prepare list to sort services by start index
	b.servicesMutex.RLock()
	indexServiceMap := make(map[uint16]*serviceHelper, len(b.services))
	indexServiceList := make([]uint16, 0, len(b.services))
	for _, s := range b.services {
		indexServiceMap[s.startIndex] = s
		indexServiceList = append(indexServiceList, s.startIndex)
	}
	b.servicesMutex.RUnlock()

	// reverse sort
	sort.Slice(indexServiceList, func(i, j int) bool {
		return indexServiceList[i] > indexServiceList[j]
	})

	for _, idx := range indexServiceList {
		b.servicesMutex.Lock()
		b.waitGroup.Add(1)
		indexServiceMap[idx].waitGroup = &b.waitGroup
		indexServiceMap[idx].running = false
		indexServiceMap[idx].requestBuffer <- Request{}
		b.servicesMutex.Unlock()
		b.waitGroup.Wait()
	}
	zap.L().Info(assets.L.Get("All services stopped"))

	b.servicesMutex.Lock()
	b.waitGroup.Add(1)
	b.running = false
	b.eventBuffer <- Event{}
	b.servicesMutex.Unlock()
	b.waitGroup.Wait()
	zap.L().Info(assets.L.Get("Bus stopped"))
}

// RegisterService to bus instance
func (b *bus) RegisterService(service Service) error {
	b.servicesMutex.Lock()

	// disallow service registration if bus was stopped
	if b.shutdownInProgress {
		b.servicesMutex.Unlock()
		return errors.New(assets.L.Get("shutdown in progress"))
	}

	key := service.Key()
	if _, ok := b.services[key]; ok {
		b.servicesMutex.Unlock()
		return errors.New(assets.L.Get("service with key %s already registered", service.Key()))
	}

	// create helper with a new index
	b.indexCounter++
	srv := &serviceHelper{
		bus:           b,
		service:       service,
		requestBuffer: make(chan Request, 100),
		startIndex:    b.indexCounter,
		running:       true,
	}

	// register trigger function for events services
	if es, ok := srv.service.(EventService); ok {
		es.SetEventTrigger(srv.triggerEvent)
	}
	b.services[key] = srv
	b.servicesMutex.Unlock()

	// start main loop
	go srv.run()

	// run init of service
	s, ok := service.(InitService)
	if ok {
		if err := s.Init(b); err != nil {
			return err
		}
	}
	return nil
}

// HandleRequest forwards the request to the assigned service
func (b *bus) HandleRequest(request Request) {
	_, key, _ := splitID(request.ID)

	b.servicesMutex.RLock()
	service, ok := b.services[key]
	b.servicesMutex.RUnlock()

	// no handler for request
	if !ok {
		request.sendResponse(nil,
			errors.New(assets.L.Get("no service for %s", key)))
		return
	}

	// service not running
	if !service.running {
		request.sendResponse(nil,
			errors.New(assets.L.Get("service for %s not running", key)))
		return
	}

	select {
	case service.requestBuffer <- request:
		// handle action

	case <-time.After(time.Millisecond * 2):
		// channel full -> do not wait

		request.sendResponse(nil,
			errors.New(assets.L.Get("service %s is busy", key)))
	}
}

// HandleRequestWait handles request and wait for response
//
// Note: This will overwrite the response channel if set
func (b *bus) HandleRequestWait(request Request, timeout time.Duration) Response {
	request.ResponseChannel = make(chan Response, 1)
	b.HandleRequest(request)

	select {
	case response := <-request.ResponseChannel:
		return response

	case <-time.After(timeout):
		return Response{
			OrgRequest: request,
			Error:      errors.New(assets.L.Get("timeout for response reached")),
		}
	}
}

// RegisterEventListener registers a new listener for the given key
func (b *bus) RegisterEventListener(id string, listener chan Event) error {
	_, key, action := splitID(id)

	b.eventListenersMutex.Lock()
	defer b.eventListenersMutex.Unlock()

	id = key + "#" + action
	listeners, ok := b.eventListeners[id]
	if !ok {
		b.eventListeners[id] = []chan Event{listener}
		return nil
	}

	// check if already added
	for _, l := range listeners {
		if l == listener {
			return nil
		}
	}
	b.eventListeners[id] = append(b.eventListeners[id], listener)

	return nil
}

// UnregisterEventListener remove the registered listener
func (b *bus) UnregisterEventListener(id string, listener chan Event) {
	_, key, action := splitID(id)

	b.eventListenersMutex.Lock()
	defer b.eventListenersMutex.Unlock()

	id = key + "#" + action
	listeners, ok := b.eventListeners[id]
	if !ok {
		return
	}

	// remove listeners
	newListeners := make([]chan Event, 0, len(listeners))
	for _, l := range listeners {
		if l != listener {
			newListeners = append(newListeners, l)
		}
	}
	b.eventListeners[id] = newListeners
}

// splitID into instance key and action
func splitID(id string) (instance string, key string, action string) {
	instanceIdx := strings.Index(id, ":")
	if instanceIdx != -1 {
		instance = id[:instanceIdx]
	}

	actionIdx := strings.Index(id, "#")
	if actionIdx != -1 {
		action = id[actionIdx+1:]
		key = id[instanceIdx+1 : actionIdx]
	} else {
		key = id[instanceIdx+1:]
	}

	return
}

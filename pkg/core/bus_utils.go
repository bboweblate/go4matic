// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package core

import (
	"errors"
	"strings"
	"time"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

// APIWrapper creates a wrapper to forward an API request to the bus
func APIWrapper(b Bus, service RequestService, action string, entry PathEntry) PathEntry {
	handler := service.Handlers()[action]

	id := service.Key() + "#" + action
	param := make(map[string]interface{}, len(handler.Def.Parameters))

	entry.Def = api.ExtendedHandler{
		Description: handler.Def.Description,
		Parameters:  handler.Def.Parameters,
		Responses: map[string]api.Response{
			"200": {
				Data: handler.Def.Response,
			},
			"400": api.InvalidRequestResponse,
		},
	}

	entry.Handler = func(ctx HTTPContext) {
		for name := range handler.Def.Parameters {
			if strings.Contains(entry.Path, ":"+name) {
				param[name] = ctx.Param(name)
			} else {
				value := ctx.BodyValue(name)
				if value != nil {
					param[name] = value
				}
			}
		}
		if ctx.IsAborted() {
			return
		}

		ctx.BusResponse(b.HandleRequestWait(Request{
			ID:         id,
			Parameters: param,
		}, time.Second))
	}
	return entry
}

// ConfigRead returns the config value from the given key
func ConfigRead(b Bus, key string) (interface{}, error) {
	response := b.HandleRequestWait(Request{
		ID: "core.config#get",
		Parameters: map[string]interface{}{
			"key": key,
		},
	}, time.Second*5)

	return response.Data["value"], response.Error
}

// ConfigWrite writes the given config value
func ConfigWrite(b Bus, key string, value interface{}) error {
	return b.HandleRequestWait(Request{
		ID: "core.config#set",
		Parameters: map[string]interface{}{
			"key":   key,
			"value": value,
		},
	}, time.Second*5).Error
}

// ConfigRemove removes the given config key
func ConfigRemove(b Bus, key string) error {
	return b.HandleRequestWait(Request{
		ID: "core.config#remove",
		Parameters: map[string]interface{}{
			"key": key,
		},
	}, time.Second*2).Error
}

// ItemAdd adds item to item service
func ItemAdd(b Bus, item Item) error {
	// give item service some time to work
	time.Sleep(time.Millisecond * 10)

	return b.HandleRequestWait(Request{
		ID: "core.items#add",
		Parameters: map[string]interface{}{
			"item": item,
		},
	}, time.Second*5).Error
}

// ItemRemove removes item to item service
func ItemRemove(b Bus, item Item) error {
	return b.HandleRequestWait(Request{
		ID: "core.items#remove",
		Parameters: map[string]interface{}{
			"item": item,
		},
	}, time.Second*5).Error
}

// TestBus implements a bus for units testing
type TestBus struct {
	StartFunc                   func() error
	ShutdownFunc                func()
	RegisterServiceFunc         func(Service) error
	HandleRequestFunc           func(Request)
	HandleRequestWaitFunc       func(Request, time.Duration) Response
	RegisterEventListenerFunc   func(string, chan Event) error
	UnregisterEventListenerFunc func(string, chan Event)
}

// Start sends start event to all services
func (t *TestBus) Start() error {
	if t.StartFunc != nil {
		return t.StartFunc()
	}
	return nil
}

// Shutdown bus and stop all services in reverse order of registration
func (t *TestBus) Shutdown() {
	if t.ShutdownFunc != nil {
		t.ShutdownFunc()
	}
}

// RegisterService to bus instance
func (t *TestBus) RegisterService(service Service) error {
	if t.RegisterServiceFunc != nil {
		return t.RegisterServiceFunc(service)
	}
	return nil
}

// HandleRequest forwards the request to the assigned service
func (t *TestBus) HandleRequest(request Request) {
	if t.HandleRequestFunc != nil {
		t.HandleRequestFunc(request)
	}
}

// HandleRequestWait handles request and wait for response
func (t *TestBus) HandleRequestWait(request Request, timeout time.Duration) Response {
	if t.HandleRequestWaitFunc != nil {
		return t.HandleRequestWaitFunc(request, timeout)
	}
	return Response{
		OrgRequest: request,
		Error:      errors.New(assets.L.Get("no response to request %s", request.ID)),
	}
}

// RegisterEventListener registers a new listener for the given key
func (t *TestBus) RegisterEventListener(id string, listener chan Event) error {
	if t.RegisterEventListenerFunc != nil {
		return t.RegisterEventListenerFunc(id, listener)
	}
	return nil
}

// UnregisterEventListener remove the registered listener
func (t *TestBus) UnregisterEventListener(id string, listener chan Event) {
	if t.UnregisterEventListenerFunc != nil {
		t.UnregisterEventListenerFunc(id, listener)
	}
}

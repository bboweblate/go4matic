// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package assets

import (
	"bytes"
	"io"
	"path"

	"github.com/leonelquinteros/gotext"
	"go.uber.org/zap"
)

// L contains loaded locale
var L = new(gotext.Locale)

// LoadLocale from translation files
func LoadLocale(lang string) {
	// load directory for selected locale
	basePath := path.Join("/locales", lang)
	file, err := Assets.Open(basePath)
	if err != nil {
		// handle fallback
		if lang == "en_US" {
			return
		}

		zap.S().Error("Failed to load locales for %s -> fallback to en_US", lang)
		LoadLocale("en_US")
		return
	}
	defer file.Close()

	// search for existing translation files
	files, err := file.Readdir(0)
	if err != nil {
		zap.S().Error("Failed to load locales for %s", lang)
		return
	}

	for _, fileInfo := range files {
		// open translation file
		file, err = Assets.Open(path.Join(basePath, fileInfo.Name()))
		if err != nil {
			continue
		}

		// load file as PO file and add to locale
		buf := bytes.NewBuffer(nil)
		io.Copy(buf, file)
		file.Close()

		po := new(gotext.Po)
		po.Parse(buf.Bytes())

		dom, _ := path.Split(fileInfo.Name())
		L.AddTranslator(dom, po)
	}
}

// SetLang for locale to use
func SetLang(lang string) {
	LoadLocale(lang)
}

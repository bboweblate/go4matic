// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

// ColourPicker block
func ColourPicker() Block {
	return Block{
		Type: "colour_picker",
		Fields: []Field{{
			Name: "COLOUR",
			Data: "#ff0000",
		}},
	}
}

// ColourRandom block
func ColourRandom() Block {
	return Block{
		Type: "colour_random",
	}
}

// ColourRGB block
func ColourRGB() Block {
	return Block{
		Type: "colour_rgb",
		Values: []Value{{
			Name: "RED",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "100",
				},
			},
		}, {
			Name: "GREEN",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "50",
				},
			},
		}, {
			Name: "BLUE",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "0",
				},
			},
		}},
	}
}

// ColourBlend block
func ColourBlend() Block {
	return Block{
		Type: "colour_blend",
		Values: []Value{{
			Name: "COLOUR1",
			Shadow: &Shadow{
				Type: "colour_picker",
				Field: Field{
					Name: "COLOUR",
					Data: "#ff0000",
				},
			},
		}, {
			Name: "COLOUR2",
			Shadow: &Shadow{
				Type: "colour_picker",
				Field: Field{
					Name: "COLOUR",
					Data: "#3333ff",
				},
			},
		}, {
			Name: "RATIO",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "0.5",
				},
			},
		}},
	}
}

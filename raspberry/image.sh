#!/bin/sh

cp ${INPUT_PATH}/out/go4matic_linux_arm ${ROOTFS_PATH}/usr/bin/go4matic
cp ${INPUT_PATH}/raspberry/init.sh ${ROOTFS_PATH}/etc/init.d/go4matic

chroot_exec rc-update add go4matic default

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package admin

import (
	"fmt"
	"time"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/internal/web/dashboard"
	"gitlab.com/go4matic/go4matic/internal/web/helper"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// handle post on dashboards admin page
func (w *Web) adminDashboardsPost(ctx core.HTTPContext) {
	var request core.Request
	action := ctx.PostForm("action")
	switch action {
	case "add":
		request = core.Request{
			ID: "core.dashboards#dashboard_add",
			Parameters: map[string]interface{}{
				"dashboard_name": ctx.PostForm("name"),
				"hide":           ctx.PostForm("hide") != "",
				"icon":           ctx.PostForm("icon"),
				"max_columns":    ctx.PostForm("max_columns"),
			},
		}
	case "remove":
		request = core.Request{
			ID: "core.dashboards#dashboard_remove",
			Parameters: map[string]interface{}{
				"dashboard_name": ctx.PostForm("name"),
			},
		}
	default:
		helper.AddMessage(ctx, "danger",
			assets.L.Get("unknown action %s", action))
		return
	}

	response := w.Bus.HandleRequestWait(request, time.Second)
	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
	}
}

// render dashboards admin
func (w *Web) adminDashboards(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/dashboards")
	ctx.Set("title", "Dashboards/Admin - Go4Matic")

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#dashboard_list",
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}
	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["dashboards"] = response.Data["dashboards"]
}

// handle post on dashboard admin page
func (w *Web) adminDashboardPost(ctx core.HTTPContext) {
	var request core.Request
	action := ctx.PostForm("action")
	switch action {
	case "edit_dashboard":
		request = core.Request{
			ID: "core.dashboards#dashboard_change",
			Parameters: map[string]interface{}{
				"dashboard_name": ctx.Param("name"),
				"new_name":       ctx.PostForm("new_name"),
				"hide":           ctx.PostForm("hide") != "",
				"icon":           ctx.PostForm("icon"),
				"max_columns":    ctx.PostForm("max_columns"),
				"new_position":   ctx.PostForm("new_position"),
			},
		}

	case "add_group":
		request = core.Request{
			ID: "core.dashboards#group_add",
			Parameters: map[string]interface{}{
				"dashboard_name": ctx.Param("name"),
				"group_name":     ctx.PostForm("name"),
			},
		}
	case "edit_group":
		request = core.Request{
			ID: "core.dashboards#group_change",
			Parameters: map[string]interface{}{
				"dashboard_name": ctx.Param("name"),
				"group_idx":      ctx.PostForm("group_id"),
				"group_name":     ctx.PostForm("name"),
				"new_position":   ctx.PostForm("new_position"),
			},
		}
	case "remove_group":
		request = core.Request{
			ID: "core.dashboards#group_remove",
			Parameters: map[string]interface{}{
				"dashboard_name": ctx.Param("name"),
				"group_idx":      ctx.PostForm("group_id"),
			},
		}

	case "remove_widget":
		request = core.Request{
			ID: "core.dashboards#widget_remove",
			Parameters: map[string]interface{}{
				"dashboard_name": ctx.Param("name"),
				"group_idx":      ctx.PostForm("group_id"),
				"widget_idx":     ctx.PostForm("widget_id"),
			},
		}

	default:
		helper.AddMessage(ctx, "danger",
			assets.L.Get("unknown action %s", action))
		return
	}

	response := w.Bus.HandleRequestWait(request, time.Second)
	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	if action == "edit_dashboard" && ctx.Param("name") != ctx.PostForm("new_name") {
		ctx.Redirect(302, ctx.PostForm("new_name"))
		ctx.Abort()
	}
}

// render dashboard admin
func (w *Web) adminDashboard(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/dashboard")
	ctx.Set("title", fmt.Sprintf("%s/Dashboards/Admin - Go4Matic", ctx.Param("name")))

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#dashboard_get",
		Parameters: map[string]interface{}{
			"dashboard_name": ctx.Param("name"),
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		ctx.Set("tpl", "admin")
		return
	}

	typeResponse := w.Bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#widget_type_list",
	}, time.Second)

	if typeResponse.Error != nil {
		helper.AddMessage(ctx, "danger", typeResponse.Error.Error())
		ctx.Set("tpl", "admin")
		return
	}

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["dashboard"] = response.Data["dashboard"]
	vars["dashboard_idx"] = response.Data["dashboard_idx"]
	vars["widget_types"] = typeResponse.Data
	vars["dashboard_name"] = ctx.Param("name")
}

// handle post on new widget admin page
func (w *Web) adminDashboardNewWidgetPost(ctx core.HTTPContext) {
	typeResponse := w.Bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#widget_type_list",
	}, time.Second)

	if typeResponse.Error != nil {
		helper.AddMessage(ctx, "danger", typeResponse.Error.Error())
		return
	}

	widgetType := ctx.Param("widget_type")
	value, ok := typeResponse.Data[widgetType]
	if !ok {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("unknown widget type %s", widgetType))
		return
	}

	values, err := helper.FieldFormValues(ctx, value.(api.FieldMap))
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#widget_add",
		Parameters: map[string]interface{}{
			"dashboard_name": ctx.Param("name"),
			"group_idx":      ctx.Param("group"),
			"widget_type":    ctx.Param("widget_type"),
			"config":         values,
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	ctx.Redirect(302, "../..")
	ctx.Abort()
}

// render new widget admin page
func (w *Web) adminDashboardNewWidget(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/dashboard_widget_add")
	ctx.Set("title", fmt.Sprintf("%s/Dashboards/Admin - Go4Matic", ctx.Param("name")))

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#dashboard_get",
		Parameters: map[string]interface{}{
			"dashboard_name": ctx.Param("name"),
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		ctx.Set("tpl", "admin")
		return
	}

	typeResponse := w.Bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#widget_type_list",
	}, time.Second)

	if typeResponse.Error != nil {
		helper.AddMessage(ctx, "danger", typeResponse.Error.Error())
		ctx.Set("tpl", "admin")
		return
	}

	widgetType := ctx.Param("widget_type")
	value, ok := typeResponse.Data[widgetType]
	if !ok {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("unknown widget type %s", widgetType))
		ctx.Set("tpl", "admin")
		return
	}
	widgetForm, err := helper.FieldForm(value.(api.FieldMap), nil)

	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		ctx.Set("tpl", "admin")
		return
	}

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["widget_type"] = widgetType
	vars["widget_form"] = widgetForm
}

// handle post on edit widget admin page
func (w *Web) adminDashboardEditWidgetPost(ctx core.HTTPContext) {
	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#dashboard_get",
		Parameters: map[string]interface{}{
			"dashboard_name": ctx.Param("name"),
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		ctx.Set("tpl", "admin")
		return
	}

	groupIdx := cast.ToInt(ctx.Param("group"))
	dash := response.Data["dashboard"].(dashboard.Dashboard)
	if groupIdx < 0 || groupIdx >= len(dash.Groups) {
		helper.AddMessage(ctx, "danger", assets.L.Get("invalid group"))
		ctx.Set("tpl", "admin")
		return
	}

	group := dash.Groups[groupIdx]
	widgetIdx := cast.ToInt(ctx.Param("widget"))
	if widgetIdx < 0 || widgetIdx >= len(group.Widgets) {
		helper.AddMessage(ctx, "danger", assets.L.Get("invalid widget"))
		ctx.Set("tpl", "admin")
		return
	}
	wrapper := group.Widgets[widgetIdx]

	if wrapper.Widget == nil {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("unknown widget type %s", wrapper.TypeID()))
		ctx.Set("tpl", "admin")
		return
	}
	config, err := helper.FieldFormValues(ctx, wrapper.Widget.ConfigDef())

	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		ctx.Set("tpl", "admin")
		return
	}

	response = w.Bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#widget_change",
		Parameters: map[string]interface{}{
			"dashboard_name": ctx.Param("name"),
			"group_idx":      ctx.Param("group"),
			"widget_idx":     ctx.Param("widget"),
			"new_position":   ctx.PostForm("new_position"),
			"config":         config,
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	ctx.Redirect(302, "../..")
	ctx.Abort()
}

// render edit widget admin page
func (w *Web) adminDashboardEditWidget(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/dashboard_widget_edit")
	ctx.Set("title", fmt.Sprintf("%s/Dashboards/Admin - Go4Matic", ctx.Param("name")))

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#dashboard_get",
		Parameters: map[string]interface{}{
			"dashboard_name": ctx.Param("name"),
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		ctx.Set("tpl", "admin")
		return
	}

	groupIdx := cast.ToInt(ctx.Param("group"))
	dash := response.Data["dashboard"].(dashboard.Dashboard)
	if groupIdx < 0 || groupIdx >= len(dash.Groups) {
		helper.AddMessage(ctx, "danger", assets.L.Get("invalid group"))
		ctx.Set("tpl", "admin")
		return
	}

	group := dash.Groups[groupIdx]
	widgetIdx := cast.ToInt(ctx.Param("widget"))
	if widgetIdx < 0 || widgetIdx >= len(group.Widgets) {
		helper.AddMessage(ctx, "danger", assets.L.Get("invalid widget"))
		ctx.Set("tpl", "admin")
		return
	}
	wrapper := group.Widgets[widgetIdx]

	typeResponse := w.Bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#widget_type_list",
	}, time.Second)

	if typeResponse.Error != nil {
		helper.AddMessage(ctx, "danger", typeResponse.Error.Error())
		ctx.Set("tpl", "admin")
		return
	}

	if wrapper.Widget == nil {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("unknown widget type %s", wrapper.TypeID()))
		ctx.Set("tpl", "admin")
		return
	}
	widgetForm, err := helper.FieldForm(wrapper.Widget.ConfigDef(), wrapper.Config)

	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		ctx.Set("tpl", "admin")
		return
	}

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["widget_name"] = wrapper.Name()
	vars["widget_form"] = widgetForm
	vars["widget_idx"] = widgetIdx
}

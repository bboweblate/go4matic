// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"runtime"

	"github.com/leonelquinteros/gotext/cli/xgotext/parser"
)

// parse jet template files
func jetParser(dirPath, basePath string, data *parser.DomainMap) error {
	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return err
	}

	for _, file := range files {
		if file.IsDir() {
			continue
		}

		parseJetFile(path.Join(dirPath, file.Name()), basePath, data)
	}
	return nil
}

var regGet = regexp.MustCompile(`Locale\.Get\(\W*("(\\"|[^"])+")`)
var regGetN = regexp.MustCompile(`Locale\.GetN\(\W*("(\\"|[^"])+")\W*,\W*("(\\"|[^"])+")`)

func parseJetFile(path, basePath string, data *parser.DomainMap) {
	file, err := os.Open(path)
	if err != nil {
		log.Print(err)
		return
	}
	defer file.Close()

	relPath, _ := filepath.Rel(basePath, path)

	scanner := bufio.NewScanner(file)
	lineCounter := 1
	for scanner.Scan() {
		lineCounter++
		line := scanner.Text()

		if regGet.MatchString(line) {
			match := regGet.FindStringSubmatch(line)

			data.AddTranslation("", &parser.Translation{
				MsgId:           match[1],
				SourceLocations: []string{fmt.Sprintf("%s:%d", relPath, lineCounter)},
			})
		} else if regGetN.MatchString(line) {
			match := regGetN.FindStringSubmatch(line)

			data.AddTranslation("", &parser.Translation{
				MsgId:           match[1],
				MsgIdPlural:     match[3],
				SourceLocations: []string{fmt.Sprintf("%s:%d", relPath, lineCounter)},
			})
		}
	}
}

func main() {
	var _, file, _, _ = runtime.Caller(0)

	parser.AddParser(jetParser)
	data := new(parser.DomainMap)

	err := parser.ParseDirRec(
		path.Join(path.Dir(file), "../../"),
		[]string{".git", "node_modules"},
		data)
	if err != nil {
		log.Fatal(err)
	}

	err = data.Save(path.Join(path.Dir(file), "../../assets/locales"))
	if err != nil {
		log.Fatal(err)
	}
}

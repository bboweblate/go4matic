module gitlab.com/go4matic/go4matic

require (
	github.com/CloudyKit/fastprinter v0.0.0-20200109182630-33d98a066a53 // indirect
	github.com/CloudyKit/jet v2.1.2+incompatible
	github.com/amimof/huego v0.0.0-20191031212646-fbe1857ba8f5
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/gin-gonic/gin v1.5.0
	github.com/gobwas/httphead v0.0.0-20180130184737-2c6c146eadee // indirect
	github.com/gobwas/pool v0.2.0 // indirect
	github.com/gobwas/ws v1.0.2
	github.com/influxdata/influxdb1-client v0.0.0-20191209144304-8bf82d3c094d
	github.com/json-iterator/go v1.1.9 // indirect
	github.com/leonelquinteros/gotext v1.4.1-0.20200229193815-be1a13b346a1
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749
	github.com/spf13/cast v1.3.1
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/yuin/gopher-lua v0.0.0-20191220021717-ab39c6098bdb
	gitlab.com/bboehmke/homematic v0.0.0-20200131164245-c77bd7d59c29
	gitlab.com/bboehmke/raspi-alpine-builder v0.0.0-20200105184953-1d0f42950834
	gitlab.com/bboehmke/sunny v0.9.3-0.20200215182607-8f56da7e2e3d
	go.uber.org/atomic v1.5.1 // indirect
	go.uber.org/multierr v1.4.0 // indirect
	go.uber.org/zap v1.13.0
	golang.org/x/lint v0.0.0-20191125180803-fdd1cda4f05f // indirect
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
	golang.org/x/sys v0.0.0-20200107162124-548cf772de50 // indirect
	golang.org/x/tools v0.0.0-20200221224223-e1da425f72fd // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0 // indirect
	gopkg.in/yaml.v2 v2.2.7
)

go 1.13

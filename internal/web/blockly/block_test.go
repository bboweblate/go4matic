// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package blockly

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/go4matic/go4matic/internal/web/blockly/toolbox"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

func TestNewBlock(t *testing.T) {
	ass := assert.New(t)

	_, err := NewBlock("", api.Function{
		Response: api.FieldList{{}, {}},
	}, "")
	ass.EqualError(err, "functions with more then one response are not supported")

	block, err := NewBlock("test", api.Function{
		Description: "desc",
		Arguments: api.FieldMap{
			"aaa": {
				Description: "desc_aaa",
				Order:       1,
				Type: &api.Function{
					Arguments: api.FieldMap{"aaa_aaa": {}},
				},
			},
			"bbb": {
				Description: "desc_bbb",
				Order:       2,
				Type:        api.String,
			},
		},
	}, "module")
	ass.NoError(err)
	ass.Equal(`Blockly.Lua['module__test'] = function(block) {
  var moduleName = Blockly.Lua.provideFunction_('module_module',
    ['local ' + Blockly.Lua.FUNCTION_NAME_PLACEHOLDER_ + ' = require("module")']);
  var aaa_aaa_aaa = Blockly.Lua.variableDB_.getName(block.getFieldValue('aaa_aaa'), Blockly.VARIABLE_CATEGORY_NAME);
  var statements_aaa = Blockly.Lua.statementToCode(block,'aaa');
  var aaa = 'function('+aaa_aaa_aaa+')\n' + statements_aaa + 'end';
  var bbb = Blockly.Lua.valueToCode(block, 'bbb', Blockly.Lua.ORDER_ATOMIC);
  return moduleName + '.test('+aaa+','+bbb+')'+'\n';
};
`, block.JS())
	ass.Equal(map[string]interface{}{
		"args0": []Arg{{
			Type: "input_dummy",
		}},
		"args1": []Arg{{
			Type:  "field_variable",
			Name:  "aaa_aaa",
			Align: "RIGHT",
		}},
		"args2": []Arg{{
			Type:  "input_statement",
			Name:  "aaa",
			Align: "RIGHT",
		}},
		"args3": []Arg{{
			Type:      "input_value",
			Name:      "bbb",
			Align:     "RIGHT",
			ValueType: api.String,
		}},
		"colour":            230,
		"inputsInline":      false,
		"message0":          "desc %1",
		"message1":          "Variables: %1",
		"message2":          "desc_aaa %1",
		"message3":          "desc_bbb %1",
		"nextStatement":     nil,
		"previousStatement": nil,
		"tooltip":           "desc",
		"type":              "module__test",
	}, block.JSON())

	block, err = NewBlock("test2", api.Function{
		Description: "desc",
		Arguments:   api.FieldMap{},
	}, "module")
	ass.NoError(err)
	ass.Equal(`Blockly.Lua['module__test2'] = function(block) {
  var moduleName = Blockly.Lua.provideFunction_('module_module',
    ['local ' + Blockly.Lua.FUNCTION_NAME_PLACEHOLDER_ + ' = require("module")']);
  return moduleName + '.test2('+''+')'+'\n';
};
`, block.JS())
	ass.Equal(map[string]interface{}{
		"args0": []Arg{{
			Type: "input_dummy",
		}},
		"colour":            230,
		"inputsInline":      false,
		"message0":          "desc %1",
		"nextStatement":     nil,
		"previousStatement": nil,
		"tooltip":           "desc",
		"type":              "module__test2",
	}, block.JSON())
}

func TestBlockDef_addJs(t *testing.T) {
	ass := assert.New(t)

	block := BlockDef{
		generator: make([]string, 0),
	}

	block.addJs("aaa", "bbb")
	ass.Equal([]string{
		"aaa", "bbb",
	}, block.generator)

	block.addJs("ccc")
	ass.Equal([]string{
		"aaa", "bbb", "ccc",
	}, block.generator)
}

func TestBlockDef_addModule(t *testing.T) {
	ass := assert.New(t)

	block := BlockDef{
		generator: make([]string, 0),
	}

	block.addModule("TEST")
	ass.Equal([]string{
		"var moduleName = Blockly.Lua.provideFunction_('TEST_module',",
		"  ['local ' + Blockly.Lua.FUNCTION_NAME_PLACEHOLDER_ + ' = require(\"TEST\")']);",
	}, block.generator)
}

func TestBlockDef_addArg(t *testing.T) {
	ass := assert.New(t)

	block := BlockDef{
		def:       make(map[string]interface{}),
		generator: make([]string, 0),
		toolbox: toolbox.Block{
			Fields: make([]toolbox.Field, 0),
			Values: make([]toolbox.Value, 0),
		},
	}

	block.addArg("test", Arg{
		Type:  "field_variable",
		Name:  "bbb",
		Align: "ccc",
	})
	ass.Equal("test", block.def["message0"])
	ass.Equal([]Arg{{
		Type:  "field_variable",
		Name:  "bbb",
		Align: "ccc",
	}}, block.def["args0"])
	ass.Empty(block.toolbox.Values)
	ass.Len(block.toolbox.Fields, 1)
	ass.Equal("bbb", block.toolbox.Fields[0].Name)
	ass.Equal("bbb", block.toolbox.Fields[0].Data)

	block.addArg("test2", Arg{
		Type:      "input_value",
		Name:      "string",
		ValueType: api.String,
	}, Arg{
		Type:      "input_value",
		Name:      "int",
		ValueType: api.Int,
	})
	ass.Equal("test2", block.def["message1"])
	ass.Equal([]Arg{{
		Type:      "input_value",
		Name:      "string",
		ValueType: api.String,
	}, {
		Type:      "input_value",
		Name:      "int",
		ValueType: api.Int,
	}}, block.def["args1"])
	ass.Len(block.toolbox.Fields, 1)
	ass.Len(block.toolbox.Values, 2)

	ass.Equal("string", block.toolbox.Values[0].Name)
	ass.Equal("text", block.toolbox.Values[0].Shadow.Type)
	ass.Equal("int", block.toolbox.Values[1].Name)
	ass.Equal("math_number", block.toolbox.Values[1].Shadow.Type)
}

func TestBlockDef_addFunction(t *testing.T) {
	ass := assert.New(t)

	block := BlockDef{
		def:       make(map[string]interface{}),
		generator: make([]string, 0),
		toolbox: toolbox.Block{
			Fields: make([]toolbox.Field, 0),
			Values: make([]toolbox.Value, 0),
		},
	}

	block.addFunction("test", "test_desc", &api.Function{
		Arguments: api.FieldMap{
			"aaa": {},
			"bbb": {},
		},
	})
	ass.Equal(map[string]interface{}{
		"args0": []Arg{{
			Type:  "field_variable",
			Name:  "aaa",
			Align: "RIGHT",
		}, {
			Type:  "field_variable",
			Name:  "bbb",
			Align: "RIGHT",
		}},
		"args1": []Arg{{
			Type:  "input_statement",
			Name:  "test",
			Align: "RIGHT",
		}},
		"message0": "Variables: %1 %2",
		"message1": "test_desc %1",
	}, block.def)
	ass.Equal([]string{
		"var test_aaa = Blockly.Lua.variableDB_.getName(block.getFieldValue('aaa'), Blockly.VARIABLE_CATEGORY_NAME);",
		"var test_bbb = Blockly.Lua.variableDB_.getName(block.getFieldValue('bbb'), Blockly.VARIABLE_CATEGORY_NAME);",
		"var statements_test = Blockly.Lua.statementToCode(block,'test');",
		"var test = 'function('+test_aaa+','+test_bbb+')\\n' + statements_test + 'end';",
	}, block.generator)
}

func TestBlockDef_addResponse(t *testing.T) {
	ass := assert.New(t)

	block := BlockDef{
		def:       make(map[string]interface{}),
		generator: make([]string, 0),
		toolbox: toolbox.Block{
			Fields: make([]toolbox.Field, 0),
			Values: make([]toolbox.Value, 0),
		},
	}

	block.addResponse("test", nil)
	ass.Equal(map[string]interface{}{
		"nextStatement":     nil,
		"previousStatement": nil,
	}, block.def)
	ass.Equal([]string{"return test+'\\n';"}, block.generator)

	block.def = make(map[string]interface{})
	block.generator = make([]string, 0)
	block.addResponse("test", api.FieldList{{}})
	ass.Equal(map[string]interface{}{
		"output": nil,
	}, block.def)
	ass.Equal([]string{"return [test, Blockly.Lua.ORDER_ATOMIC];"}, block.generator)

	block.def = make(map[string]interface{})
	block.generator = make([]string, 0)
	block.addResponse("test", api.FieldList{{
		Type: api.String,
	}})
	ass.Equal(map[string]interface{}{
		"output": "String",
	}, block.def)
	ass.Equal([]string{"return [test, Blockly.Lua.ORDER_ATOMIC];"}, block.generator)

	block.def = make(map[string]interface{})
	block.generator = make([]string, 0)
	block.addResponse("test", api.FieldList{{
		Type: api.Error,
	}})
	ass.Equal(map[string]interface{}{
		"nextStatement":     nil,
		"previousStatement": nil,
		"args0": []Arg{{
			Type:  "field_variable",
			Name:  "error",
			Align: "RIGHT",
		}}, "args1": []Arg{{
			Type:  "input_statement",
			Name:  "error_handler",
			Align: "RIGHT",
		}},
		"message0": "Variables: %1",
		"message1": "On Error %1",
	}, block.def)
	ass.Equal([]string{
		"var error_handler_error = Blockly.Lua.variableDB_.getName(block.getFieldValue('error'), Blockly.VARIABLE_CATEGORY_NAME);",
		"var statements_error_handler = Blockly.Lua.statementToCode(block,'error_handler');",
		"if (statements_error_handler == '') {",
		"  return test + '\\n';", "} else {",
		"  var code = error_handler_error + ' = ' + test + '\\n';",
		"  code += 'if '+error_handler_error+' ~= nil then\\n' + statements_error_handler +'end\\n';",
		"  return code;",
		"}",
	}, block.generator)
}

func TestBlockDef_Toolbox(t *testing.T) {
	block := BlockDef{
		toolbox: toolbox.Block{
			Fields: make([]toolbox.Field, 0),
			Values: make([]toolbox.Value, 0),
		},
	}
	assert.Equal(t, block.toolbox, block.Toolbox())
}

func TestBlockDef_JSON(t *testing.T) {
	block := BlockDef{
		def: map[string]interface{}{
			"aaa": 42,
		},
	}
	assert.Equal(t, block.def, block.JSON())
}

func TestBlockDef_JS(t *testing.T) {
	block := BlockDef{
		name: "test",
		generator: []string{
			"aaa", "bbb",
		},
	}
	assert.Equal(t, `Blockly.Lua['test'] = function(block) {
  aaa
  bbb
};
`, block.JS())
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package web

import (
	"sort"
	"time"

	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/internal/http"
	"gitlab.com/go4matic/go4matic/internal/web/admin"
	"gitlab.com/go4matic/go4matic/internal/web/dashboard"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Web base for web interface and dashboard
type Web struct {
	bus core.Bus

	dashboard *dashboard.Web
	admin     *admin.Web
}

// InitializeWeb instance that handles the web interface and dashboard
func InitializeWeb(bus core.Bus, h *http.Handler) error {
	err := bus.RegisterService(new(dashboard.Service))
	if err != nil {
		return err
	}

	w := &Web{
		bus: bus,
		dashboard: &dashboard.Web{
			Bus: bus,
		},
		admin: &admin.Web{
			Bus: bus,
		},
	}

	err = bus.RegisterService(w.admin)
	if err != nil {
		return err
	}

	h.AddBaseEndpoints([]core.PathEntry{{
		Path: "/",
		Handler: func(ctx core.HTTPContext) {
			ctx.Redirect(302, "dash")
		},
	}, {
		Path:       "static",
		FileSystem: assets.Static(),
	}, {
		Path:       "dash",
		Middleware: []func(ctx core.HTTPContext){render, w.dashBase},
		SubEntries: w.dashboard.DashboardEndpoints(),
	}, {
		Middleware: []func(ctx core.HTTPContext){render, w.adminBase},
		Path:       "admin",
		SubEntries: w.admin.AdminEndpoints(),
	}})
	return nil
}

// adminBase prepares handler for admin URLs
func (w *Web) adminBase(ctx core.HTTPContext) {
	ctx.Set("base_name", "admin")

	// get list of loaded plugins
	response := w.bus.HandleRequestWait(core.Request{
		ID: "core.plugins#list",
	}, time.Second)

	if response.Error != nil {
		zap.L().Error(response.Error.Error())
		ctx.Abort()
		return
	}

	// build plugin sub menu
	pluginMenu := make([]menuEntry, 0, len(response.Data))
	keys := make([]string, 0, len(response.Data))
	for key := range response.Data {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	for _, key := range keys {
		pluginMenu = append(pluginMenu, menuEntry{
			Name: key,
			Link: key,
		})
	}

	// create admin menu
	adminMenu := []menuEntry{{
		Name: assets.L.Get("Home"),
		Link: "",
		Icon: "home",
	}, {
		Name: assets.L.Get("Dashboards"),
		Link: "dashboards",
		Icon: "columns",
	}, {
		Name: assets.L.Get("Items"),
		Link: "items",
		Icon: "list",
	}, {
		Name: assets.L.Get("Timers"),
		Link: "timers",
		Icon: "clock",
	}, {
		Name: assets.L.Get("Scripts"),
		Link: "scripts",
		Icon: "scroll",
	}, {
		Name:       assets.L.Get("Plugins"),
		Link:       "plugins",
		Icon:       "plug",
		SubEntries: pluginMenu,
	}}

	if admin.RaspberryEnabled {
		adminMenu = append(adminMenu, menuEntry{
			Name: "Raspberry",
			Link: "raspberry",
			Icon: "tools",
		})
	}

	ctx.Set("menu", adminMenu)
}

// dashBase prepares handler for dashboard URLs
func (w *Web) dashBase(ctx core.HTTPContext) {
	ctx.Set("base_name", "dash")

	response := w.bus.HandleRequestWait(core.Request{
		ID: "core.dashboards#dashboard_list",
	}, time.Second)

	if response.Error != nil {
		// TODO improve
		panic(response.Error)
	}

	// build menu from dashboards
	menu := make([]menuEntry, 0, len(response.Data))
	dashboards := response.Data["dashboards"].([]dashboard.Dashboard)
	for _, dash := range dashboards {
		if dash.Hide {
			continue
		}
		menu = append(menu, menuEntry{
			Name: dash.Name,
			Link: dash.Name,
			Icon: dash.Icon,
		})
	}
	ctx.Set("menu", menu)
	if len(dashboards) > 0 {
		ctx.Set("dash_index", dashboards[0].Name)
	}

	// get current dash board
	name := ctx.Param("name")
	for _, d := range dashboards {
		if name == d.Name {
			ctx.Set("dashboard", &d)
			break
		}
	}

	// show admin link
	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["admin_link"] = true
}

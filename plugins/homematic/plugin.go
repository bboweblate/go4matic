// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package homematic

import (
	"errors"
	"regexp"

	"github.com/spf13/cast"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Plugin that implements homematic functionality
type Plugin struct {
	bus core.Bus

	// list of configured homematic ccus
	ccus map[string]*CCU
}

// ID of plugin
func (p *Plugin) ID() string {
	return "homematic"
}

// Version information of plugin
func (p *Plugin) Version() string {
	return "0.0.0"
}

// Init plugin
func (p *Plugin) Init(b core.Bus) error {
	p.bus = b
	return nil
}

// Shutdown called if plugin should be stopped
func (p *Plugin) Shutdown() {
	for _, c := range p.ccus {
		c.stop()
	}
}

// Config returns the definition of the config interface
func (p *Plugin) Config() *core.PluginConfigDef {
	return &core.PluginConfigDef{
		Entries: core.PluginConfigEntryDefMap{
			"ccus": {
				Config: map[string]api.Field{
					"address": {
						Type:        api.String,
						Required:    true,
						Description: assets.L.Get("Address of ccu"),
					},
					"exclude": {
						Type:         api.String,
						DefaultValue: "(HmIP-RCV-50|HM-RCV-50|HMW-RCV-50).*",
						Description:  assets.L.Get("Regex to exclude entries from item list"),
					},
				},

				LoadEntryFunc: p.configLoad,
				AddEntryFunc:  p.configAdd,
				//ChangeEntryFunc: nil,
				RemoveEntryFunc: p.configRemove,
			},
		},
	}
}

func (p *Plugin) configLoad(entries map[string]map[string]interface{}) error {
	p.ccus = make(map[string]*CCU, len(entries))
	for k, v := range entries {
		regex, err := regexp.Compile(cast.ToString(v["exclude"]))
		if err != nil {
			zap.L().Warn(err.Error())
		}

		ccu, err := NewCCU(cast.ToString(v["address"]), regex)
		if err != nil {
			zap.L().Warn(err.Error())
		}
		p.ccus[k] = ccu

		// start handling
		ccu.start(p.bus)
	}
	return nil
}

func (p *Plugin) configAdd(key string, config map[string]interface{}) error {
	exclude := cast.ToString(config["exclude"])
	regex, err := regexp.Compile(exclude)
	if err != nil {
		return errors.New(assets.L.Get("invalid exclude regex: %v", err))
	}

	address := cast.ToString(config["address"])
	ccu, err := NewCCU(address, regex)
	if err != nil {
		return errors.New(assets.L.Get("no CCU found at %s", address))
	}

	p.ccus[key] = ccu
	ccu.start(p.bus)
	return nil
}

func (p *Plugin) configRemove(key string) error {
	ccu, ok := p.ccus[key]
	if !ok {
		return errors.New(assets.L.Get("no CCU %s", key))
	}
	ccu.stop()
	delete(p.ccus, key)

	for _, dev := range ccu.devices {
		err := core.ItemRemove(p.bus, dev)
		if err != nil {
			zap.L().Error(err.Error())
		}
	}
	return nil
}

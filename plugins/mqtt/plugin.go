// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mqtt

import (
	"errors"
	"sync"

	"github.com/spf13/cast"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Plugin that implements interface to MQTT broker
type Plugin struct {
	bus core.Bus

	broker map[string]*Broker
	items  map[string]core.Item
	values map[string]*Value

	mutex sync.RWMutex
}

// ID of plugin
func (p *Plugin) ID() string {
	return "mqtt"
}

// Version information of plugin
func (p *Plugin) Version() string {
	return "0.5.0"
}

// Init plugin
func (p *Plugin) Init(b core.Bus) error {
	p.bus = b
	return nil
}

// Shutdown called if plugin should be stopped
func (p *Plugin) Shutdown() {
	for _, c := range p.broker {
		c.Close()
	}
}

// Config returns the definition of the config interface
func (p *Plugin) Config() *core.PluginConfigDef {
	return &core.PluginConfigDef{
		Entries: core.PluginConfigEntryDefMap{
			"brokers": {
				Order: 1,
				Config: api.FieldMap{
					"address": {
						Type:        api.String,
						Description: assets.L.Get("URI of MQTT broker"),
						Required:    true,
						Order:       1,
					},
					"username": {
						Type:        api.String,
						Description: assets.L.Get("Username used for connection to broker"),
						Order:       2,
					},
					"password": {
						Type:        api.String,
						Description: assets.L.Get("Password used for connection to broker"),
						Order:       3,
					},
				},
				LoadEntryFunc:   p.loadBrokers,
				AddEntryFunc:    p.addBroker,
				RemoveEntryFunc: p.removeBroker,
			},
			"values": {
				Order: 2,
				Config: api.FieldMap{
					"broker": {
						Type:           api.String,
						Description:    assets.L.Get("Broker key"),
						Required:       true,
						Order:          1,
						PossibleValues: nil,
					},
					"type": {
						Type:           api.String,
						Description:    assets.L.Get("Type of value"),
						Required:       true,
						PossibleValues: nil,
						Order:          2,
					},
					"topic": {
						Type:        api.String,
						Description: assets.L.Get("Topic to read value"),
						Required:    true,
						Order:       3,
					},
					"set_topic": {
						Type:        api.String,
						Description: assets.L.Get("Topic to write value (Default: [topic]/set)"),
						Order:       4,
					},
					"description": {
						Type:        api.String,
						Description: assets.L.Get("Description of value"),
						Order:       5,
					},
				},
				LoadEntryFunc:   p.loadValues,
				AddEntryFunc:    p.addValue,
				RemoveEntryFunc: p.removeValue,
			},
			"item": {
				Order: 3,
				Config: api.FieldMap{
					"name": {
						Type:        api.String,
						Description: assets.L.Get("Name of item"),
						Order:       1,
					},
					"is_actor": {
						Type:         api.Bool,
						Description:  assets.L.Get("True if the item is an actor"),
						DefaultValue: false,
						Order:        2,
					},
					"values": {
						Type:           api.Slice(api.String),
						Description:    assets.L.Get("Values of item"),
						Order:          3,
						PossibleValues: nil,
					},
				},
				LoadEntryFunc:   p.loadItems,
				AddEntryFunc:    p.addItem,
				RemoveEntryFunc: p.removeItem,
			},
		},
	}
}

func (p *Plugin) loadBrokers(entries map[string]map[string]interface{}) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	p.broker = make(map[string]*Broker, len(entries))
	for k, v := range entries {
		broker, err := NewBroker(
			cast.ToString(v["address"]),
			cast.ToString(v["username"]),
			cast.ToString(v["password"]))

		if err != nil {
			return errors.New(assets.L.Get("failed to connect to broker %s: %v", v["address"], err))
		}
		p.broker[k] = broker
	}
	return nil
}

func (p *Plugin) addBroker(key string, config map[string]interface{}) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	broker, err := NewBroker(
		cast.ToString(config["address"]),
		cast.ToString(config["username"]),
		cast.ToString(config["password"]))

	if err != nil {
		return errors.New(assets.L.Get("failed to connect to broker %s: %v", config["address"], err))
	}
	p.broker[key] = broker

	// subscribe existing values for this broker
	for _, value := range p.values {
		if value.broker == key {
			err := broker.Subscribe(value)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (p *Plugin) removeBroker(key string) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	// unsubscribe values from broker
	for _, value := range p.values {
		if value.broker == key {
			p.broker[key].Unsubscribe(value)
		}
	}

	p.broker[key].Close()
	delete(p.broker, key)
	return nil
}

func (p *Plugin) loadValues(entries map[string]map[string]interface{}) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	p.values = make(map[string]*Value, len(entries))
	for k, v := range entries {
		brokerKey := cast.ToString(v["broker"])
		p.values[k] = &Value{
			p:           p,
			broker:      brokerKey,
			key:         k,
			topic:       cast.ToString(v["topic"]),
			setTopic:    cast.ToString(v["set_topic"]),
			description: cast.ToString(v["description"]),
			valueType:   api.TypeFromTypeName(cast.ToString(v["type"])),
		}

		broker, ok := p.broker[brokerKey]
		if ok {
			err := broker.Subscribe(p.values[k])
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (p *Plugin) addValue(key string, config map[string]interface{}) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	brokerKey := cast.ToString(config["broker"])
	p.values[key] = &Value{
		p:           p,
		broker:      brokerKey,
		key:         key,
		topic:       cast.ToString(config["topic"]),
		setTopic:    cast.ToString(config["set_topic"]),
		description: cast.ToString(config["description"]),
		valueType:   api.TypeFromTypeName(cast.ToString(config["type"])),
	}

	broker, ok := p.broker[brokerKey]
	if ok {
		err := broker.Subscribe(p.values[key])
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *Plugin) removeValue(key string) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	broker, ok := p.broker[p.values[key].broker]
	if ok {
		broker.Unsubscribe(p.values[key])
	}
	delete(p.values, key)
	return nil
}

// getValue entry with given key
func (p *Plugin) getValue(key string) *Value {
	p.mutex.RLock()
	defer p.mutex.RUnlock()
	return p.values[key]
}

// setValue with the given key
func (p *Plugin) setValue(key string, value interface{}) error {
	p.mutex.RLock()
	defer p.mutex.RUnlock()

	val, ok := p.values[key]
	if !ok {
		return nil // value does not exist -> ignore
	}

	broker, ok := p.broker[val.broker]
	if !ok {
		return errors.New(assets.L.Get("missing broker %s for value %s", val.broker, key))
	}

	var topic string
	if val.setTopic != "" {
		topic = val.setTopic
	} else {
		topic = val.topic + "/set"
	}

	err := broker.Publish(topic, cast.ToString(value))
	if err != nil {
		return errors.New(assets.L.Get("failed to set value: %v", err))
	}

	return nil
}

func (p *Plugin) loadItems(entries map[string]map[string]interface{}) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	p.items = make(map[string]core.Item, len(entries))
	for k, v := range entries {
		if cast.ToBool(v["is_actor"]) {
			p.items[k] = &ActorItem{
				Item: Item{
					p:      p,
					id:     k,
					name:   cast.ToString(v["name"]),
					values: cast.ToStringSlice(v["values"]),
				},
			}
		} else {
			p.items[k] = &Item{
				p:      p,
				id:     k,
				name:   cast.ToString(v["name"]),
				values: cast.ToStringSlice(v["values"]),
			}
		}

		err := core.ItemAdd(p.bus, p.items[k])
		if err != nil {
			return errors.New(assets.L.Get("failed to add item: %v", err))
		}
	}
	return nil
}

func (p *Plugin) addItem(key string, config map[string]interface{}) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	var item core.Item
	if cast.ToBool(config["is_actor"]) {
		item = &ActorItem{
			Item: Item{
				p:      p,
				id:     key,
				name:   cast.ToString(config["name"]),
				values: cast.ToStringSlice(config["values"]),
			},
		}
	} else {
		item = &Item{
			p:      p,
			id:     key,
			name:   cast.ToString(config["name"]),
			values: cast.ToStringSlice(config["values"]),
		}
	}

	err := core.ItemAdd(p.bus, item)
	if err != nil {
		return errors.New(assets.L.Get("failed to add item: %v", err))
	}

	p.items[key] = item
	return nil
}

func (p *Plugin) removeItem(key string) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	err := core.ItemRemove(p.bus, p.items[key])
	if err != nil {
		zap.L().Error(assets.L.Get("failed to remove item: %v", err))
	}
	delete(p.items, key)
	return nil
}

// triggerItemChange for the given values
func (p *Plugin) triggerItemChange(valueKey string, value interface{}) {
	p.mutex.RLock()
	defer p.mutex.RUnlock()

	for _, item := range p.items {
		i, ok := item.(*Item)
		if ok {
			i.trigger("changed", map[string]interface{}{
				valueKey: value,
			})
		} else {
			ai, ok := item.(*ActorItem)
			if ok {
				ai.trigger("changed", map[string]interface{}{
					valueKey: value,
				})
			}
		}
	}
}

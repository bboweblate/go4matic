// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package core

import (
	"net/http"
	"strings"

	"gitlab.com/go4matic/go4matic/pkg/api"
)

// HTTPContext provides an interface for handling of HTTP requests
type HTTPContext interface {
	// Request from HTTP client
	Request() *http.Request

	// Writer returns HTTP response object
	Writer() http.ResponseWriter

	// Next will execute pending handlers (inside current handler)
	Next()

	// # # # # # parse request # # # # #
	// GetHeader returns value from request headers
	GetHeader(key string) string

	// Header is a intelligent shortcut for c.Writer.Header().Set(key, value).
	// It writes a header in the response.
	// If value == "", this method removes the header `c.Writer.Header().Del(key)`
	Header(key, value string)

	// Param returns the value of the URL param.
	Param(key string) string

	// BodyValue returns value from body.
	BodyValue(key string) interface{}

	// PostForm returns the POST form data
	PostForm(key string) string

	// # # # # # request metadata # # # # #
	// Set store a key value pair for this context
	Set(key string, value interface{})

	// Get returns the value for the given key
	Get(key string) (value interface{}, exists bool)

	// MustGet returns the value for the given key or panics if not exist
	MustGet(key string) interface{}

	// # # # # # manipulate handling # # # # #
	// Abort prevents pending handlers from being called
	Abort()

	// IsAborted returns true if the current context was aborted.
	IsAborted() bool

	// AbortWithStatusJSON calls `Abort()` and then `JSON` internally.
	AbortWithStatusJSON(code int, jsonObj interface{})

	// # # # # # send response # # # # #
	// JSON serializes the given struct as JSON into the response body.
	// It also sets the Content-Type as "application/json".
	JSON(code int, obj interface{})

	// XML serializes the given struct as XML into the response body.
	// It also sets the Content-Type as "application/xml".
	XML(code int, obj interface{})

	// Status sets the HTTP response code.
	Status(code int)

	// Redirect redirect to location
	Redirect(code int, location string)

	// BusResponse writes a bus response to http context
	BusResponse(response Response)
}

// PathEntry defines part of http routing
type PathEntry struct {
	// Definition of entry (only used for endpoint)
	Def api.ExtendedHandler

	// Group of entry (only used for endpoint)
	Group string

	// Path to this entry
	Path string

	// Method that this entry will handle (only used for endpoint)
	Method string

	// FileSystem for handling of static files (can not be used with Handler and SubEntries)
	FileSystem http.FileSystem

	// Middleware to handle before endpoint
	Middleware []func(ctx HTTPContext)

	// Handler if this is an endpoint
	Handler func(ctx HTTPContext)

	// SubEntries under this one
	SubEntries []PathEntry
}

// SimplePathEntryHandler handles simple path entries
type SimplePathEntryHandler func(param map[string]interface{}) (map[string]interface{}, error)

// SimplePathEntry defines simpler path entry variant
type SimplePathEntry struct {
	// Definition of entry
	Def api.SimpleHandler

	// Path to this entry
	Path string

	// Method that this entry will handle
	Method string

	// Handler for this entry
	Handler SimplePathEntryHandler
}

// PathEntry created from simple entry
func (s SimplePathEntry) PathEntry() PathEntry {
	entry := PathEntry{
		Def: api.ExtendedHandler{
			Description: s.Def.Description,
			Parameters:  s.Def.Parameters,
			Responses: map[string]api.Response{
				"200": {
					Data: s.Def.Response,
				},
				"400": api.InvalidRequestResponse,
			},
		},
		Path:   s.Path,
		Method: s.Method,
	}

	param := make(map[string]interface{}, len(entry.Def.Parameters))
	entry.Handler = func(ctx HTTPContext) {
		// parameters to map
		for name := range entry.Def.Parameters {
			if strings.Contains(entry.Path, ":"+name) {
				param[name] = ctx.Param(name)
			} else {
				param[name] = ctx.BodyValue(name)
			}
		}
		// abort if parameter parsing failed
		if ctx.IsAborted() {
			return
		}

		data, err := s.Handler(param)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, map[string]interface{}{
				"error": err.Error(),
			})
		} else {
			if data == nil {
				ctx.Status(http.StatusOK)
			} else {
				ctx.JSON(http.StatusOK, data)
			}
		}
	}
	return entry
}

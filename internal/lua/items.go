// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package lua

import (
	"time"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// ItemsModule for item functionality
type ItemsModule struct {
	bus core.Bus
}

// CreateItemsModule instance and prepare event listener
func CreateItemsModule(bus core.Bus) *ItemsModule {
	return &ItemsModule{
		bus: bus,
	}
}

// Name of module
func (m *ItemsModule) Name() string {
	return "items"
}

// Def return the definition of this module
func (m *ItemsModule) Def() core.ScriptModuleDefinition {
	return core.ScriptModuleDefinition{
		Description: assets.L.Get("Core item functionality"),
		Functions: map[string]core.ScriptFunction{
			"list": {
				Def: api.Function{
					Description: assets.L.Get("Returns list of existing items"),
					Response: api.FieldList{{
						Description: assets.L.Get("List of item IDs"),
						Type:        api.Slice(api.String),
					}},
				},
				Func: m.itemList,
			},
			"set_value": {
				Def: api.Function{
					Description: assets.L.Get("Set value of item"),
					Arguments: api.FieldMap{
						"id": {
							Description: assets.L.Get("Item id"),
							Type:        api.String,
							Required:    true,
							Order:       1,
						},
						"value_name": {
							Description: assets.L.Get("Item value name"),
							Type:        api.String,
							Required:    true,
							Order:       2,
						},
						"value_data": {
							Description: assets.L.Get("Value to set"),
							Type:        api.Any,
							Required:    true,
							Order:       3,
						},
					},
					Response: api.FieldList{{
						Description: assets.L.Get("Error string if failed"),
						Type:        api.Error,
					}},
				},
				Func: m.itemSetValue,
			},
			"get_value": {
				Def: api.Function{
					Description: assets.L.Get("Get value of item"),
					Arguments: api.FieldMap{
						"id": {
							Description: assets.L.Get("Item id"),
							Type:        api.String,
							Required:    true,
							Order:       1,
						},
						"value_name": {
							Description: assets.L.Get("Item value name"),
							Type:        api.String,
							Required:    true,
							Order:       2,
						},
					},
					Response: api.FieldList{{
						Description: assets.L.Get("Value of item or nil on error"),
						Type:        api.Any,
					}},
				},
				Func: m.itemGetValue,
			},
			"log_values": {
				Def: api.Function{
					Description: assets.L.Get("Log values of item"),
					Arguments: api.FieldMap{
						"id": {
							Description: assets.L.Get("Item id"),
							Type:        api.String,
							Required:    true,
							Order:       1,
						},
						"value_id": {
							Description: assets.L.Get("Identifier of values"),
							Type:        api.String,
							Required:    true,
							Order:       2,
						},
						"values": {
							Description: assets.L.Get("Map of values to log"),
							Type:        api.Map(api.String, api.Any),
							Required:    true,
							Order:       3,
						},
					},
					Response: api.FieldList{{
						Description: assets.L.Get("Error string if failed"),
						Type:        api.Error,
					}},
				},
				Func: m.itemLogValue,
			},
		},
	}
}

// itemList returns a IDs of existing items
func (m *ItemsModule) itemList(script core.Script, args []interface{}) []interface{} {
	request := core.Request{
		ID: "core.items#list",
	}

	response := m.bus.HandleRequestWait(request, time.Second)

	items := make([]string, 0, len(response.Data))
	for id := range response.Data {
		items = append(items, id)
	}

	return []interface{}{items}
}

// itemSetValue sets the value of an item
func (m *ItemsModule) itemSetValue(script core.Script, args []interface{}) []interface{} {
	request := core.Request{
		ID: "core.items#set_values",
		Parameters: map[string]interface{}{
			"id": cast.ToString(args[0]),
			"values": map[string]interface{}{
				cast.ToString(args[1]): args[2],
			},
		},
	}

	response := m.bus.HandleRequestWait(request, time.Second)

	// return error message if request failed
	if response.Error != nil {
		return []interface{}{
			response.Error.Error(),
		}
	}

	return []interface{}{nil}
}

// itemGetValue returns a value of an item
func (m *ItemsModule) itemGetValue(script core.Script, args []interface{}) []interface{} {
	request := core.Request{
		ID: "core.items#get_value",
		Parameters: map[string]interface{}{
			"id": cast.ToString(args[0]),
		},
	}

	response := m.bus.HandleRequestWait(request, time.Second)
	// return nil on error
	if response.Error != nil {
		return []interface{}{nil}
	}

	// return nil if value does not exist
	value, ok := response.Data[cast.ToString(args[1])]
	if !ok {
		return []interface{}{nil}
	}

	return []interface{}{value}
}

// itemLogValue log values to item
func (m *ItemsModule) itemLogValue(script core.Script, args []interface{}) []interface{} {
	request := core.Request{
		ID: "core.items#log_values",
		Parameters: map[string]interface{}{
			"id":        cast.ToString(args[0]),
			"values_id": cast.ToString(args[1]),
			"values":    cast.ToStringMap(args[2]),
		},
	}

	response := m.bus.HandleRequestWait(request, time.Second)
	// return error message if request failed
	if response.Error != nil {
		return []interface{}{
			response.Error.Error(),
		}
	}

	return []interface{}{nil}
}

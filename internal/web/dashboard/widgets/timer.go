// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package widgets

import (
	"fmt"
	"strings"
	"time"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// TimerWidget defines time switch widget
type TimerWidget struct {
	Bus core.Bus
}

// TypeID identifies widget type
func (t *TimerWidget) TypeID() string {
	return "timer"
}

// ConfigDef defines configuration of widget
func (t *TimerWidget) ConfigDef() api.FieldMap {
	return api.FieldMap{
		"title": {
			Description: assets.L.Get("Title for timer"),
			Type:        api.String,
		},
		"timer": {
			Description:    assets.L.Get("Key of timer"),
			Type:           api.String,
			Required:       true,
			PossibleValues: t.timerKeys,
		},
		"simple": {
			Description:  assets.L.Get("Simple timer (only ON/OFF)"),
			Type:         api.Bool,
			DefaultValue: true,
		},
	}
}

func (t *TimerWidget) timerKeys() map[interface{}]string {
	response := t.Bus.HandleRequestWait(core.Request{
		ID: "core.timers#list",
	}, time.Second)

	data := make(map[interface{}]string, len(response.Data))
	for key := range response.Data {
		data[key] = key
	}
	return data
}

// RenderVariables returns variables for widget rendering
func (t *TimerWidget) RenderVariables(config map[string]interface{}) (map[string]interface{}, error) {
	// get current state of item
	response := t.Bus.HandleRequestWait(core.Request{
		ID: "core.timers#get",
		Parameters: map[string]interface{}{
			"key": cast.ToString(config["timer"]),
		},
	}, time.Second)

	if response.Error != nil {
		return nil, response.Error
	}

	return map[string]interface{}{
		"times": response.Data["times"],
		"secToTime": func(sec int) string {
			return time.Date(0, 0, 0,
				0, 0, sec, 0, time.UTC).Format("15:04")
		},
		"repeatStr": func(repeat int) string {
			if repeat == 0 {
				return "No"
			}
			list := make([]string, 0, 7)
			if repeat&(0x02) > 0 {
				list = append(list, assets.L.Get("Mo"))
			}
			if repeat&(0x04) > 0 {
				list = append(list, assets.L.Get("Tu"))
			}
			if repeat&(0x08) > 0 {
				list = append(list, assets.L.Get("We"))
			}
			if repeat&(0x10) > 0 {
				list = append(list, assets.L.Get("Th"))
			}
			if repeat&(0x20) > 0 {
				list = append(list, assets.L.Get("Fr"))
			}
			if repeat&(0x40) > 0 {
				list = append(list, assets.L.Get("Sa"))
			}
			if repeat&(0x01) > 0 {
				list = append(list, assets.L.Get("Su"))
			}
			return strings.Join(list, ", ")
		},
		"formatState": func(state interface{}) string {
			if state == nil {
				return ""
			}
			if !cast.ToBool(config["simple"]) {
				return fmt.Sprintf("%v", state)
			}

			if cast.ToBool(state) {
				return "ON"
			}
			return "OFF"
		},
	}, nil
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dummy

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/spf13/cast"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/pkg/api"
)

type dummyItem struct {
	key string

	valInt    int
	valString string
	valBool   bool

	eventTrigger func(action string, data map[string]interface{})
	eventTicker  *time.Ticker
}

func (i *dummyItem) ID() string {
	return "plugin.dummy.item." + i.key
}

func (i *dummyItem) Name() string {
	return "Dummy " + i.key
}

func (i *dummyItem) GetValueTypes() api.FieldMap {
	return api.FieldMap{
		"int": {
			Type:        api.Int,
			Description: "Test integer",
		},
		"string": {
			Type:        api.String,
			Description: "Test string",
		},
		"bool": {
			Type:        api.Bool,
			Description: "Test bool",
		},
	}
}

func (i *dummyItem) GetValues() map[string]interface{} {
	return map[string]interface{}{
		"int":    i.valInt,
		"string": i.valString,
		"bool":   i.valBool,
	}
}

func (i *dummyItem) SetValues(values map[string]interface{}) error {
	changedValues := make(map[string]interface{})
	for key, value := range values {
		switch key {
		case "int":
			i.valInt = cast.ToInt(value)
			changedValues["int"] = i.valInt
		case "string":
			i.valString = cast.ToString(value)
			changedValues["string"] = i.valString
		case "bool":
			i.valBool = cast.ToBool(value)
			changedValues["bool"] = i.valBool
		default:
			return fmt.Errorf("unknown value %s", key)
		}
	}
	if len(changedValues) > 0 {
		i.eventTrigger("changed", changedValues)
	}
	return nil
}

func (i *dummyItem) Events() map[string]api.SimpleHandler {
	return map[string]api.SimpleHandler{
		"trigger": {
			Parameters: api.FieldMap{
				"random": {
					Type: api.Int,
				},
			},
		},
		"changed": {
			Parameters: i.GetValueTypes(),
		},
	}
}

func (i *dummyItem) SetEventTrigger(trigger func(action string, data map[string]interface{})) {
	i.eventTrigger = trigger
}

func (i *dummyItem) LogValues(id string, values map[string]interface{}) error {
	zap.S().With("values", values).Infof("Log %s", id)
	return nil
}

func (i *dummyItem) StartTrigger(interval time.Duration) {
	if i.eventTicker != nil {
		i.eventTicker.Stop()
	}

	i.eventTicker = time.NewTicker(interval)
	go func() {
		for range i.eventTicker.C {
			i.eventTrigger("trigger", map[string]interface{}{
				"random": rand.Int(),
			})
		}
	}()
}
func (i *dummyItem) StopTrigger() {
	if i.eventTicker != nil {
		i.eventTicker.Stop()
	}
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package core

import (
	"errors"
	"fmt"
	"sort"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/pkg/api"
)

// Plugin that provides functionality
type Plugin interface {
	// ID of plugin
	ID() string

	// Version information of plugin
	Version() string

	// Init plugin
	Init(b Bus) error
}

// ShutdownPlugin handles shutdown event
type ShutdownPlugin interface {
	Plugin

	// Shutdown called if plugin should be stopped
	Shutdown()
}

// APIPlugin provides API endpoints
type APIPlugin interface {
	Plugin

	// APIEndpoints returns list of endpoints provided by plugin
	APIEndpoints() []PathEntry
}

// WebPlugin provides web endpoints
type WebPlugin interface {
	Plugin

	// WebEndpoints returns list of endpoints provided by plugin
	WebEndpoints() []PathEntry
}

// ConfigPlugin provides generic configuration
type ConfigPlugin interface {
	Plugin

	// Config returns the definition of the Config interface
	Config() *PluginConfigDef
}

// PluginConfig stores current configuration
type PluginConfig struct {
	// Configuration of current entry
	Config map[string]interface{}

	// configuration of sub entries
	Entries map[string]map[string]map[string]interface{}
}

// Dump configuration
func (c *PluginConfig) Dump() map[string]interface{} {
	data := make(map[string]interface{}, len(c.Config)+len(c.Entries))
	for key, value := range c.Config {
		data[key] = value
	}

	for key, entries := range c.Entries {
		data[key] = entries
	}
	return data
}

// PluginConfigDef defines configuration structure of plugin
type PluginConfigDef struct {
	// Global plugin configuration
	Config api.FieldMap `json:"config,omitempty"`

	// Called to load Config after startup
	LoadFunc func(config map[string]interface{}) error `json:"-"`
	// Called if Config was changed
	ChangeFunc func(config map[string]interface{}) error `json:"-"`

	// Map of sub entry configuration
	Entries PluginConfigEntryDefMap `json:"entries,omitempty"`
}

// Validate if all required functions are provided
func (d *PluginConfigDef) Validate() error {
	if len(d.Config) > 0 {
		if d.LoadFunc == nil {
			return errors.New("missing LoadFunc")
		}
		if d.ChangeFunc == nil {
			return errors.New("missing ChangeFunc")
		}
	}

	for key, entryDef := range d.Entries {
		err := entryDef.Validate()
		if err != nil {
			return fmt.Errorf("%v in entry \"%s\"", err, key)
		}
	}
	return nil
}

// Load configuration from raw data
func (d *PluginConfigDef) Load(data interface{}) (*PluginConfig, error) {
	pc := PluginConfig{}
	dataMap := cast.ToStringMap(data)

	// entry configuration
	if d.Config != nil {
		pc.Config = d.Config.Load(dataMap)

		err := d.LoadFunc(pc.Config)
		if err != nil {
			return nil, err
		}
	}

	// sub entry configuration
	pc.Entries = make(map[string]map[string]map[string]interface{}, len(d.Entries))
	if d.Entries != nil {
		var err error
		for _, entryKey := range d.Entries.SortedKeys() {
			entryDef := d.Entries[entryKey]
			pc.Entries[entryKey], err = entryDef.Load(dataMap[entryKey])
			if err != nil {
				return nil, err
			}
		}
	}

	return &pc, nil
}

// PluginConfigEntryDef defines configuration structure of sub Config entries
type PluginConfigEntryDef struct {
	// Config of entry
	Config api.FieldMap `json:"config,omitempty"`
	// Order index of entry
	Order int

	// Called to load Config entries after startup
	LoadEntryFunc func(entries map[string]map[string]interface{}) error `json:"-"`
	// Called to add a new Config entry
	AddEntryFunc func(key string, config map[string]interface{}) error `json:"-"`
	// Called to change a Config entry (optional)
	ChangeEntryFunc func(key string, config map[string]interface{}) error `json:"-"`
	// Called to remove a Config entry
	RemoveEntryFunc func(key string) error `json:"-"`
}

// Validate if all required functions are provided
func (d *PluginConfigEntryDef) Validate() error {
	if len(d.Config) > 0 {
		if d.LoadEntryFunc == nil {
			return errors.New("missing LoadFunc")
		}
		if d.AddEntryFunc == nil {
			return errors.New("missing AddEntryFunc")
		}
		if d.RemoveEntryFunc == nil {
			return errors.New("missing RemoveEntryFunc")
		}
	}
	return nil
}

// Load configuration from raw data
func (d *PluginConfigEntryDef) Load(data interface{}) (map[string]map[string]interface{}, error) {
	entryMap := cast.ToStringMap(data)
	configs := make(map[string]map[string]interface{}, len(entryMap))

	// entry configuration
	if d.Config != nil {
		for entryKey, entry := range entryMap {
			configs[entryKey] = d.Config.Load(cast.ToStringMap(entry))
		}

		err := d.LoadEntryFunc(configs)
		if err != nil {
			return nil, err
		}
	}

	return configs, nil
}

// PluginConfigEntryDefMap provides a sortable map of PluginConfigEntryDef
type PluginConfigEntryDefMap map[string]PluginConfigEntryDef

// SortedKeys returns list with sorted map keys (uses Order if set)
func (d PluginConfigEntryDefMap) SortedKeys() []string {
	keys := make([]string, 0, len(d))
	for key := range d {
		keys = append(keys, key)
	}

	sort.Slice(keys, func(i, j int) bool {
		f1 := d[keys[i]]
		f2 := d[keys[j]]
		if f1.Order == f2.Order {
			return keys[i] < keys[j]
		}
		return f1.Order < f2.Order
	})

	return keys
}

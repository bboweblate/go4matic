// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

import "testing"

func TestControlsRepeatExt(t *testing.T) {
	equalXML(t, `<block type="controls_repeat_ext">
  <value name="TIMES">
    <shadow type="math_number">
      <field name="NUM">10</field>
    </shadow>
  </value>
</block>`, ControlsRepeatExt())
}

func TestControlsWhileUntil(t *testing.T) {
	equalXML(t, `<block type="controls_whileUntil">
  <field name="MODE">WHILE</field>
</block>`, ControlsWhileUntil())
}

func TestControlsFor(t *testing.T) {
	equalXML(t, `<block type="controls_for">
  <field name="VAR">i</field>
  <value name="FROM">
    <shadow type="math_number">
      <field name="NUM">1</field>
    </shadow>
  </value>
  <value name="TO">
    <shadow type="math_number">
      <field name="NUM">10</field>
    </shadow>
  </value>
  <value name="BY">
    <shadow type="math_number">
      <field name="NUM">1</field>
    </shadow>
  </value>
</block>`, ControlsFor())
}

func TestControlsForEach(t *testing.T) {
	equalXML(t, `<block type="controls_forEach">
  <field name="VAR">j</field>
</block>`, ControlsForEach())
}

func TestControlsFlowStatements(t *testing.T) {
	equalXML(t, `<block type="controls_flow_statements">
  <field name="FLOW">BREAK</field>
</block>`, ControlsFlowStatements())
}

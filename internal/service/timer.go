// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"errors"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/spf13/cast"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// possible weekdays
const (
	Sunday int = 1 << iota
	Monday
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday

	Everyday = Sunday | Monday | Tuesday | Wednesday | Thursday | Friday | Saturday
)

// TimeEntry holds information about next trigger time
type TimeEntry struct {
	Active bool        `json:"active"`
	Time   int         `json:"time"` // seconds since midnight
	Repeat int         `json:"repeat"`
	State  interface{} `json:"state"`
}

// DaysToWait returns the amount of days between today and trigger
func (e *TimeEntry) DaysToWait(today time.Weekday) int {
	if e.Repeat == 0 {
		return 0 // same day
	}

	var days int
	for days = 0; days <= 7; days++ {
		if e.Repeat&(1<<uint(today)) > 0 {
			break
		}

		today++
		if today > time.Saturday {
			today = time.Sunday
		}
	}

	return days
}

// Next returns the next time for trigger
func (e *TimeEntry) Next(now time.Time) time.Time {
	// if not active return "invalid" date
	if !e.Active {
		return time.Unix(0, 0)
	}

	// get nano seconds since midnight
	t := int64(now.Hour()*3600+now.Minute()*60+now.Second())*1e9 + int64(now.Nanosecond())

	// check if this entry can trigger today
	weekdayStart := now.Weekday()
	days := 0
	if int64(e.Time)*1e9 < t { // skip today -> time gone
		weekdayStart++
		days++
	}

	// get days until next trigger
	days += e.DaysToWait(weekdayStart)

	return time.Date(
		now.Year(), now.Month(), now.Day()+days,
		0, 0, e.Time, 0, now.Location())
}

// Timer triggers on time entries
type Timer struct {
	// list of time entries
	times []*TimeEntry

	// item values to set on trigger
	itemValues []string

	// mutex for synchronisation of item values
	mutex sync.RWMutex

	// channel to communicate with run function
	stop        chan struct{}
	add         chan TimeEntry
	remove      chan int
	getRequest  chan struct{}
	getResponse chan []TimeEntry

	// true if running
	running bool

	// key of this timer
	key string

	// instance of timer service
	service *TimerService
}

// Start the timer handling
func (t *Timer) Start() {
	if t.running {
		return
	}
	t.running = true
	go t.run()
}

// Stop the timer handling
func (t *Timer) Stop() {
	if !t.running {
		return
	}

	t.running = false
	t.stop <- struct{}{}
}

// AddEntry to timer
func (t *Timer) AddEntry(entry TimeEntry) {
	t.add <- entry
}

// RemoveEntry from timer
func (t *Timer) RemoveEntry(idx int) {
	t.remove <- idx
}

// ListEntries return time entries
func (t *Timer) ListEntries() []TimeEntry {
	t.getRequest <- struct{}{}
	return <-t.getResponse
}

// AddItemValue to set on trigger event
func (t *Timer) AddItemValue(itemValue string) {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	for _, value := range t.itemValues {
		if value == itemValue {
			return
		}
	}
	t.itemValues = append(t.itemValues, itemValue)
}

// RemoveItemValue from timer
func (t *Timer) RemoveItemValue(itemValue string) {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	values := make([]string, 0)
	for _, value := range t.itemValues {
		if value != itemValue {
			values = append(values, value)
		}
	}
	t.itemValues = values
}

// Dump timer information
func (t *Timer) Dump() map[string]interface{} {
	return map[string]interface{}{
		"times":       t.ListEntries(),
		"item_values": t.itemValues,
	}
}

// run main loop
func (t *Timer) run() {
	now := time.Now()
	for {
		var nextEntries []*TimeEntry
		nextTime := now.AddDate(0, 1, 0)

		// get next entries
		for _, entry := range t.times {
			if !entry.Active {
				continue
			}

			et := entry.Next(now)
			sub := et.Sub(nextTime)
			if sub < 0 {
				nextEntries = []*TimeEntry{entry}
				nextTime = et
			} else if sub == 0 {
				nextEntries = append(nextEntries, entry)
			}
		}

		// check if timer found
		var timer *time.Timer
		if len(nextEntries) == 0 {
			timer = time.NewTimer(100000 * time.Hour)
		} else {
			timer = time.NewTimer(nextTime.Sub(now))
		}

		select {
		case now = <-timer.C: // timer triggered
			t.triggerEntries(nextEntries)

		case entry := <-t.add: // add new time entry
			now = time.Now()

			t.times = append(t.times, &entry)
			sort.Slice(t.times, func(i, j int) bool {
				return t.times[i].Time < t.times[j].Time
			})

			t.service.trigger("core.timers#changed", map[string]interface{}{
				"key": t.key,
			})

		case idx := <-t.remove: // remove time entry
			now = time.Now()

			t.times = append(t.times[:idx], t.times[idx+1:]...)

			t.service.trigger("core.timers#changed", map[string]interface{}{
				"key": t.key,
			})

		case <-t.getRequest: // handle request of entry list
			now = time.Now()
			entries := make([]TimeEntry, len(t.times))
			for idx, entry := range t.times {
				entries[idx] = *entry
			}
			t.getResponse <- entries

		case <-t.stop: // stop main loop
			timer.Stop()
			return
		}
	}
}

// triggerEntries if given and active
func (t *Timer) triggerEntries(entries []*TimeEntry) {
	if len(entries) > 0 {
		for _, entry := range entries {
			if entry.Repeat == 0 {
				entry.Active = false
			}
			t.trigger(entry.State)
		}
	}
}

// trigger handles a trigger from timer and set states of item values and trigger event
func (t *Timer) trigger(state interface{}) {
	t.mutex.RLock()
	defer t.mutex.RUnlock()
	t.service.trigger("core.timers#trigger", map[string]interface{}{
		"key":   t.key,
		"state": state,
	})

	for _, val := range t.itemValues {
		err := t.setState(val, state)
		if err != nil {
			zap.L().Error(err.Error())
		}
	}
}

// setState of the given item value
func (t *Timer) setState(itemValue string, state interface{}) error {
	item := strings.Split(itemValue, "#")
	if len(item) < 2 {
		return errors.New(assets.L.Get("invalid item_value should be [ITEM_ID]#[VALUE] given %s", itemValue))
	}

	return t.service.bus.HandleRequestWait(core.Request{
		ID: "core.items#set_values",
		Parameters: map[string]interface{}{
			"id": item[0],
			"values": map[string]interface{}{
				item[1]: state,
			},
		},
	}, time.Second).Error
}

// TimerService handles timer objects
type TimerService struct {
	// map of created timers
	timers map[string]*Timer

	// mutex for timers
	mutex sync.RWMutex

	// trigger functions for events
	trigger core.EventTrigger

	bus core.Bus
}

// Key that identifies this service
func (s *TimerService) Key() string {
	return "core.timers"
}

// Events returns all events that can be emitted
func (s *TimerService) Events() map[string]api.SimpleHandler {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	return map[string]api.SimpleHandler{
		"core.timers#trigger": {
			Description: assets.L.Get("Emitted on timer trigger"),
			Parameters: map[string]api.Field{
				"key": {
					Description: assets.L.Get("Key of timer"),
					Type:        api.String,
				},
				"state": {
					Description: assets.L.Get("State of timer entry"),
				},
			},
		},
		"core.timers#changed": {
			Description: assets.L.Get("Emitted if timer changed"),
			Parameters: map[string]api.Field{
				"key": {
					Description: assets.L.Get("Key of timer"),
					Type:        api.String,
				},
			},
		},
	}
}

// SetEventTrigger for service
func (s *TimerService) SetEventTrigger(trigger core.EventTrigger) {
	s.trigger = trigger
}

// Init load the service
func (s *TimerService) Init(bus core.Bus) error {
	s.bus = bus

	value, err := core.ConfigRead(bus, "core.timers")
	if err != nil {
		return err
	}

	timersRaw := cast.ToStringMap(value)
	s.timers = make(map[string]*Timer, len(timersRaw))
	for key, info := range timersRaw {
		infoData := cast.ToStringMap(info)

		timesRaw := cast.ToSlice(infoData["times"])
		times := make([]*TimeEntry, len(timesRaw))
		for idx, entry := range timesRaw {
			data := cast.ToStringMap(entry)
			times[idx] = &TimeEntry{
				cast.ToBool(data["active"]),
				cast.ToInt(data["time"]),
				cast.ToInt(data["repeat"]),
				data["state"],
			}
		}

		s.timers[key] = s.newTimer(key, times, cast.ToStringSlice(infoData["item_values"]))
	}
	zap.L().Info(assets.L.GetN(
		"Loaded %d timer", "Loaded %d timers",
		len(s.timers), len(s.timers)))
	return nil
}

// Start timers
func (s *TimerService) Start(bus core.Bus) error {
	for _, timer := range s.timers {
		timer.Start()
	}
	return nil
}

// Update saves actual timers
func (s *TimerService) Update(bus core.Bus) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	data := make(map[string]interface{}, len(s.timers))
	for key, timer := range s.timers {
		data[key] = timer.Dump()
	}

	err := core.ConfigWrite(bus, "core.timers", data)
	if err != nil {
		zap.L().Error(err.Error())
	}
}

// Shutdown saves actual timers
func (s *TimerService) Shutdown(bus core.Bus) {
	s.Update(bus)
	for _, timer := range s.timers {
		timer.Stop()
	}
}

// Handlers for this service
func (s *TimerService) Handlers() map[string]core.RequestHandler {
	infoFields := api.FieldMap{
		"item_values": {
			Description: assets.L.Get("List of item values"),
			Type:        api.Slice(api.String),
		},
		"times": {
			Description: assets.L.Get("List of time entries"),
			Type: api.FieldMap{
				"{idx}": {
					Type: api.FieldMap{
						"active": {
							Description: assets.L.Get("True if entry is enabled"),
							Type:        api.Bool,
							Required:    true,
						},
						"time": {
							Description: assets.L.Get("Time of entry"),
							Type:        api.Int,
							Required:    true,
						},
						"repeat": {
							Description: assets.L.Get("Repeat weekday or 0 for onetime"),
							Type:        api.Int,
							Required:    true,
						},
						"state": {
							Description: assets.L.Get("State value for this entry"),
							Type:        api.Any,
							Required:    true,
						},
					},
				},
			},
		},
	}
	return map[string]core.RequestHandler{
		"add": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Add a new timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
				},
			},
			Func: s.addTimer,
		},
		"remove": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
				},
			},
			Func: s.removeTimer,
		},
		"list": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("List all timers"),
				Response: map[string]api.Field{
					"{key}": {
						Description: assets.L.Get("Information of timer"),
						Type:        infoFields,
					},
				},
			},
			Func: s.listTimer,
		},
		"get": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Get timer information"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
				},
				Response: infoFields,
			},
			Func: s.getTimer,
		},

		"add_time": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Add time to timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},

					"active": {
						Description:  assets.L.Get("True if entry is enabled"),
						Type:         api.Bool,
						DefaultValue: true,
					},
					"time": {
						Description: assets.L.Get("Time of entry"),
						//Type: api.Int,
						Required: true,
					},
					"repeat": {
						Description:  assets.L.Get("Repeat weekday or 0 for onetime"),
						Type:         api.Int,
						DefaultValue: Everyday,
					},
					"state": {
						Description: assets.L.Get("State value for this entry"),
						Type:        api.Any,
					},
				},
			},
			Func: s.addTimeEntry,
		},
		"edit_time": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Edit time of timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
					"idx": {
						Description: assets.L.Get("Index of time entry"),
						Type:        api.Uint,
						Required:    true,
					},

					"active": {
						Description: assets.L.Get("True if entry is enabled"),
						Type:        api.Bool,
					},
					"time": {
						Description: assets.L.Get("Time of entry"),
						//Type: api.Int,
					},
					"repeat": {
						Description: assets.L.Get("Repeat weekday or 0 for onetime"),
						Type:        api.Uint8,
					},
					"state": {
						Description: assets.L.Get("State value for this entry"),
						Type:        api.Any,
					},
				},
			},
			Func: s.editTimeEntry,
		},
		"remove_time": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove time of timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
					"idx": {
						Description: assets.L.Get("Index of time entry"),
						Type:        api.Uint,
						Required:    true,
					},
				},
			},
			Func: s.removeTimeEntry,
		},

		"add_item_value": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Add item value to timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
					"item_value": {
						Description: assets.L.Get("Item value"),
						Type:        api.String,
						Required:    true,
					},
				},
			},
			Func: s.addItemValue,
		},
		"remove_item_value": {
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove item value from timer"),
				Parameters: api.FieldMap{
					"key": {
						Description: assets.L.Get("Identifier of timer"),
						Type:        api.String,
						Required:    true,
					},
					"item_value": {
						Description: assets.L.Get("Item value"),
						Type:        api.String,
						Required:    true,
					},
				},
			},
			Func: s.removeItemValue,
		},
	}
}

// APIEndpoints for service
func (s *TimerService) APIEndpoints(bus core.Bus) []core.PathEntry {
	return []core.PathEntry{{
		Path: "/timers",
		SubEntries: []core.PathEntry{
			core.APIWrapper(bus, s, "list", core.PathEntry{
				Path:  "/",
				Group: "Core",
			}),
			core.APIWrapper(bus, s, "add", core.PathEntry{
				Path:   "/",
				Group:  "Core",
				Method: "POST",
			}),
			core.APIWrapper(bus, s, "get", core.PathEntry{
				Path:  "/:key",
				Group: "Core",
			}),
			core.APIWrapper(bus, s, "remove", core.PathEntry{
				Path:   "/:key",
				Group:  "Core",
				Method: "DELETE",
			}),

			core.APIWrapper(bus, s, "add_time", core.PathEntry{
				Path:   "/:key/times",
				Group:  "Core",
				Method: "POST",
			}),
			core.APIWrapper(bus, s, "edit_time", core.PathEntry{
				Path:   "/:key/times/:idx",
				Group:  "Core",
				Method: "PUT",
			}),
			core.APIWrapper(bus, s, "remove_time", core.PathEntry{
				Path:   "/:key/times/:idx",
				Group:  "Core",
				Method: "DELETE",
			}),

			core.APIWrapper(bus, s, "add_item_value", core.PathEntry{
				Path:   "/:key/item_values",
				Group:  "Core",
				Method: "POST",
			}),
			core.APIWrapper(bus, s, "remove_item_value", core.PathEntry{
				Path:   "/:key/item_values/:item_value",
				Group:  "Core",
				Method: "DELETE",
			}),
		},
	}}
}

// newTimer creates a new empty timer
func (s *TimerService) newTimer(key string, times []*TimeEntry, itemValues []string) *Timer {
	if times == nil {
		times = make([]*TimeEntry, 0)
	}
	if itemValues == nil {
		itemValues = make([]string, 0)
	}

	return &Timer{
		stop:        make(chan struct{}),
		add:         make(chan TimeEntry),
		remove:      make(chan int),
		getRequest:  make(chan struct{}),
		getResponse: make(chan []TimeEntry),
		service:     s,
		key:         key,
		times:       times,
		itemValues:  itemValues,
	}
}

func (s *TimerService) addTimer(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	key := cast.ToString(data["key"])
	_, ok := s.timers[key]
	if ok {
		return nil, errors.New(assets.L.Get("timer with key %s already exist", key))
	}

	timer := s.newTimer(key, nil, nil)
	timer.Start()

	s.timers[key] = timer

	return nil, nil
}

func (s *TimerService) removeTimer(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	key := cast.ToString(data["key"])
	timer, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	delete(s.timers, key)
	timer.Stop()

	return nil, nil
}

func (s *TimerService) listTimer(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	timers := make(map[string]interface{}, len(s.timers))
	for key, timer := range s.timers {
		timers[key] = timer.Dump()
	}
	return timers, nil
}

func (s *TimerService) getTimer(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	key := cast.ToString(data["key"])
	timer, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	return timer.Dump(), nil
}

// convertTime from interface to int (seconds since midnight)
func convertTime(raw interface{}) int {
	timeStr := cast.ToString(raw)
	if strings.Contains(timeStr, ":") {
		t := 0
		parts := strings.SplitN(timeStr, ":", 3)
		for _, e := range parts {
			i, _ := strconv.Atoi(e)
			t = t*60 + i
		}

		// no seconds given?
		if len(parts) < 3 {
			t = t * 60
		}
		return t
	}
	return cast.ToInt(raw)
}

func (s *TimerService) addTimeEntry(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	key := cast.ToString(data["key"])
	timer, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	timer.AddEntry(TimeEntry{
		Active: cast.ToBool(data["active"]),
		Time:   convertTime(data["time"]),
		Repeat: cast.ToInt(data["repeat"]),
		State:  data["state"],
	})

	return nil, nil
}

func (s *TimerService) editTimeEntry(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	key := cast.ToString(data["key"])
	timer, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	idx := cast.ToInt(data["idx"])
	entries := timer.ListEntries()
	if idx < 0 || idx >= len(entries) {
		return nil, errors.New(assets.L.Get("invalid time entry index %d", idx))
	}
	entry := entries[idx]

	// remove old entry
	timer.RemoveEntry(idx)

	if active, ok := data["active"]; ok {
		entry.Active = cast.ToBool(active)
	}

	if dataTime, ok := data["time"]; ok {
		entry.Time = convertTime(dataTime)
	}

	if repeat, ok := data["repeat"]; ok {
		entry.Repeat = cast.ToInt(repeat)
	}

	if state, ok := data["state"]; ok {
		entry.State = state
	}

	// add entry again
	timer.AddEntry(entry)
	return nil, nil
}

func (s *TimerService) removeTimeEntry(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	key := cast.ToString(data["key"])
	timer, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	idx := cast.ToInt(data["idx"])
	entries := timer.ListEntries()
	if idx < 0 || idx >= len(entries) {
		return nil, errors.New(assets.L.Get("invalid time entry index %d", idx))
	}

	// remove old entry
	timer.RemoveEntry(idx)

	return nil, nil
}

func (s *TimerService) addItemValue(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	key := cast.ToString(data["key"])
	timer, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	timer.AddItemValue(cast.ToString(data["item_value"]))
	return nil, nil
}

func (s *TimerService) removeItemValue(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	key := cast.ToString(data["key"])
	timer, ok := s.timers[key]
	if !ok {
		return nil, errors.New(assets.L.Get("no timer with key %s", key))
	}

	timer.RemoveItemValue(cast.ToString(data["item_value"]))
	return nil, nil
}

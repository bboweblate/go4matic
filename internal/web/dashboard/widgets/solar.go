// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package widgets

import (
	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// SolarWidget defines solar widget
type SolarWidget struct {
	Bus core.Bus
}

// TypeID identifies widget type
func (w *SolarWidget) TypeID() string {
	return "solar"
}

// ConfigDef defines configuration of widget
func (w *SolarWidget) ConfigDef() api.FieldMap {
	return api.FieldMap{
		"supplier_plus": {
			Description: assets.L.Get("Item value for positive supplier power"),
			Type:        core.SensorItemValueType,
			Required:    true,
		},
		"supplier_minus": {
			Description: assets.L.Get("Item value for negative supplier power"),
			Type:        core.SensorItemValueType,
			Required:    true,
		},
		"solar_power": {
			Description: assets.L.Get("Item value for solar power"),
			Type:        core.SensorItemValueType,
			Required:    true,
		},
		"battery_power": {
			Description: assets.L.Get("Item value for battery power"),
			Type:        core.SensorItemValueType,
			Required:    true,
		},
		"battery_charge": {
			Description: assets.L.Get("Item value for battery charge"),
			Type:        core.SensorItemValueType,
			Required:    true,
		},
	}
}

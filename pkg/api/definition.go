// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"encoding/json"
	"errors"
)

// Field information for a value
type Field struct {
	// Type of value
	// 0/Invalid for undefined/any type
	Type Type

	// Description of field
	Description string

	// True if field is required
	Required bool

	// Default value if field is not required
	DefaultValue interface{}

	// Order index of field (should not be 0 if used)
	Order uint

	// Possible values for this field
	//  key: value for field
	//  value: description of value (or value as fallback)
	PossibleValues func() map[interface{}]string
}

// TypeName return the name of the type
func (f Field) TypeName() string {
	if f.Type == nil {
		return Any.TypeName()
	}
	return f.Type.TypeName()
}

// MarshalJSON converts Field to JSON
func (f Field) MarshalJSON() ([]byte, error) {
	t := f.Type
	if t == nil {
		t = Any
	}

	return json.Marshal(struct {
		Type         string      `json:"type"`
		BaseType     string      `json:"base_type"`
		Description  string      `json:"description"`
		Required     bool        `json:"required"`
		DefaultValue interface{} `json:"default_value,omitempty"`
	}{
		Type:         t.TypeName(),
		BaseType:     t.BaseName(),
		Description:  f.Description,
		Required:     f.Required,
		DefaultValue: f.DefaultValue,
	})
}

// Response information
type Response struct {
	Description string

	// Data fields inside response
	Data FieldMap
}

// Function with positional arguments and response
type Function struct {
	Description string
	Arguments   FieldMap // order should be defined
	Response    FieldList
}

// ValidateArguments check if data matches with defined parameters and add defaults to data
func (f *Function) ValidateArguments(data []interface{}) ([]interface{}, error) {
	if f.Arguments == nil {
		return data, nil
	}

	// handle arguments as field list
	list := make(FieldList, len(f.Arguments))
	for idx, key := range f.Arguments.SortedKeys() {
		list[idx] = f.Arguments[key]
	}
	return list.Validate(data)
}

// Convert given value to type if possible
func (f *Function) Convert(value interface{}) (interface{}, error) {
	_, ok := value.(func([]interface{}) []interface{})
	if !ok {
		return nil, errors.New("not implemented")
	}
	return value, nil
}

// Check if the value matches the type
func (f *Function) Check(value interface{}) bool {
	_, ok := value.(func([]interface{}) []interface{})
	return ok
}

// TypeName return the name of the type
func (f *Function) TypeName() string {
	return "func"
}

// BaseName return the name of the type category
func (f *Function) BaseName() string {
	return "Function"
}

// SimpleHandler with named parameters and a single response
type SimpleHandler struct {
	Description string
	Parameters  FieldMap
	Response    FieldMap
}

// ValidateParameters check if data matches with defined parameters and add defaults to data
func (f *SimpleHandler) ValidateParameters(data map[string]interface{}) (map[string]interface{}, error) {
	if f.Parameters == nil {
		return data, nil
	}
	return f.Parameters.Validate(data)
}

// ExtendedHandler definition
type ExtendedHandler struct {
	Description string
	Parameters  FieldMap
	Responses   map[string]Response
}

// ValidateParameters check if data matches with defined parameters and add defaults to data
func (f *ExtendedHandler) ValidateParameters(data map[string]interface{}) (map[string]interface{}, error) {
	if f.Parameters == nil {
		return data, nil
	}
	return f.Parameters.Validate(data)
}

// InvalidRequestResponse defines response for invalid/failed requests
var InvalidRequestResponse = Response{
	Description: "invalid request",
	Data: FieldMap{
		"error": {
			Type:        String,
			Description: "Error message",
			Required:    true,
		},
	},
}

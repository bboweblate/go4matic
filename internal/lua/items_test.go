// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package lua

import (
	"errors"
	"sort"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/go4matic/go4matic/pkg/core"
)

func TestCreateItemsModule(t *testing.T) {
	ass := assert.New(t)

	bus := core.CreateBus()
	defer bus.Shutdown()

	module := CreateCoreModule(bus)

	ass.Equal(bus, module.bus)
}

func TestItemsModule_Name(t *testing.T) {
	ass := assert.New(t)

	module := ItemsModule{}

	ass.Equal("items", module.Name())
}

func TestItemsModule_Def(t *testing.T) {
	ass := assert.New(t)

	module := ItemsModule{}
	def := module.Def()

	ass.Len(def.Functions, 4)
	ass.Contains(def.Functions, "list")
	ass.Contains(def.Functions, "set_value")
	ass.Contains(def.Functions, "get_value")
	ass.Contains(def.Functions, "log_values")
}

func TestItemsModule_itemList(t *testing.T) {
	ass := assert.New(t)

	bus := new(core.TestBus)
	bus.HandleRequestWaitFunc = func(request core.Request, timeout time.Duration) core.Response {
		ass.Equal("core.items#list", request.ID)
		ass.Equal(time.Second, timeout)
		return core.Response{
			OrgRequest: request,
			Data: map[string]interface{}{
				"aaa": nil,
				"bbb": nil,
				"ccc": nil,
			},
		}
	}

	module := CreateItemsModule(bus)
	response := module.itemList(nil, nil)

	ass.Len(response, 1)
	sort.Strings(response[0].([]string))
	ass.Equal([]string{"aaa", "bbb", "ccc"}, response[0])
}

func TestItemsModule_itemSetValue(t *testing.T) {
	ass := assert.New(t)

	var parameters map[string]interface{}
	var returnErr error
	bus := new(core.TestBus)
	bus.HandleRequestWaitFunc = func(request core.Request, timeout time.Duration) core.Response {
		ass.Equal("core.items#set_values", request.ID)
		ass.Equal(time.Second, timeout)
		parameters = request.Parameters
		return core.Response{
			OrgRequest: request,
			Error:      returnErr,
		}
	}

	module := CreateItemsModule(bus)

	response := module.itemSetValue(nil, []interface{}{"aaa", "bbb", "ccc"})
	ass.Len(response, 1)
	ass.Nil(response[0])
	ass.Equal(map[string]interface{}{
		"id": "aaa",
		"values": map[string]interface{}{
			"bbb": "ccc",
		},
	}, parameters)

	returnErr = errors.New("error")

	response = module.itemSetValue(nil, []interface{}{"aaa", "bbb", "ccc"})
	ass.Len(response, 1)
	ass.Equal(response[0], "error")
}

func TestItemsModule_itemGetValue(t *testing.T) {
	ass := assert.New(t)

	var parameters map[string]interface{}
	var returnData map[string]interface{}
	var returnErr error
	bus := new(core.TestBus)
	bus.HandleRequestWaitFunc = func(request core.Request, timeout time.Duration) core.Response {
		ass.Equal("core.items#get_value", request.ID)
		ass.Equal(time.Second, timeout)
		parameters = request.Parameters
		return core.Response{
			OrgRequest: request,
			Data:       returnData,
			Error:      returnErr,
		}
	}

	module := CreateItemsModule(bus)

	returnErr = errors.New("error")

	response := module.itemGetValue(nil, []interface{}{"aaa", "bbb"})
	ass.Len(response, 1)
	ass.Nil(response[0])
	ass.Equal(map[string]interface{}{
		"id": "aaa",
	}, parameters)

	returnErr = nil
	returnData = map[string]interface{}{
		"ccc": "ccc",
	}

	response = module.itemGetValue(nil, []interface{}{"aaa", "bbb"})
	ass.Len(response, 1)
	ass.Nil(response[0])

	returnErr = nil
	returnData = map[string]interface{}{
		"bbb": "ccc",
	}

	response = module.itemGetValue(nil, []interface{}{"aaa", "bbb"})
	ass.Len(response, 1)
	ass.Equal("ccc", response[0])
}

func TestItemsModule_itemLogValue(t *testing.T) {
	ass := assert.New(t)

	var parameters map[string]interface{}
	var returnErr error
	bus := new(core.TestBus)
	bus.HandleRequestWaitFunc = func(request core.Request, timeout time.Duration) core.Response {
		ass.Equal("core.items#log_values", request.ID)
		ass.Equal(time.Second, timeout)
		parameters = request.Parameters
		return core.Response{
			OrgRequest: request,
			Error:      returnErr,
		}
	}

	module := CreateItemsModule(bus)

	response := module.itemLogValue(nil, []interface{}{
		"aaa", "bbb",
		map[string]interface{}{
			"aaa": 111,
			"bbb": 222,
		}})
	ass.Len(response, 1)
	ass.Nil(response[0])
	ass.Equal(map[string]interface{}{
		"id":        "aaa",
		"values_id": "bbb",
		"values": map[string]interface{}{
			"aaa": 111,
			"bbb": 222,
		},
	}, parameters)

	returnErr = errors.New("error")

	response = module.itemLogValue(nil, []interface{}{
		"aaa", "bbb",
		map[string]interface{}{
			"aaa": 111,
			"bbb": 222,
		}})
	ass.Len(response, 1)
	ass.Equal(response[0], "error")
}

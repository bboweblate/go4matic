// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package plugin

import (
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	testifyHttp "github.com/stretchr/testify/http"

	"gitlab.com/go4matic/go4matic/internal/http"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

type TestPlugin struct {
	bus core.Bus
}

func (p *TestPlugin) ID() string {
	return "test1"
}

func (p *TestPlugin) Version() string {
	return "0.0.0"
}

func (p *TestPlugin) Init(b core.Bus) error {
	p.bus = b
	return nil
}

type TestPluginShutdown struct {
	TestPlugin

	shutdown bool
}

func (p *TestPluginShutdown) ID() string {
	return "test2"
}

func (p *TestPluginShutdown) Shutdown() {
	p.shutdown = true
}

type TestPluginAPI struct {
	TestPlugin
}

func (p *TestPluginAPI) APIEndpoints() []core.PathEntry {
	return []core.PathEntry{{Path: "/aaa"}}
}
func (p *TestPluginAPI) ID() string {
	return "test3"
}

type TestPluginWeb struct {
	TestPlugin
}

func (p *TestPluginWeb) WebEndpoints() []core.PathEntry {
	return []core.PathEntry{{Path: "/web"}}
}
func (p *TestPluginWeb) ID() string {
	return "test4"
}

func TestLoader_Key(t *testing.T) {
	loader := Loader{}
	assert.Equal(t, "core.plugins", loader.Key())
}

func TestLoader_Load(t *testing.T) {
	bus := new(core.TestBus)
	plugin := &TestPlugin{}
	staticPlugins = []core.Plugin{plugin, plugin}

	loader := Loader{}
	loader.Load(bus)

	ass := assert.New(t)
	ass.Equal(bus, plugin.bus)
	ass.Equal(plugin, loader.plugins["test1"].plugin)

	// check handlerPluginGet

	_, err := loader.handlerPluginGet(map[string]interface{}{
		"plugin_id": "test2",
	})
	ass.Error(err, "no plugin with ID test2")

	data, err := loader.handlerPluginGet(map[string]interface{}{
		"plugin_id": "test1",
	})
	ass.NoError(err)
	ass.Equal(map[string]interface{}{
		"id":      plugin.ID(),
		"version": plugin.Version(),
	}, data)

}
func TestLoader_pluginHandler(t *testing.T) {
	ass := assert.New(t)

	staticPlugins = []core.Plugin{&TestPlugin{}}

	loader := Loader{}
	loader.Load(new(core.TestBus))

	handler := loader.pluginHandler(0)

	_, err := handler(map[string]interface{}{
		"plugin_id": "test2",
	})
	ass.EqualError(err, "no plugin with ID test2")

	_, err = handler(map[string]interface{}{
		"plugin_id": "test1",
	})
	ass.EqualError(err, "unknown function 0")

	// more test requires fully loaded config plugin
}

func TestLoader_Handlers(t *testing.T) {
	ass := assert.New(t)

	loader := Loader{}
	handlers := loader.Handlers()

	ass.Len(handlers, 9)
	ass.Contains(handlers, "list")
	ass.Contains(handlers, "get_info")

	ass.Contains(handlers, "config_get")
	ass.Contains(handlers, "config_change")

	ass.Contains(handlers, "config_entry_list")
	ass.Contains(handlers, "config_entry_get")
	ass.Contains(handlers, "config_entry_add")
	ass.Contains(handlers, "config_entry_change")
	ass.Contains(handlers, "config_entry_remove")
}

func TestLoader_APIEndpoints(t *testing.T) {
	staticPlugins = []core.Plugin{&TestPlugin{}, &TestPluginAPI{}}

	loader := Loader{}
	loader.Load(new(core.TestBus))

	endpoints := loader.APIEndpoints()

	ass := assert.New(t)
	ass.Len(endpoints, 1)
	ass.Len(endpoints[0].SubEntries, 2)

	if endpoints[0].SubEntries[0].Path == "/test3/" {
		ass.Len(endpoints[0].SubEntries[0].SubEntries, 1)
	} else {
		ass.Len(endpoints[0].SubEntries[1].SubEntries, 1)
	}
}

func TestLoader_WebEndpoints(t *testing.T) {
	staticPlugins = []core.Plugin{&TestPlugin{}, &TestPluginWeb{}}

	loader := Loader{}
	loader.Load(new(core.TestBus))

	endpoints := loader.WebEndpoints()

	ass := assert.New(t)
	ass.Len(endpoints, 1)
}

func TestLoader_Shutdown(t *testing.T) {
	plugin := &TestPlugin{}
	pluginShutdown := &TestPluginShutdown{}
	staticPlugins = []core.Plugin{plugin, pluginShutdown}

	loader := Loader{}
	loader.Load(new(core.TestBus))

	ass := assert.New(t)
	ass.False(pluginShutdown.shutdown)

	loader.Shutdown()

	ass.True(pluginShutdown.shutdown)
}

var pluginListJSON = `{
    "test1": {
        "id": "test1",
        "version": "0.0.0"
    }
}`

func TestLoader_apiList(t *testing.T) {
	gin.SetMode(gin.TestMode)
	writer := testifyHttp.TestResponseWriter{}
	ctx, _ := gin.CreateTestContext(&writer)

	staticPlugins = []core.Plugin{&TestPlugin{}}

	loader := Loader{}
	loader.Load(new(core.TestBus))
	loader.apiList(http.WrapContext(ctx))

	ass := assert.New(t)
	ass.Equal(200, writer.StatusCode)
	ass.Equal(pluginListJSON, writer.Output)
}

var pluginInfoJSON = `{
    "id": "test1",
    "version": "0.0.0"
}`

func TestLoader_apiGet(t *testing.T) {
	gin.SetMode(gin.TestMode)
	writer := testifyHttp.TestResponseWriter{}
	ctx, _ := gin.CreateTestContext(&writer)

	loader := Loader{}
	loader.apiGet(&pluginInfo{
		plugin: &TestPlugin{},
	})(http.WrapContext(ctx))

	ass := assert.New(t)
	ass.Equal(200, writer.StatusCode)
	ass.Equal(pluginInfoJSON, writer.Output)
}

func Test_setPluginEndpointGroup(t *testing.T) {
	entries := setPluginEndpointGroup("test", []core.PathEntry{{
		Path: "aaa",
		SubEntries: []core.PathEntry{{
			Path: "ccc",
		}},
	}, {
		Path:  "bbb",
		Group: "test",
	}})

	ass := assert.New(t)
	ass.Equal("Plugin test", entries[0].Group)
	ass.Equal("Plugin test", entries[0].SubEntries[0].Group)
	ass.Equal("Plugin test - test", entries[1].Group)
}

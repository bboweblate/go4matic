// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

import "testing"

func TestControlsIf(t *testing.T) {
	equalXML(t, `<block type="controls_if"></block>`, ControlsIf())
}

func TestLogicCompare(t *testing.T) {
	equalXML(t, `<block type="logic_compare">
  <field name="OP">EQ</field>
</block>`, LogicCompare())
}

func TestLogicOperation(t *testing.T) {
	equalXML(t, `<block type="logic_operation">
  <field name="OP">AND</field>
</block>`, LogicOperation())
}

func TestLogicNegate(t *testing.T) {
	equalXML(t, `<block type="logic_negate"></block>`, LogicNegate())
}

func TestLogicBoolean(t *testing.T) {
	equalXML(t, `<block type="logic_boolean">
  <field name="BOOL">TRUE</field>
</block>`, LogicBoolean())
}

func TestLogicNull(t *testing.T) {
	equalXML(t, `<block type="logic_null"></block>`, LogicNull())
}

func TestLogicTernary(t *testing.T) {
	equalXML(t, `<block type="logic_ternary"></block>`, LogicTernary())
}

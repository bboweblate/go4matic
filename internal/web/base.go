// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package web

import (
	"strings"

	"gitlab.com/go4matic/go4matic/internal/web/helper"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// menuEntry represent an entry in the main menu
type menuEntry struct {
	// Name of entry
	Name string
	// Link target of entry
	Link string
	// Icon of font awesome
	Icon string

	SubEntries []menuEntry
}

// render the current request from template
func render(ctx core.HTTPContext) {
	ctx.Set("vars", make(map[string]interface{}))
	ctx.Set("title", "Go4Matic")
	ctx.Set("tpl", "empty")
	ctx.Set("messages", make([]helper.Message, 0))

	ctx.Next()

	if ctx.IsAborted() {
		return
	}

	path := ctx.Request().URL.Path
	baseName := ctx.MustGet("base_name").(string)

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["static_path"] = "/static/"
	vars["api_path"] = "/api/"
	vars["bus_path"] = "/bus"
	vars["base_path"] = "/" + baseName + "/"
	vars["dash_path"] = "/dash"
	vars["admin_path"] = "/admin/"
	vars["title"] = ctx.MustGet("title").(string)
	vars["menu"] = ctx.MustGet("menu").([]menuEntry)
	vars["messages"] = ctx.MustGet("messages").([]helper.Message)

	if len(path) < len(baseName)+2 {
		vars["menu_current"] = []string{}
	} else {
		vars["menu_current"] = strings.Split(path[len(baseName)+2:], "/")
	}

	helper.Render(
		ctx,
		ctx.MustGet("tpl").(string),
		vars)
}

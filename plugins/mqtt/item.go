// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package mqtt

import (
	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

// Item groups multiple MQTT values
type Item struct {
	id     string
	name   string
	values []string

	trigger func(action string, data map[string]interface{})

	p *Plugin
}

// ID of item
func (i *Item) ID() string {
	return "plugin.mqtt." + i.id
}

// Name of item
func (i *Item) Name() string {
	return i.name
}

// GetValueTypes of item
func (i *Item) GetValueTypes() api.FieldMap {
	fields := make(api.FieldMap, len(i.values))
	for _, name := range i.values {
		value := i.p.getValue(name)

		if value == nil {
			continue // ignore not existing values
		}

		fields[name] = api.Field{
			Type:        value.valueType,
			Description: value.description,
		}
	}
	return fields
}

// Events returns all events that can be emitted
func (i *Item) Events() map[string]api.SimpleHandler {
	return map[string]api.SimpleHandler{
		"changed": {
			Description: assets.L.Get("Emitted if a value changed"),
			Parameters:  i.GetValueTypes(),
		},
	}
}

// SetEventTrigger for item event
func (i *Item) SetEventTrigger(trigger func(action string, data map[string]interface{})) {
	i.trigger = trigger
}

// GetValues of item
func (i *Item) GetValues() map[string]interface{} {
	values := make(map[string]interface{}, len(i.values))
	for _, name := range i.values {
		value := i.p.getValue(name)

		if value == nil {
			continue // ignore not existing values
		}

		values[name] = value.getValue()
	}
	return values
}

// ActorItem groups multiple MQTT values that can be modified
type ActorItem struct {
	Item
}

// SetValues of item
func (i *ActorItem) SetValues(values map[string]interface{}) error {
	for name, value := range values {
		err := i.p.setValue(name, value)
		if err != nil {
			return err
		}
	}
	return nil
}

// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package helper

import (
	"bytes"
	"fmt"
	"net/http"
	"reflect"
	"sort"
	"strings"

	"github.com/CloudyKit/jet"
	"github.com/CloudyKit/jet/loaders/httpfs"
	"github.com/spf13/cast"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/internal/version"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

var tpl *jet.Set

// JetTpl returns the instance of the template engine
func JetTpl() *jet.Set {
	if tpl == nil {
		tpl = jet.NewHTMLSetLoader(
			httpfs.NewLoader(assets.Templates()))
		tpl.SetDevelopmentMode(assets.DevMode)

		tpl.AddGlobal("version", version.Version)
		tpl.AddGlobalFunc("sort", func(args jet.Arguments) reflect.Value {
			args.RequireNumOfArguments("sort", 1, 1)
			rawValue := args.Get(0).Interface()

			switch v := rawValue.(type) {
			case []string:
				sort.Strings(v)
				return reflect.ValueOf(v)
			case []int:
				sort.Ints(v)
				return reflect.ValueOf(v)

			case map[string]interface{}:
				keys := make([]string, 0, len(v))
				for k := range v {
					keys = append(keys, k)
				}
				sort.Strings(keys)
				return reflect.ValueOf(keys)

			case core.PluginConfigEntryDefMap:
				return reflect.ValueOf(v.SortedKeys())

			case api.FieldMap:
				return reflect.ValueOf(v.SortedKeys())

			default:
				args.Panicf("invalid type")
			}
			return reflect.ValueOf(nil)
		})

		// workaround for interface bool -> jet eval.go -> castBoolean
		tpl.AddGlobalFunc("bool", func(args jet.Arguments) reflect.Value {
			args.RequireNumOfArguments("bool", 1, 1)
			return reflect.ValueOf(
				cast.ToBool(
					args.Get(0).Interface()))
		})

		// chop string after given size if larger
		tpl.AddGlobalFunc("chop_string", func(args jet.Arguments) reflect.Value {
			args.RequireNumOfArguments("chop_string", 2, 2)

			str := cast.ToString(args.Get(0).Interface())
			length := cast.ToInt(args.Get(1).Interface())
			if len(str) > length {
				return reflect.ValueOf(str[:length])
			}
			return reflect.ValueOf(str)
		})

		// improved length function
		tpl.AddGlobalFunc("len2", func(args jet.Arguments) reflect.Value {
			args.RequireNumOfArguments("len2", 1, 1)

			rawValue := args.Get(0).Interface()

			switch v := rawValue.(type) {
			case []string:
				return reflect.ValueOf(len(v))
			case []int:
				return reflect.ValueOf(len(v))

			case map[string]interface{}:
				return reflect.ValueOf(len(v))

			case api.FieldMap:
				return reflect.ValueOf(len(v))

			default:
				args.Panicf("invalid type")
			}
			return reflect.ValueOf(nil)
		})

		tpl.AddGlobal("Locale", assets.L)
	}
	return tpl
}

// RenderString converts template to string
func RenderString(name string, vars map[string]interface{}) (string, error) {
	tpl, err := JetTpl().GetTemplate(name)
	if err != nil {
		return "", err
	}

	jetVars := make(jet.VarMap)
	for k, v := range vars {
		jetVars.Set(k, v)
	}

	var buff bytes.Buffer
	err = tpl.Execute(&buff, jetVars, nil)
	return buff.String(), err
}

// Render template to context
func Render(ctx core.HTTPContext, name string, vars map[string]interface{}) {
	tpl, err := JetTpl().GetTemplate(name)
	if err != nil {
		ctx.Abort()
		ctx.Status(http.StatusInternalServerError)
		zap.L().Error(err.Error())
		return
	}

	jetVars := make(jet.VarMap)
	for k, v := range vars {
		jetVars.Set(k, v)
	}

	err = tpl.Execute(ctx.Writer(), jetVars, nil)
	if err != nil {
		if strings.Contains(err.Error(), "write: broken pipe") {
			// TODO remove later
			fmt.Print()
		}

		ctx.Abort()
		ctx.Status(http.StatusInternalServerError)
		zap.L().Error(err.Error())
		return
	}
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package helper

import (
	"errors"
	"fmt"
	"strings"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// FieldForm renders a form based on a map of fields
func FieldForm(fields api.FieldMap, values map[string]interface{}) (string, error) {
	if values == nil {
		values = make(map[string]interface{}, len(fields))
	}
	for key, field := range fields {
		v, ok := values[key]
		if !ok {
			switch field.Type.BaseName() {
			case "boolean":
				values[key] = cast.ToBool(field.DefaultValue)
			case "string":
				values[key] = cast.ToString(field.DefaultValue)
			case "integer":
				values[key] = cast.ToInt64(field.DefaultValue)
			case "number":
				values[key] = cast.ToFloat64(field.DefaultValue)
			case "array":
				if field.Type.TypeName() == "[]string" {
					values[key] = strings.Join(cast.ToStringSlice(field.DefaultValue), " ")
				} else {
					return "", fmt.Errorf("unsupported slice %s", field.Type.TypeName())
				}
			default:
				return "", errors.New(assets.L.Get("unsupported form field: %s", field.Type.BaseName()))
			}
		} else if v == nil {
			values[key] = ""
		} else if field.Type.TypeName() == "[]string" {
			values[key] = strings.Join(cast.ToStringSlice(v), " ")
		}
	}

	var f func() map[interface{}]string
	return RenderString("utils/field_form", map[string]interface{}{
		"fields":   fields,
		"values":   values,
		"nil_func": f,
	})
}

// FieldFormValues retrieves post data from form created with FieldForm
func FieldFormValues(ctx core.HTTPContext, fields api.FieldMap) (map[string]interface{}, error) {
	data := make(map[string]interface{}, len(fields))

	for key, field := range fields {
		value := ctx.PostForm(key)
		if field.Type.BaseName() == "boolean" {
			data[key] = value != ""
			continue
		}

		if field.Required && value == "" {
			return nil, errors.New(assets.L.Get("required field missing %s", key))
		} else if field.Type.TypeName() == "[]string" {
			data[key] = cast.ToStringSlice(value)
		} else {
			data[key] = value
		}
	}
	return data, nil
}

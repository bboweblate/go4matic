// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

import "testing"

func TestListCreateWith(t *testing.T) {
	equalXML(t, `<block type="lists_create_with">
  <mutation items="0"></mutation>
</block>`, ListCreateWith(0))
	equalXML(t, `<block type="lists_create_with">
  <mutation items="42"></mutation>
</block>`, ListCreateWith(42))
}

func TestListRepeat(t *testing.T) {
	equalXML(t, `<block type="lists_repeat">
  <value name="NUM">
    <shadow type="math_number">
      <field name="NUM">5</field>
    </shadow>
  </value>
</block>`, ListRepeat())
}

func TestListLength(t *testing.T) {
	equalXML(t, `<block type="lists_length"></block>`, ListLength())
}

func TestListIsEmpty(t *testing.T) {
	equalXML(t, `<block type="lists_isEmpty"></block>`, ListIsEmpty())
}

func TestListIndexOf(t *testing.T) {
	equalXML(t, `<block type="lists_indexOf">
  <field name="END">FIRST</field>
  <value name="VALUE">
    <block type="variables_get">
      <field name="VAR">list</field>
    </block>
  </value>
</block>`, ListIndexOf())
}

func TestListGetIndex(t *testing.T) {
	equalXML(t, `<block type="lists_getIndex">
  <mutation at="true" statement="false"></mutation>
  <field name="MODE">GET</field>
  <field name="WHERE">FROM_START</field>
  <value name="VALUE">
    <block type="variables_get">
      <field name="VAR">list</field>
    </block>
  </value>
</block>`, ListGetIndex())
}

func TestListSetIndex(t *testing.T) {
	equalXML(t, `<block type="lists_setIndex">
  <mutation at="true"></mutation>
  <field name="MODE">SET</field>
  <field name="WHERE">FROM_START</field>
  <value name="LIST">
    <block type="variables_get">
      <field name="VAR">list</field>
    </block>
  </value>
</block>`, ListSetIndex())
}

func TestListGetSublist(t *testing.T) {
	equalXML(t, `<block type="lists_getSublist">
  <mutation at1="true" at2="true"></mutation>
  <field name="WHERE1">FROM_START</field>
  <field name="WHERE2">FROM_START</field>
  <value name="LIST">
    <block type="variables_get">
      <field name="VAR">list</field>
    </block>
  </value>
</block>`, ListGetSublist())
}

func TestListSplit(t *testing.T) {
	equalXML(t, `<block type="lists_split">
  <mutation mode="SPLIT"></mutation>
  <field name="MODE">SPLIT</field>
  <value name="DELIM">
    <shadow type="text">
      <field name="TEXT">,</field>
    </shadow>
  </value>
</block>`, ListSplit())
}

func TestListSort(t *testing.T) {
	equalXML(t, `<block type="lists_sort">
  <field name="TYPE">NUMERIC</field>
  <field name="DIRECTION">1</field>
</block>`, ListSort())
}

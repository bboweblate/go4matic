// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

func TestItemService_Key(t *testing.T) {
	service := NewItemService()
	assert.Equal(t, "core.items", service.Key())
}

func TestItemService_Events(t *testing.T) {
	ass := assert.New(t)
	service := NewItemService()

	_, err := service.addItem(map[string]interface{}{
		"item": &testEvent{itemID: "test.event"},
	})
	ass.NoError(err)
	_, err = service.addItem(map[string]interface{}{
		"item": &testEvent{itemID: "test.event2"},
	})
	ass.NoError(err)
	_, err = service.addItem(map[string]interface{}{
		"item": &testActor{},
	})
	ass.NoError(err)

	assert.Len(t, service.Events(), 2)
}

func TestItemService_Handlers(t *testing.T) {
	service := NewItemService()
	assert.Len(t, service.Handlers(), 7)
}

type testActor struct {
	values map[string]interface{}
}

func (i *testActor) ID() string {
	return "test.actor"
}
func (i *testActor) Name() string {
	return "Test actor"
}
func (i *testActor) GetValueTypes() api.FieldMap {
	return nil
}
func (i *testActor) SetValues(values map[string]interface{}) error {
	i.values = values
	return nil
}

type testSensor struct {
}

func (i *testSensor) ID() string {
	return "test.sensor"
}
func (i *testSensor) Name() string {
	return "Test sensor"
}
func (i *testSensor) GetValueTypes() api.FieldMap {
	return api.FieldMap{
		"value": {
			Type: api.String,
		},
	}
}
func (i *testSensor) GetValues() map[string]interface{} {
	return map[string]interface{}{
		"value": "test",
	}
}

type testEvent struct {
	itemID  string
	trigger core.EventTrigger
}

func (i *testEvent) ID() string {
	return i.itemID
}
func (i *testEvent) Name() string {
	return "Test Event"
}
func (i *testEvent) Events() map[string]api.SimpleHandler {
	return map[string]api.SimpleHandler{
		"test": {
			Description: "test event",
			Parameters: api.FieldMap{
				"test": {
					Description: "test field",
					Type:        api.String,
				},
				"test2": {
					Description: "test field #2",
				},
			},
		},
	}
}
func (i *testEvent) SetEventTrigger(trigger func(action string, data map[string]interface{})) {
	i.trigger = trigger
}

type emptyTestEvent struct {
	trigger core.EventTrigger
}

func (i *emptyTestEvent) ID() string {
	return "test.empty_event"
}
func (i *emptyTestEvent) Name() string {
	return "Test Event"
}
func (i *emptyTestEvent) Events() map[string]api.SimpleHandler {
	return map[string]api.SimpleHandler{
		"test2": {
			Description: "test event",
		},
	}
}
func (i *emptyTestEvent) SetEventTrigger(trigger func(action string, data map[string]interface{})) {
	i.trigger = trigger
}

type testLogger struct {
	id     string
	values map[string]interface{}
	err    error
}

func (i *testLogger) ID() string {
	return "test.logger"
}
func (i *testLogger) Name() string {
	return "Test Logger"
}
func (i *testLogger) LogValues(id string, values map[string]interface{}) error {
	i.id = id
	i.values = values
	return i.err
}

func TestItemService_addItem(t *testing.T) {
	ass := assert.New(t)

	service := NewItemService()

	_, err := service.addItem(map[string]interface{}{
		"item": "test",
	})
	ass.EqualError(err, "invalid item instance given")

	_, err = service.addItem(map[string]interface{}{
		"item": &testSensor{},
	})
	ass.NoError(err)

	_, err = service.addItem(map[string]interface{}{
		"item": &testSensor{},
	})
	ass.EqualError(err, "item already exist: test.sensor")
}

func TestItemService_removeItem(t *testing.T) {
	ass := assert.New(t)

	service := NewItemService()

	sensor := &testSensor{}
	_, err := service.addItem(map[string]interface{}{
		"item": sensor,
	})
	ass.NoError(err)

	_, err = service.removeItem(map[string]interface{}{
		"item": sensor,
	})
	ass.NoError(err)
}

func TestItemService_events(t *testing.T) {
	ass := assert.New(t)

	service := NewItemService()
	eventItem := testEvent{itemID: "test.event"}

	_, err := service.addItem(map[string]interface{}{
		"item": &eventItem,
	})
	ass.NoError(err)

	var eventID string
	var eventData map[string]interface{}
	service.SetEventTrigger(func(id string, data map[string]interface{}) {
		eventID = id
		eventData = data
	})

	eventItem.trigger("aaa", map[string]interface{}{"test": 42})

	ass.Equal("test.event#aaa", eventID)
	ass.Equal(map[string]interface{}{"test": 42}, eventData)
}

func TestItemService_itemInfo(t *testing.T) {
	ass := assert.New(t)

	service := NewItemService()
	sensor := testSensor{}
	actor := testActor{}
	event := testEvent{itemID: "test.event"}
	emptyEvent := emptyTestEvent{}
	logger := testLogger{}

	_, err := service.addItem(map[string]interface{}{
		"item": &sensor,
	})
	ass.NoError(err)
	_, err = service.addItem(map[string]interface{}{
		"item": &actor,
	})
	ass.NoError(err)
	_, err = service.addItem(map[string]interface{}{
		"item": &event,
	})
	ass.NoError(err)
	_, err = service.addItem(map[string]interface{}{
		"item": &emptyEvent,
	})
	ass.NoError(err)
	_, err = service.addItem(map[string]interface{}{
		"item": &logger,
	})
	ass.NoError(err)

	// item list
	items, err := service.itemList(map[string]interface{}{})
	ass.NoError(err)

	ass.Len(items, 5)

	sensorInfo := map[string]interface{}{
		"name":      "Test sensor",
		"is_actor":  false,
		"is_sensor": true,
		"is_logger": false,
		"is_event":  false,
		"values":    sensor.GetValueTypes(),
		"events":    map[string]interface{}{},
	}
	ass.Equal(sensorInfo, items["test.sensor"].(map[string]interface{}))

	actorInfo := map[string]interface{}{
		"name":      "Test actor",
		"is_actor":  true,
		"is_sensor": false,
		"is_logger": false,
		"is_event":  false,
		"values":    api.FieldMap{},
		"events":    map[string]interface{}{},
	}
	ass.Equal(actorInfo, items["test.actor"].(map[string]interface{}))

	eventInfo := map[string]interface{}{
		"name":      "Test Event",
		"is_actor":  false,
		"is_sensor": false,
		"is_logger": false,
		"is_event":  true,
		"values":    api.FieldMap{},
		"events": map[string]interface{}{
			"test": map[string]interface{}{
				"description": event.Events()["test"].Description,
				"parameters":  event.Events()["test"].Parameters,
			},
		},
	}
	ass.Equal(eventInfo, items["test.event"].(map[string]interface{}))

	eventInfo = map[string]interface{}{
		"name":      "Test Event",
		"is_actor":  false,
		"is_sensor": false,
		"is_logger": false,
		"is_event":  true,
		"values":    api.FieldMap{},
		"events": map[string]interface{}{
			"test2": map[string]interface{}{
				"description": "test event",
				"parameters":  api.FieldMap{},
			},
		},
	}
	ass.Equal(eventInfo, items["test.empty_event"].(map[string]interface{}))

	loggerInfo := map[string]interface{}{
		"name":      "Test Logger",
		"is_actor":  false,
		"is_sensor": false,
		"is_logger": true,
		"is_event":  false,
		"values":    api.FieldMap{},
		"events":    map[string]interface{}{},
	}
	ass.Equal(loggerInfo, items["test.logger"].(map[string]interface{}))

	// item info
	_, err = service.itemGetInfo(map[string]interface{}{
		"id": "test.test",
	})
	ass.EqualError(err, "item does not exist: test.test")

	item, err := service.itemGetInfo(map[string]interface{}{
		"id": "test.sensor",
	})
	ass.NoError(err)

	ass.Equal(sensorInfo, item)
}

func TestItemService_itemGetValues(t *testing.T) {
	ass := assert.New(t)

	service := NewItemService()
	sensor := testSensor{}
	actor := testActor{}

	_, err := service.addItem(map[string]interface{}{
		"item": &sensor,
	})
	ass.NoError(err)
	_, err = service.addItem(map[string]interface{}{
		"item": &actor,
	})
	ass.NoError(err)

	_, err = service.itemGetValues(map[string]interface{}{
		"id": "test.test",
	})
	ass.EqualError(err, "item does not exist: test.test")

	_, err = service.itemGetValues(map[string]interface{}{
		"id": "test.actor",
	})
	ass.EqualError(err, "item test.actor is not a sensor")

	data, err := service.itemGetValues(map[string]interface{}{
		"id": "test.sensor",
	})
	ass.NoError(err)
	ass.Equal(sensor.GetValues(), data)
}

func TestItemService_itemSetValues(t *testing.T) {
	ass := assert.New(t)

	service := NewItemService()
	sensor := testSensor{}
	actor := testActor{}

	_, err := service.addItem(map[string]interface{}{
		"item": &sensor,
	})
	ass.NoError(err)
	_, err = service.addItem(map[string]interface{}{
		"item": &actor,
	})
	ass.NoError(err)

	testValues := map[string]interface{}{"value": "test"}

	_, err = service.itemSetValues(map[string]interface{}{
		"id":     "test.test",
		"values": testValues,
	})
	ass.EqualError(err, "item does not exist: test.test")

	_, err = service.itemSetValues(map[string]interface{}{
		"id":     "test.sensor",
		"values": testValues,
	})
	ass.EqualError(err, "item test.sensor is not an actor")

	_, err = service.itemSetValues(map[string]interface{}{
		"id":     "test.actor",
		"values": testValues,
	})
	ass.NoError(err)
	ass.Equal(testValues, actor.values)
}

func TestItemService_APIEndpoints(t *testing.T) {
	service := ItemService{}

	endpoints := service.APIEndpoints(nil)

	ass := assert.New(t)
	ass.Len(endpoints, 1)
	ass.Len(endpoints[0].SubEntries, 3)
}

func TestItemService_itemLogValues(t *testing.T) {
	ass := assert.New(t)

	service := NewItemService()
	logger := testLogger{}
	actor := testActor{}

	logger.err = errors.New("test")

	_, err := service.addItem(map[string]interface{}{
		"item": &logger,
	})
	ass.NoError(err)
	_, err = service.addItem(map[string]interface{}{
		"item": &actor,
	})
	ass.NoError(err)

	testValues := map[string]interface{}{"value": "test"}

	_, err = service.itemLogValues(map[string]interface{}{
		"id":        "test.test",
		"values_id": "test",
		"values":    testValues,
	})
	ass.EqualError(err, "item does not exist: test.test")

	_, err = service.itemLogValues(map[string]interface{}{
		"id":        "test.actor",
		"values_id": "test",
		"values":    testValues,
	})
	ass.EqualError(err, "item test.actor is not a logger")

	_, err = service.itemLogValues(map[string]interface{}{
		"id":        "test.logger",
		"values_id": "test",
		"values":    testValues,
	})
	ass.EqualError(err, "test")
	ass.Equal(testValues, logger.values)
	ass.Equal("test", logger.id)
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package blockly

import (
	"encoding/json"
	"sort"

	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/internal/web/blockly/toolbox"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

// ModuleDef defines toolbox and JS for an entry module
type ModuleDef struct {
	name   string
	blocks []*BlockDef
}

// NewModule creates block definitions for a map of functions
func NewModule(moduleName string, functions map[string]api.Function) *ModuleDef {
	mod := ModuleDef{
		name:   moduleName,
		blocks: make([]*BlockDef, 0, len(functions)),
	}
	for name, funcDef := range functions {
		block, err := NewBlock(name, funcDef, moduleName)
		if err != nil {
			zap.S().Errorf("Ignore function %s.%s: %s", moduleName, name, err)
			continue
		}
		mod.blocks = append(mod.blocks, block)
	}
	return &mod
}

// Toolbox returns toolbox category for block
func (m *ModuleDef) Toolbox() toolbox.Category {
	cat := toolbox.Category{
		Name:    m.name,
		Color:   "#aaaaaa",
		Entries: make([]interface{}, len(m.blocks)),
	}
	sort.Slice(m.blocks, func(i, j int) bool {
		return m.blocks[i].name < m.blocks[j].name
	})
	for idx, block := range m.blocks {
		cat.Entries[idx] = block.Toolbox()
	}
	return cat
}

// JSON returns block definitions
func (m *ModuleDef) JSON() []interface{} {
	jsonList := make([]interface{}, len(m.blocks))
	for idx, block := range m.blocks {
		jsonList[idx] = block.JSON()
	}
	return jsonList
}

// JS returns code generator for module blocks
func (m *ModuleDef) JS() string {
	var js string
	for _, block := range m.blocks {
		js += block.JS()
	}
	return js
}

// ModuleDefList defines toolbox and JS for a list of modules
type ModuleDefList []*ModuleDef

// Add a module to list
func (l *ModuleDefList) Add(def *ModuleDef) {
	*l = append(*l, def)
}

// ExtendedDefaultToolbox returns a default toolbox extended for modules
func (l *ModuleDefList) ExtendedDefaultToolbox() toolbox.XML {
	tb := toolbox.DefaultToolbox()
	tb.Entries = append(tb.Entries, new(toolbox.Separator))

	sort.Slice(*l, func(i, j int) bool {
		return (*l)[i].name < (*l)[j].name
	})

	for _, module := range *l {
		tb.Entries = append(tb.Entries, module.Toolbox())
	}
	return tb
}

// BlockJSON returns JSON object for all blocks in all modules
func (l *ModuleDefList) BlockJSON() []interface{} {
	jsonList := make([]interface{}, 0)
	for _, module := range *l {
		jsonList = append(jsonList, module.JSON()...)
	}
	return jsonList
}

// BlockJS returns content of JS block definition and code generator
func (l *ModuleDefList) BlockJS() (string, error) {
	defData, err := json.MarshalIndent(l.BlockJSON(), "", "  ")
	if err != nil {
		return "", err
	}

	js := "Blockly.defineBlocksWithJsonArray(" + string(defData) + ");\n"

	for _, module := range *l {
		js += module.JS()
	}
	return js, nil
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package influxdb

import (
	"errors"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// Plugin that implements influxdb interface
type Plugin struct {
	bus core.Bus

	// list of configured databases
	entries map[string]*Database
}

// ID of plugin
func (p *Plugin) ID() string {
	return "influxdb"
}

// Version information of plugin
func (p *Plugin) Version() string {
	return "0.1.0"
}

// Init plugin
func (p *Plugin) Init(b core.Bus) error {
	p.bus = b
	return nil
}

// Config returns the definition of the config interface
func (p *Plugin) Config() *core.PluginConfigDef {
	return &core.PluginConfigDef{
		Entries: core.PluginConfigEntryDefMap{
			"databases": {
				Config: api.FieldMap{
					"address": {
						Type:        api.String,
						Description: assets.L.Get("Address of database"),
						Required:    true,
					},
					"username": {
						Type:        api.String,
						Description: assets.L.Get("User of database"),
						Required:    true,
					},
					"password": {
						Type:        api.String,
						Description: assets.L.Get("Password of database"),
						Required:    true,
					},
					"database": {
						Type:        api.String,
						Description: assets.L.Get("Name of database"),
						Required:    true,
					},
				},

				LoadEntryFunc: p.entryConfigLoad,
				AddEntryFunc:  p.entryConfigAdd,
				//ChangeEntryFunc: nil,
				RemoveEntryFunc: p.entryConfigRemove,
			},
		},
	}
}

func (p *Plugin) entryConfigLoad(entries map[string]map[string]interface{}) error {
	p.entries = make(map[string]*Database, len(entries))
	for k, v := range entries {
		err := p.createEntry(k, cast.ToStringMap(v))
		if err != nil {
			return err
		}
	}
	return nil
}
func (p *Plugin) entryConfigAdd(key string, config map[string]interface{}) error {
	return p.createEntry(key, config)
}
func (p *Plugin) entryConfigRemove(key string) error {
	return p.removeEntry(key)
}

func (p *Plugin) createEntry(key string, config map[string]interface{}) error {
	db := &Database{
		key:      key,
		address:  cast.ToString(config["address"]),
		username: cast.ToString(config["username"]),
		password: cast.ToString(config["password"]),
		database: cast.ToString(config["database"]),
	}

	err := db.Connect()
	if err != nil {
		return errors.New(assets.L.Get("failed to connect to database %v", err))
	}

	err = core.ItemAdd(p.bus, db)
	if err != nil {
		return errors.New(assets.L.Get("failed to add database item %v", err))
	}

	p.entries[key] = db
	return nil
}
func (p *Plugin) removeEntry(key string) error {
	err := core.ItemRemove(p.bus, p.entries[key])
	if err != nil {
		return errors.New(assets.L.Get("failed to remove database item %v", err))
	}

	delete(p.entries, key)
	return nil
}

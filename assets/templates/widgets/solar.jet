<div class="col-md-12" style="padding-bottom: 10px;">
    <div id="solar_img_{{id}}" style="height: 100%;">
        {{include "solar.svg"}}
    </div>
    <br/>
</div>

<script>
    $(function() {
        let solar_img = $("#solar_img_{{id}}");

        // SVG functions
        function svg_show(id) {
            solar_img.find(id).attr("visibility", "visible");
        }

        function svg_hide(id) {
            solar_img.find(id).attr("visibility", "hidden");
        }

        function svg_set_width(id, w) {
            solar_img.find(id).attr("width", w);
        }

        function svg_set_height(id, h) {
            solar_img.find(id).attr("height", h);
        }

        function svg_set_text(id, value) {
            solar_img.find(id).text(value);
        }

        // prepare SVG
        svg_hide("#transfer_solar_to_supplier");
        svg_hide("#transfer_solar_to_home");
        svg_hide("#transfer_solar_to_battery");
        svg_hide("#transfer_battery_to_home");
        svg_hide("#transfer_supplier_to_home");

        svg_hide("#solar_green");
        svg_hide("#supplier_green");
        svg_hide("#supplier_red");
        svg_hide("#battery_green");

        svg_set_width("#home_green", 0);
        svg_set_width("#home_red", 0);
        svg_set_height("#battery_charge_img", 0);

        svg_set_text("#solar_power", 0);
        svg_set_text("#supplier_power", 0);
        svg_set_text("#home_power", 0);
        svg_set_text("#battery_power", 0);
        svg_set_text("#battery_charge", 0);

        // define values for widget
        let values = {
            "{{ supplier_plus.String() }}": 0,
            "{{ supplier_minus.String() }}": 0,
            "{{ solar_power.String() }}": 0,
            "{{ battery_power.String() }}": 0,
            "{{ battery_charge.String() }}": 0
        };
        let item_values = {};
        for (let val in values) {
            if (!values.hasOwnProperty(val)) {
                continue;
            }
            let valSplit = val.split("#");

            if (!item_values.hasOwnProperty(valSplit[0])) {
                item_values[valSplit[0]] = {};
            }
            item_values[valSplit[0]][valSplit[1]] = true;
        }

        // draw function if values updated
        function draw() {
            let supplier_plus = values["{{ supplier_plus.String() }}"];
            let supplier_minus = values["{{ supplier_minus.String() }}"];
            let solar_power = values["{{ solar_power.String() }}"];
            let battery_power = values["{{ battery_power.String() }}"];
            let battery_charge = values["{{ battery_charge.String() }}"];

            let supplier_power = supplier_plus - supplier_minus;
            let home_power = solar_power + supplier_power + battery_power;
            if (home_power <= 0) {
                console.log("home_power <= 0");
                return;
            }

            let home_supplier_power = 0;
            if (supplier_power > 0) {
                home_supplier_power = supplier_power;
            }
            let home_own_power = home_power - home_supplier_power;

            // solar
            svg_set_text("#solar_power", Math.round(solar_power).toLocaleString());
            if (solar_power > 0) {
                svg_show("#solar_green");
            } else {
                svg_hide("#solar_green");
            }

            if (solar_power > 0 && home_power > 0) {
                svg_show("#transfer_solar_to_home");
            } else {
                svg_hide("#transfer_solar_to_home");
            }

            // battery
            svg_set_text("#battery_power", Math.round(battery_power).toLocaleString());
            svg_set_text("#battery_charge", Math.round(battery_charge));
            svg_set_height("#battery_charge_img", battery_charge);
            if (battery_charge > 0 || battery_power !== 0) {
                svg_show("#battery_green");
            } else {
                svg_hide("#battery_green");
            }

            if (battery_power < 0) {
                svg_show("#transfer_solar_to_battery");
                svg_hide("#transfer_battery_to_home");
            } else if (battery_power > 0) {
                svg_hide("#transfer_solar_to_battery");
                svg_show("#transfer_battery_to_home");
            } else {
                svg_hide("#transfer_solar_to_battery");
                svg_hide("#transfer_battery_to_home");
            }

            // supplier
            svg_set_text("#supplier_power", Math.round(supplier_power).toLocaleString());
            if (supplier_power > 10) {
                svg_hide("#supplier_green");
                svg_show("#supplier_red");

                svg_show("#transfer_supplier_to_home");
                svg_hide("#transfer_solar_to_supplier");
            } else if (supplier_power < -10) {
                svg_show("#supplier_green");
                svg_hide("#supplier_red");

                svg_hide("#transfer_supplier_to_home");
                svg_show("#transfer_solar_to_supplier");
            } else {
                svg_hide("#supplier_green");
                svg_hide("#supplier_red");

                svg_hide("#transfer_supplier_to_home");
                svg_hide("#transfer_solar_to_supplier");
            }

            // home
            svg_set_text("#home_power", Math.round(home_power).toLocaleString());
            svg_set_width("#home_green", home_own_power / home_power);
            svg_set_width("#home_red", home_supplier_power / home_power);
        }

        // prepare event handlers
        let eventHandlers = {};
        for (let item_id in item_values) {
            if (!item_values.hasOwnProperty(item_id)) {
                continue;
            }

            eventHandlers[item_id + "#changed"] = function (id, data) {
                for (let value in item_values[item_id]) {
                    if (!item_values[item_id].hasOwnProperty(value)) {
                        continue;
                    }

                    if (value in data) {
                        values[item_id + "#" + value] = data[value]
                    }
                }

                draw();
            }
        }

        // register events
        registerConnectedHandler(function () {
            for (let key in eventHandlers) {
                if (!eventHandlers.hasOwnProperty(key)) {
                    continue;
                }

                registerEventHandler(key, eventHandlers[key]);
            }
        });
    });
</script>
// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"context"
	"errors"
	"strings"
	"sync"
	"time"

	"github.com/spf13/cast"
	"github.com/yuin/gopher-lua"
	"github.com/yuin/gopher-lua/parse"
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// lua2go convert a LUA value to go
func lua2go(ls *lua.LState, value lua.LValue) interface{} {
	switch value := value.(type) {
	case lua.LBool:
		return value == lua.LTrue

	case lua.LNumber:
		return float64(value)

	case lua.LString:
		return string(value)

	case *lua.LTable:
		data := make(map[string]interface{}, value.Len())

		value.ForEach(func(key lua.LValue, value lua.LValue) {
			data[key.String()] = lua2go(ls, value)
		})

		return data

	case *lua.LFunction:
		return func(data []interface{}) []interface{} {
			ls.Push(value)

			for _, val := range data {
				ls.Push(go2lua(val))
			}

			return []interface{}{
				ls.PCall(len(data), 0, nil),
			}
		}

	default:
		return nil
	}
}

// lua2goL gets multiple values from LUA
func lua2goL(ls *lua.LState, length int) []interface{} {
	args := make([]interface{}, length)
	for i := 0; i < length; i++ {
		args[i] = lua2go(ls, ls.Get(i+1))
	}
	return args
}

// go2lua convert go value to LUA value
func go2lua(value interface{}) lua.LValue {
	switch value := value.(type) {
	case int, uint, int8, uint8, int16, uint16, int32, uint32, int64, uint64, float32, float64:
		return lua.LNumber(cast.ToFloat64(value))

	case string:
		return lua.LString(value)
	case error:
		return lua.LString(value.Error())

	case bool:
		return lua.LBool(value)

	case []string:
		data := &lua.LTable{}

		for _, val := range value {
			data.Append(lua.LString(val))
		}

		return data

	case map[string]interface{}:
		data := &lua.LTable{}

		for key, val := range value {
			data.RawSetString(key, go2lua(val))
		}

		return data

	default:
		return lua.LNil
	}
}

// go2luaL convert go values to LUA values
func go2luaL(values []interface{}) []lua.LValue {
	luaValues := make([]lua.LValue, len(values))
	for idx, value := range values {
		luaValues[idx] = go2lua(value)
	}
	return luaValues
}

// go2luaResp convert go values to LUA response
func go2luaResp(ls *lua.LState, values []interface{}) int {
	if values == nil {
		return 0
	}

	for _, value := range go2luaL(values) {
		ls.Push(value)
	}
	return len(values)
}

// luaModule prepares lua module from function definitions
func luaModule(functions map[string]core.ScriptFunction, script core.Script) map[string]lua.LGFunction {
	modules := make(map[string]lua.LGFunction, len(functions))
	for name, f := range functions {
		function := f
		modules[name] = func(ls *lua.LState) int {
			args, err := function.Def.ValidateArguments(lua2goL(ls, len(function.Def.Arguments)))
			if err != nil {
				ls.RaiseError(err.Error())
				return 0
			}
			return go2luaResp(ls, function.Func(script, args))
		}
	}
	return modules
}

// luaCheck checks does compile (does not check if functions exist)
func luaCheck(script string) error {
	chunk, err := parse.Parse(strings.NewReader(script), "script.lua")
	if err != nil {
		return err
	}

	_, err = lua.Compile(chunk, "script.lua")
	if err != nil {
		return err
	}

	return nil
}

type luaScript struct {
	// content of script
	content string

	// enabled true if script starts on load
	enabled bool

	// blockly data
	blocklyEnabled bool
	blocklyXML     string

	// lua vm instance
	luaVM *lua.LState

	// True if script is running
	running bool

	// modules
	def *core.ScriptDefinition

	// function to stop lua vm
	cancel context.CancelFunc

	mutex sync.RWMutex
}

// Start and initialize LUA VM
func (s *luaScript) Start() {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if s.luaVM != nil {
		return
	}
	s.luaVM = lua.NewState()

	// prepare for stopping of LUA VM
	var ctx context.Context
	ctx, s.cancel = context.WithCancel(context.Background())
	s.luaVM.SetContext(ctx)

	// add modules to VM
	for key, module := range s.def.Modules {
		m := module
		s.luaVM.PreloadModule(key, func(ls *lua.LState) int {
			ls.Push(ls.SetFuncs(ls.NewTable(), luaModule(m.Functions, s)))
			return 1
		})
	}

	// add global functions
	for name, f := range luaModule(s.def.GlobalFunctions, s) {
		s.luaVM.SetGlobal(name, s.luaVM.NewFunction(f))
	}

	// add constant values
	for name, value := range s.def.ConstantValues {
		s.luaVM.SetGlobal(name, go2luaL([]interface{}{value})[0])
	}

	callbackChannel := make(chan func(), 10)

	// register event channel
	for _, module := range s.def.Modules {
		if module.CallbackChannelFunc != nil {
			module.CallbackChannelFunc(s, callbackChannel)
		}
	}

	// start execution
	vm := s.luaVM
	s.running = true
	go func() {
		if err := vm.DoString(s.content); err != nil {
			zap.L().Error(err.Error())
			s.Stop()
			return
		}

		for f := range callbackChannel {
			if !s.IsRunning() {
				break
			}
			f()
		}
	}()
}

// Stop LUA VM
func (s *luaScript) Stop() {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if !s.running {
		return
	}

	// stop script
	s.cancel()
	time.Sleep(time.Millisecond * 10)

	// cleanup modules
	for _, module := range s.def.Modules {
		if module.CleanupFunc != nil {
			module.CleanupFunc(s)
		}
	}

	s.running = false
	s.luaVM = nil
}

// Content returns the content of the script
func (s *luaScript) Content() string {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	return s.content
}

// IsRunning returns true if the script is running
func (s *luaScript) IsRunning() bool {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	return s.running
}

// ScriptService manages all script instances
type ScriptService struct {
	scripts map[string]*luaScript

	// definition of script functionality
	def core.ScriptDefinition

	// mutex for items
	mutex sync.RWMutex
}

// Key that identifies this service
func (s *ScriptService) Key() string {
	return "core.scripts"
}

// AddModule to script definition
func (s *ScriptService) AddModule(module core.ScriptModule) error {
	if _, ok := s.def.Modules[module.Name()]; ok {
		return errors.New(assets.L.Get("module %s already added", module.Name()))
	}

	s.def.Modules[module.Name()] = module.Def()
	return nil
}

// Init load the service
func (s *ScriptService) Init(bus core.Bus) error {
	value, err := core.ConfigRead(bus, "core.scripts")
	if err != nil {
		return err
	}
	s.scripts = make(map[string]*luaScript)
	s.def = core.ScriptDefinition{
		Modules: make(map[string]core.ScriptModuleDefinition),
	}

	for id, info := range cast.ToStringMap(value) {
		infoData := cast.ToStringMap(info)

		s.scripts[id] = &luaScript{
			def:            &s.def,
			content:        cast.ToString(infoData["script"]),
			enabled:        cast.ToBool(infoData["enabled"]),
			blocklyEnabled: cast.ToBool(infoData["blockly_enabled"]),
			blocklyXML:     cast.ToString(infoData["blockly_xml"]),
		}
	}
	zap.L().Info(assets.L.GetN(
		"Loaded %d script",
		"Loaded %d scripts",
		len(s.scripts), len(s.scripts)))
	return nil
}

// Start scripts that are enabled
func (s *ScriptService) Start(bus core.Bus) error {
	for _, script := range s.scripts {
		if script.enabled {
			script.Start()
		}
	}
	return nil
}

// Update saves actual scripts
func (s *ScriptService) Update(bus core.Bus) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	data := make(map[string]interface{}, len(s.scripts))
	for id, script := range s.scripts {
		data[id] = map[string]interface{}{
			"script":          script.content,
			"enabled":         script.enabled,
			"blockly_enabled": script.blocklyEnabled,
			"blockly_xml":     script.blocklyXML,
		}
	}

	err := core.ConfigWrite(bus, "core.scripts", data)
	if err != nil {
		zap.L().Error(err.Error())
	}
}

// Shutdown saves actual scripts
func (s *ScriptService) Shutdown(bus core.Bus) {
	s.Update(bus)
}

// Handlers for this service
func (s *ScriptService) Handlers() map[string]core.RequestHandler {
	return map[string]core.RequestHandler{
		"list_modules": {
			Func: s.moduleList,
			Def: api.SimpleHandler{
				Description: assets.L.Get("List available script modules"),
				Response: api.FieldMap{
					"{module_name}": {
						Description: assets.L.Get("Information about module"),
						Type: api.FieldMap{
							"description": {
								Description: assets.L.Get("Description of module"),
								Required:    true,
								Type:        api.String,
							},
							"functions": {
								Description: assets.L.Get("List of module functions"),
								Required:    true,
								Type:        api.Map(api.String, api.Any),
							},
						},
					},
				},
			},
		},

		"add": {
			Func: s.scriptAdd,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Create new script"),
				Parameters: api.FieldMap{
					"id": {
						Description: assets.L.Get("Unique identifier for script"),
						Required:    true,
						Type:        api.String,
					},
					"script": {
						Description: assets.L.Get("Content of the script"),
						Required:    true,
						Type:        api.String,
					},
					"enabled": {
						Description:  assets.L.Get("True if script is enabled"),
						DefaultValue: false,
						Type:         api.Bool,
					},
					"blockly_enabled": {
						Description:  assets.L.Get("True if blockly is enabled"),
						DefaultValue: true,
						Type:         api.Bool,
					},
					"blockly_xml": {
						Description: assets.L.Get("Blockly XML data"),
						Type:        api.String,
					},
				},
			},
		},
		"update": {
			Func: s.scriptUpdate,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Update script"),
				Parameters: api.FieldMap{
					"id": {
						Description: assets.L.Get("Identifier of script"),
						Required:    true,
						Type:        api.String,
					},
					"script": {
						Description: assets.L.Get("Content of the script"),
						Type:        api.String,
					},
					"enabled": {
						Description: assets.L.Get("True if script is enabled"),
						Type:        api.Bool,
					},
					"blockly_enabled": {
						Description: assets.L.Get("True if blockly is enabled"),
						Type:        api.Bool,
					},
					"blockly_xml": {
						Description: assets.L.Get("Blockly XML data"),
						Type:        api.String,
					},
				},
			},
		},
		"remove": {
			Func: s.scriptRemove,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Remove script"),
				Parameters: api.FieldMap{
					"id": {
						Description: assets.L.Get("Identifier of script"),
						Required:    true,
						Type:        api.String,
					},
				},
			},
		},

		"list": {
			Func: s.scriptList,
			Def: api.SimpleHandler{
				Description: assets.L.Get("List all known script"),
				Response: api.FieldMap{
					"{id}": {
						Description: assets.L.Get("Information of script"),
						Type: api.FieldMap{
							"script": {
								Description: assets.L.Get("Content of the script"),
								Required:    true,
								Type:        api.String,
							},
							"enabled": {
								Description: assets.L.Get("True if script is enabled"),
								Required:    true,
								Type:        api.Bool,
							},
							"blockly_enabled": {
								Description: assets.L.Get("True if blockly is enabled"),
								Required:    true,
								Type:        api.Bool,
							},
							"blockly_xml": {
								Description: assets.L.Get("Blockly XML data"),
								Required:    true,
								Type:        api.String,
							},
						},
					},
				},
			},
		},
		"get": {
			Func: s.scriptGet,
			Def: api.SimpleHandler{
				Description: assets.L.Get("Information of script"),
				Parameters: api.FieldMap{
					"id": {
						Description: assets.L.Get("Identifier of script"),
						Required:    true,
						Type:        api.String,
					},
				},
				Response: api.FieldMap{
					"script": {
						Description: assets.L.Get("Content of the script"),
						Required:    true,
						Type:        api.String,
					},
					"enabled": {
						Description: assets.L.Get("True if script is enabled"),
						Required:    true,
						Type:        api.Bool,
					},
					"blockly_enabled": {
						Description: assets.L.Get("True if blockly is enabled"),
						Required:    true,
						Type:        api.Bool,
					},
					"blockly_xml": {
						Description: assets.L.Get("Blockly XML data"),
						Required:    true,
						Type:        api.String,
					},
				},
			},
		},
	}
}

// APIEndpoints for service
func (s *ScriptService) APIEndpoints(bus core.Bus) []core.PathEntry {
	return []core.PathEntry{{
		Path: "/scripts",
		SubEntries: []core.PathEntry{
			core.APIWrapper(bus, s, "list", core.PathEntry{
				Path:  "/",
				Group: "Core",
			}),
			core.APIWrapper(bus, s, "add", core.PathEntry{
				Path:   "/",
				Group:  "Core",
				Method: "POST",
			}),
			core.APIWrapper(bus, s, "get", core.PathEntry{
				Path:  "/:id",
				Group: "Core",
			}),
			core.APIWrapper(bus, s, "update", core.PathEntry{
				Path:   "/:id",
				Group:  "Core",
				Method: "PUT",
			}),
			core.APIWrapper(bus, s, "remove", core.PathEntry{
				Path:   "/:id",
				Group:  "Core",
				Method: "DELETE",
			}),
		},
	}}
}

// moduleList lists all known scripts
func (s *ScriptService) moduleList(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	modules := make(map[string]interface{}, len(s.def.Modules))

	for id, module := range s.def.Modules {
		modules[id] = map[string]interface{}{
			"description": module.Description,
			"functions":   module.FunctionDef(),
		}
	}
	return modules, nil
}

// scriptAdd adds a new script to the service
func (s *ScriptService) scriptAdd(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	id := data["id"].(string)

	// check if script already exist
	if _, ok := s.scripts[id]; ok {
		return nil, errors.New(assets.L.Get("script already exist: %s", id))
	}

	// validate script
	script := cast.ToString(data["script"])
	if err := luaCheck(script); err != nil {
		return nil, errors.New(assets.L.Get("script invalid: %v", err))
	}

	ls := luaScript{
		def:            &s.def,
		content:        script,
		enabled:        cast.ToBool(data["enabled"]),
		blocklyEnabled: cast.ToBool(data["blockly_enabled"]),
		blocklyXML:     cast.ToString(data["blockly_xml"]),
	}

	// start if enabled
	if ls.enabled {
		ls.Start()
	}

	s.scripts[id] = &ls
	return nil, nil
}

// scriptUpdate updates a script
func (s *ScriptService) scriptUpdate(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	id := data["id"].(string)

	// check if script exist
	ls, ok := s.scripts[id]
	if !ok {
		return nil, errors.New(assets.L.Get("script does not exist: %s", id))
	}

	if rawScript := data["script"]; rawScript != nil {
		// validate script
		script := cast.ToString(rawScript)

		// script changed?
		if ls.content != script {
			if err := luaCheck(script); err != nil {
				return nil, errors.New(assets.L.Get("script invalid: %v", err))
			}
			ls.content = script
		}
	}

	// stop & reset if running
	if ls.IsRunning() {
		ls.Stop()
	}

	// updated enabled status if required
	if rawEnabled := data["enabled"]; rawEnabled != nil {
		ls.enabled = cast.ToBool(rawEnabled)
	}

	// updated blockly enabled if required
	if rawEnabled := data["blockly_enabled"]; rawEnabled != nil {
		ls.blocklyEnabled = cast.ToBool(rawEnabled)
	}

	// updated blockly xml if required
	if xml := data["blockly_xml"]; xml != nil {
		ls.blocklyXML = cast.ToString(xml)
	}

	// start if enabled
	if ls.enabled {
		ls.Start()
	}
	return nil, nil
}

// scriptRemove removes a script from service
func (s *ScriptService) scriptRemove(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	id := data["id"].(string)

	// check if script exist
	ls, ok := s.scripts[id]
	if !ok {
		return nil, errors.New(assets.L.Get("script does not exist: %s", id))
	}

	// stop if running
	if ls.IsRunning() {
		ls.Stop()
	}

	delete(s.scripts, id)
	return nil, nil
}

// scriptList lists all known scripts
func (s *ScriptService) scriptList(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	scripts := make(map[string]interface{}, len(s.scripts))

	for id, script := range s.scripts {
		scripts[id] = map[string]interface{}{
			"script":          script.content,
			"enabled":         script.enabled,
			"blockly_enabled": script.blocklyEnabled,
			"blockly_xml":     script.blocklyXML,
		}
	}
	return scripts, nil
}

// scriptGet returns information of the given script
func (s *ScriptService) scriptGet(data map[string]interface{}) (map[string]interface{}, error) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	id := data["id"].(string)

	// check if script exist
	script, ok := s.scripts[id]
	if !ok {
		return nil, errors.New(assets.L.Get("script does not exist: %s", id))
	}

	return map[string]interface{}{
		"script":          script.content,
		"enabled":         script.enabled,
		"blockly_enabled": script.blocklyEnabled,
		"blockly_xml":     script.blocklyXML,
	}, nil
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

import (
	"testing"
)

func TestColourPicker(t *testing.T) {
	equalXML(t, `<block type="colour_picker">
  <field name="COLOUR">#ff0000</field>
</block>`, ColourPicker())
}

func TestColourRandom(t *testing.T) {
	equalXML(t, `<block type="colour_random"></block>`, ColourRandom())
}

func TestColourRGB(t *testing.T) {
	equalXML(t, `<block type="colour_rgb">
  <value name="RED">
    <shadow type="math_number">
      <field name="NUM">100</field>
    </shadow>
  </value>
  <value name="GREEN">
    <shadow type="math_number">
      <field name="NUM">50</field>
    </shadow>
  </value>
  <value name="BLUE">
    <shadow type="math_number">
      <field name="NUM">0</field>
    </shadow>
  </value>
</block>`, ColourRGB())
}

func TestColourBlend(t *testing.T) {
	equalXML(t, `<block type="colour_blend">
  <value name="COLOUR1">
    <shadow type="colour_picker">
      <field name="COLOUR">#ff0000</field>
    </shadow>
  </value>
  <value name="COLOUR2">
    <shadow type="colour_picker">
      <field name="COLOUR">#3333ff</field>
    </shadow>
  </value>
  <value name="RATIO">
    <shadow type="math_number">
      <field name="NUM">0.5</field>
    </shadow>
  </value>
</block>`, ColourBlend())
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

// test data for TestBasicTypes
var checkBasicTypeData = []struct {
	typ      Type
	baseType string
	value    interface{}
}{
	{Bool, "boolean", true},
	{Int, "integer", int(-42)},
	{Int8, "integer", int8(-42)},
	{Int16, "integer", int16(-42)},
	{Int32, "integer", int32(-42)},
	{Int64, "integer", int64(-42)},
	{Uint, "integer", uint(42)},
	{Uint8, "integer", uint8(42)},
	{Uint16, "integer", uint16(42)},
	{Uint32, "integer", uint32(42)},
	{Uint64, "integer", uint64(42)},
	{Float32, "number", float32(3.14)},
	{Float64, "number", float64(3.14)},
	{String, "string", "test"},
}

func TestBasicTypes(t *testing.T) {
	// different values
	for _, data := range checkBasicTypeData {
		name := data.typ.TypeName()

		t.Run(name, func(st *testing.T) {
			ass := assert.New(st)

			ass.Equal(data.baseType, data.typ.BaseName())
			ass.True(data.typ.Check(data.value))

			if data.typ == String {
				ass.False(data.typ.Check(map[string]interface{}{}))
			} else {
				ass.False(data.typ.Check("aaa"))
			}
		})
	}
	t.Run("unknown_type", func(st *testing.T) {
		invalidType := basicType{42, "unknown", "unknown"}

		assert.False(st, invalidType.Check(42))
	})
}

func TestErrorType(t *testing.T) {
	ass := assert.New(t)

	ass.True(Error.Check(errors.New("test")))
	ass.True(Error.Check("test"))
	ass.False(Error.Check([]int{42, 32}))
	ass.Equal("error", Error.TypeName())
	ass.Equal("error", Error.BaseName())
}

func TestAnyType(t *testing.T) {
	ass := assert.New(t)

	ass.True(Any.Check([]int{42, 32}))
	ass.True(Any.Check(42))
	ass.True(Any.Check(errors.New("test")))
	ass.Equal("*", Any.TypeName())
	ass.Equal("AnyValue", Any.BaseName())
}

func TestSliceTypes(t *testing.T) {
	ass := assert.New(t)

	intSliceType := Slice(Int)
	ass.Equal("[]int", intSliceType.TypeName())
	ass.Equal("array", intSliceType.BaseName())

	ass.True(intSliceType.Check([]int{42, 32}))
	ass.False(intSliceType.Check(42))
	ass.False(intSliceType.Check([]string{"test"}))

	stringSliceType := Slice(String)
	ass.Equal("[]string", stringSliceType.TypeName())
	ass.Equal("array", stringSliceType.BaseName())

	ass.True(stringSliceType.Check([]string{"test"}))
	ass.True(stringSliceType.Check([]int{42, 32}))
	ass.False(stringSliceType.Check(42))
}

func TestMapTypes(t *testing.T) {
	ass := assert.New(t)

	intStringMapType := Map(Int, String)
	ass.Equal("map[int]string", intStringMapType.TypeName())
	ass.Equal("object", intStringMapType.BaseName())

	ass.True(intStringMapType.Check(map[int]string{1: "test", 2: "test2"}))
	ass.False(intStringMapType.Check(42))

	intIntMapType := Map(Int, Int)
	ass.Equal("map[int]int", intIntMapType.TypeName())
	ass.Equal("object", intIntMapType.BaseName())

	ass.True(intIntMapType.Check(map[int]int{1: 42, 2: 32}))
	ass.False(intIntMapType.Check(map[int]string{1: "test", 2: "test2"}))
	ass.False(intIntMapType.Check(42))

	stringIntMapType := Map(String, Int)
	ass.Equal("map[string]int", stringIntMapType.TypeName())
	ass.Equal("object", stringIntMapType.BaseName())

	ass.True(stringIntMapType.Check(map[string]int{"test": 1, "test2": 2}))
	ass.False(stringIntMapType.Check(map[int]string{1: "test", 2: "test2"}))
	ass.False(stringIntMapType.Check(42))
}

func TestCombinedTypes(t *testing.T) {
	ass := assert.New(t)

	m := Map(String, Slice(Map(String, Any)))
	ass.Equal("map[string][]map[string]*", m.TypeName())
	ass.Equal("object", m.BaseName())

	ass.True(m.Check(map[string][]map[string]interface{}{
		"test": {
			map[string]interface{}{
				"aaa": 42,
				"bbb": "test",
			},
			map[string]interface{}{
				"111": 43,
				"222": "test2",
			},
		},
		"test2": {
			map[string]interface{}{
				"aaa": 42,
				"bbb": "test",
			},
		},
	}))
	ass.True(m.Check(map[string][]map[string]interface{}{
		"test": {
			map[string]interface{}{},
		},
	}))
	ass.False(m.Check(map[int]string{1: "test", 2: "test2"}))
	ass.False(m.Check(map[int]int{1: 42, 2: 32}))
	ass.False(m.Check(42))
}

var typeFromValueData = []struct {
	value    interface{}
	typeName string
}{{
	value:    nil,
	typeName: "*",
}, {
	value:    true,
	typeName: "bool",
}, {
	value:    42,
	typeName: "int",
}, {
	value:    int8(42),
	typeName: "int8",
}, {
	value:    int16(42),
	typeName: "int16",
}, {
	value:    int32(42),
	typeName: "int32",
}, {
	value:    int64(42),
	typeName: "int64",
}, {
	value:    uint(42),
	typeName: "uint",
}, {
	value:    uint8(42),
	typeName: "uint8",
}, {
	value:    uint16(42),
	typeName: "uint16",
}, {
	value:    uint32(42),
	typeName: "uint32",
}, {
	value:    uint64(42),
	typeName: "uint64",
}, {
	value:    float32(42),
	typeName: "float32",
}, {
	value:    float64(42),
	typeName: "float64",
}, {
	value:    "test",
	typeName: "string",
}, {
	value:    []string{"aaa", "bbb"},
	typeName: "[]string",
}, {
	value:    []interface{}{"aaa", 42},
	typeName: "[]*",
}, {
	value: map[string]string{
		"aaa": "111",
		"bbb": "222",
	},
	typeName: "map[string]string",
}, {
	value: map[string]interface{}{
		"aaa": "111",
		"bbb": 222,
	},
	typeName: "map[string]*",
}}

func TestTypeFromValue(t *testing.T) {
	for _, data := range typeFromValueData {
		t.Run(data.typeName, func(st *testing.T) {
			ass := assert.New(st)
			ass.Equal(data.typeName,
				TypeFromValue(data.value).TypeName())
		})
	}
}

var typeFromTypeNameData = []struct {
	name     string
	typeName string
}{{
	name:     "nil",
	typeName: "*",
}, {
	name:     "bool",
	typeName: "bool",
}, {
	name:     "int",
	typeName: "int",
}, {
	name:     "int8",
	typeName: "int8",
}, {
	name:     "int16",
	typeName: "int16",
}, {
	name:     "int32",
	typeName: "int32",
}, {
	name:     "int64",
	typeName: "int64",
}, {
	name:     "uint",
	typeName: "uint",
}, {
	name:     "uint8",
	typeName: "uint8",
}, {
	name:     "uint16",
	typeName: "uint16",
}, {
	name:     "uint32",
	typeName: "uint32",
}, {
	name:     "uint64",
	typeName: "uint64",
}, {
	name:     "float32",
	typeName: "float32",
}, {
	name:     "float64",
	typeName: "float64",
}, {
	name:     "string",
	typeName: "string",
}, {
	name:     "[]string",
	typeName: "[]string",
}, {
	name:     "map[string]string",
	typeName: "map[string]string",
}}

func TestTypeFromTypeName(t *testing.T) {
	for _, data := range typeFromTypeNameData {
		t.Run(data.typeName, func(st *testing.T) {
			ass := assert.New(st)
			ass.Equal(data.typeName,
				TypeFromTypeName(data.name).TypeName())
		})
	}
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// +build !dynplugin

package plugin

import (
	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// loadDynPlugins at runtime from plugin directory
// dummy method that do not load the plugins
func loadDynPlugins() (map[string]core.Plugin, error) {
	zap.L().Warn(assets.L.Get("Dynamic plugins not supported"))
	return make(map[string]core.Plugin), nil
}

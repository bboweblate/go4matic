// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package http

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"

	"gitlab.com/go4matic/go4matic/internal/version"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// apiSpec generates Open API specification
type apiSpec struct {
	handler *Handler
}

// registerAPISpec to handler
func registerAPISpec(handler *Handler) {
	spec := apiSpec{handler}
	handler.apiGroup.GET("/", spec.handleIndex)
	handler.apiGroup.GET("/spec.json", spec.handleSpec)
}

// handleIndex return swagger page for request
func (s *apiSpec) handleIndex(ctx *gin.Context) {
	ctx.Header("Content-Type", "text/html; charset=utf-8")
	ctx.String(http.StatusOK, `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>API Doc</title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.20.4/swagger-ui.css" >
    <style>
      html {
        box-sizing: border-box;
        overflow: -moz-scrollbars-vertical;
        overflow-y: scroll;
      }
      *,
      *:before,
      *:after {
        box-sizing: inherit;
      }
      body {
        margin:0;
        background: #fafafa;
      }
    </style>
  </head>

  <body>
    <div id="swagger-ui"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.20.4/swagger-ui-bundle.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.20.4/swagger-ui-standalone-preset.js"> </script>
    <script>
    function HideTopbarPlugin() {
      return { components: { Topbar: function() { return null } } }
    }

    window.onload = function() {
      const ui = SwaggerUIBundle({
        url: "./spec.json",
        dom_id: '#swagger-ui',
        deepLinking: true,
        presets: [
          SwaggerUIBundle.presets.apis,
          SwaggerUIStandalonePreset
        ],
        plugins: [
          SwaggerUIBundle.plugins.DownloadUrl,
          HideTopbarPlugin
        ],
        layout: "StandaloneLayout"
      })
      window.ui = ui
    }
  </script>
  </body>
</html>
`)
}

// handleSpec returns a open API 3.0 specification of the API
func (s *apiSpec) handleSpec(ctx *gin.Context) {
	ctx.IndentedJSON(http.StatusOK, map[string]interface{}{
		"openapi": "3.0.0",
		"info": map[string]interface{}{
			"title":   "GO Home API",
			"version": version.Version,
		},
		"paths": specEndpoints(
			getEndpoints("/api", s.handler.apiHandler)),
	})
}

// specEndpoints returns specification for the given path endpoints
func specEndpoints(endpoints map[string][]core.PathEntry) map[string]interface{} {
	pathList := make(map[string]interface{}, len(endpoints))

	for path, entries := range endpoints {
		operations := make(map[string]interface{}, len(entries))
		pathKeys := make(map[string]bool)

		for _, entry := range entries {
			data := map[string]interface{}{
				"summary": entry.Def.Description,
				"tags": []string{
					entry.Group,
				},
			}

			data["responses"] = specResponses(entry.Def.Responses)

			keys, parameters, body := specParameter(path, entry.Def.Parameters)
			for _, k := range keys {
				pathKeys[k] = true
			}
			if len(parameters) > 0 {
				data["parameters"] = parameters
			}
			if len(body) > 0 {
				data["requestBody"] = body
			}

			operations[strings.ToLower(entry.Method)] = data
		}

		// cleanup path
		for key := range pathKeys {
			path = strings.Replace(path, "/:"+key, "/{"+key+"}", 1)
		}
		path = strings.Replace(path, "//", "/", len(path))

		pathList[path] = operations
	}

	return pathList
}

// specParameter returns the parameter specification for given path
func specParameter(path string, parameterFields api.FieldMap) ([]string, []map[string]interface{}, map[string]interface{}) {
	pathKeys := make([]string, 0)
	pathParameters := make([]map[string]interface{}, 0)
	bodyParameters := make(api.FieldMap)

	for key, parameter := range parameterFields {
		if !strings.Contains(path, "/:"+key) {
			bodyParameters[key] = parameter
			continue
		}

		pathKeys = append(pathKeys, key)

		param := map[string]interface{}{
			"in":          "path",
			"name":        key,
			"required":    true,
			"description": parameter.Description,
		}

		if parameter.Type == nil || parameter.Type == api.Any {
			param["schema"] = map[string]interface{}{}
		} else {
			param["schema"] = map[string]interface{}{
				"type": parameter.Type.BaseName(),
			}
		}

		pathParameters = append(pathParameters, param)
	}

	if len(bodyParameters) == 0 {
		return pathKeys, pathParameters, map[string]interface{}{}
	}

	return pathKeys, pathParameters, map[string]interface{}{
		"required": true,
		"content": map[string]interface{}{
			"application/json": map[string]interface{}{
				"schema": specFieldMap(bodyParameters),
			},
		},
	}
}

// specResponses returns responses specification
func specResponses(responses map[string]api.Response) map[string]interface{} {
	if responses == nil {
		return map[string]interface{}{
			"200": map[string]interface{}{
				"description": "OK",
			},
		}
	}

	data := make(map[string]interface{}, len(responses))
	for key, response := range responses {
		d := map[string]interface{}{
			"description": response.Description,
		}
		if response.Data != nil {
			d["content"] = map[string]interface{}{
				"application/json": map[string]interface{}{
					"schema": specFieldMap(response.Data),
				},
			}
		}

		data[key] = d
	}
	return data
}

// specFieldMap returns specification for given fields
func specFieldMap(entries api.FieldMap) map[string]interface{} {
	properties := make(map[string]interface{}, len(entries))
	requiredProperties := make([]string, 0)

	var additionalProperties map[string]interface{}
	for key, entry := range entries {

		var entryData map[string]interface{}
		if entry.Type == nil || entry.Type == api.Any {
			entryData = map[string]interface{}{
				"description": entry.Description,
			}
		} else if fields, ok := entry.Type.(api.FieldMap); ok {
			fieldMap := specFieldMap(fields)
			fieldMap["description"] = entry.Description
			entryData = fieldMap
		} else {
			if entry.Type.BaseName() == "object" {
				entryData = map[string]interface{}{
					"description":          entry.Description,
					"type":                 "object",
					"additionalProperties": map[string]interface{}{},
				}
			} else {
				entryData = map[string]interface{}{
					"type":        entry.Type.BaseName(),
					"description": entry.Description,
				}
			}
		}

		if additionalProperties == nil &&
			strings.HasPrefix(key, "{") &&
			strings.HasSuffix(key, "}") {

			additionalProperties = entryData

		} else {
			properties[key] = entryData
			if entry.Required {
				requiredProperties = append(requiredProperties, key)
			}
		}
	}
	data := map[string]interface{}{
		"type": "object",
	}

	if len(properties) > 0 {
		data["properties"] = properties
	}
	if additionalProperties != nil {
		data["additionalProperties"] = additionalProperties
	}

	if len(requiredProperties) > 0 {
		data["required"] = requiredProperties
	}

	return data
}

// getEndpoints joins all path entries and returns map with paths and operations
func getEndpoints(base string, entries []core.PathEntry) map[string][]core.PathEntry {
	data := make(map[string][]core.PathEntry)
	for _, entry := range entries {
		if entry.Method == "" {
			entry.Method = "GET"
		}

		if entry.Group == "" {
			entry.Group = "Unknown"
		}

		if entry.SubEntries != nil {
			if entry.Handler != nil {
				addEndpointToMap(data, base+entry.Path, entry)
			}

			for p, e := range getEndpoints(base+entry.Path, entry.SubEntries) {
				addEndpointToMap(data, p, e...)
			}
		} else {
			addEndpointToMap(data, base+entry.Path, entry)
		}
	}
	return data
}

// addEndpointToMap and create slice if not already exist
func addEndpointToMap(data map[string][]core.PathEntry, path string, entries ...core.PathEntry) {
	if _, ok := data[path]; !ok {
		data[path] = make([]core.PathEntry, 0)
	}
	data[path] = append(data[path], entries...)
}

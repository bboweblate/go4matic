// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"errors"
	"fmt"
	"sort"

	"github.com/spf13/cast"

	"gitlab.com/go4matic/go4matic/assets"
)

// FieldMap type with sub fields
type FieldMap map[string]Field

// Convert given value to type if possible
func (t FieldMap) Convert(value interface{}) (interface{}, error) {
	_, err := t.Validate(cast.ToStringMap(value))
	return value, err
}

// Check if the value matches the type
func (t FieldMap) Check(value interface{}) bool {
	_, err := t.Validate(cast.ToStringMap(value))
	return err == nil
}

// TypeName return the name of the type
func (t FieldMap) TypeName() string {
	return fmt.Sprintf("FieldMap[%d]", len(t))
}

// BaseName return the name of the type category
func (t FieldMap) BaseName() string {
	return "object"
}

// SortedKeys returns list with sorted map keys (uses Order if set)
func (t FieldMap) SortedKeys() []string {
	keys := make([]string, 0, len(t))
	for key := range t {
		keys = append(keys, key)
	}

	sort.Slice(keys, func(i, j int) bool {
		f1 := t[keys[i]]
		f2 := t[keys[j]]
		if f1.Order == f2.Order {
			return keys[i] < keys[j]
		}
		return f1.Order < f2.Order
	})

	return keys
}

// Load data and fallback to default (ignores required flag)
func (t FieldMap) Load(data map[string]interface{}) map[string]interface{} {
	outData := make(map[string]interface{}, len(t))

	for name, field := range t {
		outData[name] = field.DefaultValue

		if val, ok := data[name]; ok {
			if field.Type == nil || field.Type.Check(val) {
				outData[name] = val
			}
		}
	}
	return outData
}

// Validate check if data matches with defined fields and add defaults to data
func (t FieldMap) Validate(data map[string]interface{}) (map[string]interface{}, error) {
	// return an valid map even if data is nil
	if data == nil {
		data = make(map[string]interface{})
	}

	var wildcard bool
	for name, field := range t {
		// wildcard fields
		if name[0] == '{' {
			wildcard = true
			continue
		}

		// check if value for this field was given
		value, ok := data[name]
		if !ok || value == nil {
			if field.Required {
				return nil, errors.New(assets.L.Get("required field \"%s\" missing", name))
			}
			if field.DefaultValue != nil {
				data[name] = field.DefaultValue
			}
			continue
		}

		// check value type
		if field.Type != nil {
			value, err := field.Type.Convert(value)
			if err != nil {
				return nil, errors.New(assets.L.Get("invalid value type for field \"%s\"", name))
			}
			data[name] = value
		}
	}

	// wildcard -> no check for unknown fields
	if wildcard {
		return data, nil
	}

	// check for unknown fields
	for name := range data {
		if _, ok := t[name]; !ok {
			return nil, errors.New(assets.L.Get("unknown field \"%s\" given", name))
		}
	}

	return data, nil
}

// FieldList type with list of fields
type FieldList []Field

// Convert given value to type if possible
func (t FieldList) Convert(value interface{}) (interface{}, error) {
	_, err := t.Validate(cast.ToSlice(value))
	return value, err
}

// Check if the value matches the type
func (t FieldList) Check(value interface{}) bool {
	_, err := t.Validate(cast.ToSlice(value))
	return err == nil
}

// TypeName return the name of the type
func (t FieldList) TypeName() string {
	return fmt.Sprintf("FieldList[%d]", len(t))
}

// BaseName return the name of the type category
func (t FieldList) BaseName() string {
	return "array"
}

// Validate check if data matches with defined fields and add defaults to data
func (t FieldList) Validate(data []interface{}) ([]interface{}, error) {
	// return an valid list even if data is nil
	if data == nil {
		data = make([]interface{}, 0)
	}

	if len(t) < len(data) {
		return nil, errors.New(assets.L.Get("expected %d fields but get %d", len(t), len(data)))
	}

	resp := make([]interface{}, len(t))
	for idx, field := range t {
		// check if value for this field was given
		if len(data) <= idx || data[idx] == nil {
			if field.Required {
				return nil, errors.New(assets.L.Get("required field %d missing", idx))
			}
			if field.DefaultValue != nil {
				resp[idx] = field.DefaultValue
			}
			continue
		}

		// check value type
		if field.Type != nil {
			value, err := field.Type.Convert(data[idx])
			if err != nil {
				return nil, errors.New(assets.L.Get("invalid value type for field %d", idx))
			}
			data[idx] = value
		}
		resp[idx] = data[idx]
	}

	return resp, nil
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

// MathNumber block
func MathNumber() Block {
	return Block{
		Type: "math_number",
		Fields: []Field{{
			Name: "NUM",
			Data: "0",
		}},
	}
}

// MathArithmetic block
func MathArithmetic() Block {
	return Block{
		Type: "math_arithmetic",
		Fields: []Field{{
			Name: "OP",
			Data: "ADD",
		}},
		Values: []Value{{
			Name: "A",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "1",
				},
			},
		}, {
			Name: "B",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "1",
				},
			},
		}},
	}
}

// MathSingle block
func MathSingle() Block {
	return Block{
		Type: "math_single",
		Fields: []Field{{
			Name: "OP",
			Data: "ROOT",
		}},
		Values: []Value{{
			Name: "NUM",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "9",
				},
			},
		}},
	}
}

// MathTrig block
func MathTrig() Block {
	return Block{
		Type: "math_trig",
		Fields: []Field{{
			Name: "OP",
			Data: "SIN",
		}},
		Values: []Value{{
			Name: "NUM",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "45",
				},
			},
		}},
	}
}

// MathConstant block
func MathConstant() Block {
	return Block{
		Type: "math_constant",
		Fields: []Field{{
			Name: "CONSTANT",
			Data: "PI",
		}},
	}
}

// MathNumberProperty block
func MathNumberProperty() Block {
	return Block{
		Type: "math_number_property",
		Mutations: []Mutation{{
			Attributes: map[string]interface{}{
				"divisor_input": false,
			},
		}},
		Fields: []Field{{
			Name: "PROPERTY",
			Data: "EVEN",
		}},
		Values: []Value{{
			Name: "NUMBER_TO_CHECK",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "0",
				},
			},
		}},
	}
}

// MathRound block
func MathRound() Block {
	return Block{
		Type: "math_round",
		Fields: []Field{{
			Name: "OP",
			Data: "ROUND",
		}},
		Values: []Value{{
			Name: "NUM",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "3.1",
				},
			},
		}},
	}
}

// MathOnList block
func MathOnList() Block {
	return Block{
		Type: "math_on_list",
		Mutations: []Mutation{{
			Attributes: map[string]interface{}{
				"op": "SUM",
			},
		}},
		Fields: []Field{{
			Name: "OP",
			Data: "SUM",
		}},
	}
}

// MathModulo block
func MathModulo() Block {
	return Block{
		Type: "math_modulo",
		Values: []Value{{
			Name: "DIVIDEND",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "64",
				},
			},
		}, {
			Name: "DIVISOR",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "10",
				},
			},
		}},
	}
}

// MathConstrain block
func MathConstrain() Block {
	return Block{
		Type: "math_constrain",
		Values: []Value{{
			Name: "VALUE",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "50",
				},
			},
		}, {
			Name: "LOW",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "1",
				},
			},
		}, {
			Name: "HIGH",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "100",
				},
			},
		}},
	}
}

// MathRandomInt block
func MathRandomInt() Block {
	return Block{
		Type: "math_random_int",
		Values: []Value{{
			Name: "FROM",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "1",
				},
			},
		}, {
			Name: "TO",
			Shadow: &Shadow{
				Type: "math_number",
				Field: Field{
					Name: "NUM",
					Data: "100",
				},
			},
		}},
	}
}

// MathRandomFloat block
func MathRandomFloat() Block {
	return Block{
		Type: "math_random_float",
	}
}

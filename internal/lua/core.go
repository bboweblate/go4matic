// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package lua

import (
	"sync"

	"go.uber.org/zap"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/pkg/api"
	"gitlab.com/go4matic/go4matic/pkg/core"

	"github.com/spf13/cast"
)

// CoreModule for basic functionality
type CoreModule struct {
	bus core.Bus

	// buffer for received events
	eventBuffer chan core.Event

	// map with registered callbacks
	eventCallbacks map[core.Script]map[string]func([]interface{}) []interface{}

	// channels to trigger callback
	callbackChannels map[core.Script]chan func()

	// mutex for eventCallbacks
	mutex sync.RWMutex
}

// CreateCoreModule instance and prepare event listener
func CreateCoreModule(bus core.Bus) *CoreModule {
	module := CoreModule{
		bus:              bus,
		eventBuffer:      make(chan core.Event, 10),
		eventCallbacks:   make(map[core.Script]map[string]func([]interface{}) []interface{}),
		callbackChannels: make(map[core.Script]chan func()),
	}

	go module.handleEvents()
	return &module
}

// Name of module
func (m *CoreModule) Name() string {
	return "core"
}

// Def return the definition of this module
func (m *CoreModule) Def() core.ScriptModuleDefinition {
	return core.ScriptModuleDefinition{
		Description: assets.L.Get("Core script functionality"),
		Functions: map[string]core.ScriptFunction{
			"register_listener": {
				Def: api.Function{
					Description: assets.L.Get("Register event listener"),
					Arguments: api.FieldMap{
						"id": {
							Description: assets.L.Get("Identifier for event"),
							Type:        api.String,
							Required:    true,
							Order:       1,
						},
						"handler": {
							Description: assets.L.Get("Callback for events"),
							Type: &api.Function{
								Arguments: api.FieldMap{
									"event_id": {
										Description: assets.L.Get("Identifier for event"),
										Type:        api.String,
										Required:    true,
										Order:       1,
									},
									"event_data": {
										Description: assets.L.Get("Data of event"),
										Type:        api.Map(api.String, api.Any),
										Required:    true,
										Order:       2,
									},
								},
							},
							Required: true,
							Order:    2,
						},
					},
					Response: api.FieldList{{
						Description: assets.L.Get("Error string if register failed"),
						Type:        api.Error,
					}},
				},
				Func: m.registerListener,
			},
			"log": {
				Def: api.Function{
					Description: assets.L.Get("Log message to console"),
					Arguments: api.FieldMap{
						"msg": {
							Description: assets.L.Get("Message to log"),
							Type:        api.String,
							Required:    true,
							Order:       1,
						},
					},
				},
				Func: m.log,
			},
		},
		CallbackChannelFunc: m.registerCallbackChannel,
		CleanupFunc:         m.cleanup,
	}
}

// cleanup removes channel and handler
func (m *CoreModule) cleanup(script core.Script) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	// TODO unregister event
	delete(m.callbackChannels, script)
	delete(m.eventCallbacks, script)
}

// sets channel to trigger callbacks
func (m *CoreModule) registerCallbackChannel(script core.Script, c chan func()) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	m.callbackChannels[script] = c
}

// handleEvents that are received
func (m *CoreModule) handleEvents() {
	for event := range m.eventBuffer {
		m.mutex.RLock()
		for script, callbacks := range m.eventCallbacks {
			callback, ok := callbacks[event.ID]
			if !ok {
				continue
			}

			ch, ok := m.callbackChannels[script]
			if !ok {
				continue
			}

			e := event
			ch <- func() {
				callback([]interface{}{e.ID, e.Data})
			}
		}
		m.mutex.RUnlock()
	}
}

// registerListener handler script function
func (m *CoreModule) registerListener(script core.Script, args []interface{}) []interface{} {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	id := cast.ToString(args[0])

	err := m.bus.RegisterEventListener(id, m.eventBuffer)
	if err != nil {
		return []interface{}{err}
	}

	f := args[1].(func([]interface{}) []interface{})

	if _, ok := m.eventCallbacks[script]; !ok {
		m.eventCallbacks[script] = map[string]func([]interface{}) []interface{}{
			id: f,
		}
	} else {
		m.eventCallbacks[script][id] = f
	}

	return nil
}

// log message to console
func (m *CoreModule) log(_ core.Script, args []interface{}) []interface{} {
	zap.L().Info(cast.ToString(args[0]))
	return nil
}

// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package core

import (
	"errors"
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"

	"gitlab.com/go4matic/go4matic/pkg/api"
)

// test data for TestIDSplit
var idSplitData = []struct {
	id       string
	instance string
	key      string
	action   string
}{
	{"aaa.bbb", "", "aaa.bbb", ""},
	{"aaa.bbb#action1", "", "aaa.bbb", "action1"},
	{"instance1:aaa.bbb", "instance1", "aaa.bbb", ""},
	{"instance1:aaa.bbb#action1", "instance1", "aaa.bbb", "action1"},
}

// TestIDSplit checks if splitID function
func TestIDSplit(t *testing.T) {
	for _, data := range idSplitData {
		t.Run(data.id, func(st *testing.T) {
			ass := assert.New(st)

			instance, key, action := splitID(data.id)

			ass.Equal(data.instance, instance)
			ass.Equal(data.key, key)
			ass.Equal(data.action, action)
		})
	}
}

func TestBus_callHandler(t *testing.T) {
	ass := assert.New(t)

	request := Request{
		ID:              "test",
		ResponseChannel: make(chan Response, 1),
	}

	handler := RequestHandler{
		Def: api.SimpleHandler{},
		Func: func(map[string]interface{}) (map[string]interface{}, error) {
			panic("test")
		},
	}

	sh := new(serviceHelper)
	sh.callHandler(request, handler)

	response := <-request.ResponseChannel
	ass.EqualError(response.Error, "failed to handle request")
}

// TestBusSuite executes BusTestSuite
func TestBusSuite(t *testing.T) {
	suite.Run(t, new(BusTestSuite))
}

// counter to detect service init and shutdown order
var mutex sync.Mutex
var initCounter int
var shutdownCounter int
var errTest = errors.New("test")

// TestService is a mock for a service implementation that is used by BusTestSuite
type TestService struct {
	mock.Mock
	trigger EventTrigger

	// data of last request
	lastData map[string]interface{}

	// time to wait in request (simulate some long running action)
	requestSleep time.Duration

	// index for detection of init and shutdown order (low = first)
	initCall     int // should increase
	shutdownCall int // should decrease
	updateCall   int // should increase
	startCall    int // should increase
}

func (s *TestService) UpdateCall() int {
	mutex.Lock()
	defer mutex.Unlock()
	return s.updateCall
}
func (s *TestService) LastData() map[string]interface{} {
	mutex.Lock()
	defer mutex.Unlock()
	return s.lastData
}

func (s *TestService) Key() string {
	return s.Called().String(0)
}
func (s *TestService) Handlers() map[string]RequestHandler {
	return map[string]RequestHandler{
		// dummy action that will response with the parameters as data
		// -> additional if err is set as parameter this will be returned as error
		"test": {
			Func: s.testFunc,
			Def: api.SimpleHandler{
				Parameters: api.FieldMap{
					"err":  {},
					"test": {},
				},
			},
		},
	}
}
func (s *TestService) testFunc(data map[string]interface{}) (map[string]interface{}, error) {
	mutex.Lock()
	defer mutex.Unlock()

	time.Sleep(s.requestSleep)
	s.lastData = data
	if data != nil {
		if err, ok := data["err"]; ok && err != nil {
			return data, fmt.Errorf("%v", err)
		}
	}
	return data, nil
}

func (s *TestService) Events() map[string]api.SimpleHandler {
	return map[string]api.SimpleHandler{}
}
func (s *TestService) SetEventTrigger(trigger EventTrigger) {
	s.trigger = trigger
}
func (s *TestService) Trigger(action string, data map[string]interface{}) {
	s.trigger(action, data)
}
func (s *TestService) Init(bus Bus) error {
	mutex.Lock()
	defer mutex.Unlock()

	// count up and store start index
	initCounter++
	s.initCall = initCounter

	return s.Called().Error(0)
}
func (s *TestService) Start(bus Bus) error {
	mutex.Lock()
	defer mutex.Unlock()

	s.startCall++

	return s.Called().Error(0)
}
func (s *TestService) Update(bus Bus) {
	mutex.Lock()
	defer mutex.Unlock()
	s.updateCall++
}
func (s *TestService) UpdateInterval() time.Duration {
	return time.Millisecond * 10
}
func (s *TestService) Shutdown(bus Bus) {
	mutex.Lock()
	defer mutex.Unlock()

	// count up and store shutdown index
	shutdownCounter++
	s.shutdownCall = shutdownCounter
}

type BusTestSuite struct {
	suite.Suite

	service *TestService
	bus     Bus
}

func (s *BusTestSuite) SetupTest() {
	// reset counter
	initCounter = 0
	shutdownCounter = 0

	// create a new bus instance
	s.bus = CreateBus()

	// crate a test service
	s.service = new(TestService)
	events := s.service.Handlers()
	_ = events
	s.service.On("Key").Return("aaa.bbb")
	s.service.On("Init").Return(nil)
	s.service.On("Start").Return(nil)
}
func (s *BusTestSuite) TearDownTest() {
	// try some cleanup
	s.bus.Shutdown()
}
func (s *BusTestSuite) TestRegisterService() {
	s.Equal(0, s.service.initCall) // ensure service was not started before
	s.NoError(s.bus.RegisterService(s.service))
	s.Equal(1, s.service.initCall)

	// second register must fail
	s.Error(s.bus.RegisterService(s.service))

	// create a second service
	service2 := new(TestService)
	service2.On("Key").Return("aaa.ccc")
	service2.On("Init").Return(nil)
	service2.On("Start").Return(nil)

	s.Equal(0, service2.initCall) // ensure service was not started before
	s.NoError(s.bus.RegisterService(service2))
	s.Equal(2, service2.initCall) // index one higher compared to first service

	// check start (no error)
	s.NoError(s.bus.Start())
	s.Equal(1, s.service.startCall)
	s.Equal(1, service2.startCall)

	// create a third service
	service3 := new(TestService)
	service3.On("Key").Return("aaa.ddd")
	service3.On("Init").Return(errTest)
	service3.On("Start").Return(errTest)
	s.Equal(errTest, s.bus.RegisterService(service3))

	// check start (with error)
	s.Equal(errTest, s.bus.Start())
}

// test data for TestRequestHandle
var requestHandleData = []struct {
	name string
	id   string
	data map[string]interface{}
	err  bool
}{
	{"empty", "aaa.bbb#test", map[string]interface{}{}, false},
	{"unknown_action", "aaa.bbb#test2", map[string]interface{}{}, true},
	{"unknown_action2", "aaa.bbb", map[string]interface{}{}, true},
	{"unknown_service", "aaa.ccc#test", map[string]interface{}{}, true},
	{"unknown_field", "aaa.bbb#test", map[string]interface{}{"test2": 42}, true},
	{"with_data", "aaa.bbb#test", map[string]interface{}{"test": 42}, false},
	{"with_error_response", "aaa.bbb#test", map[string]interface{}{"err": "test"}, true},
}

func (s *BusTestSuite) TestRequestHandle() {
	s.NoError(s.bus.RegisterService(s.service))

	request := Request{
		ResponseChannel: make(chan Response, 2),
	}

	for _, data := range requestHandleData {
		s.T().Run(data.name, func(st *testing.T) {
			ass := assert.New(st)

			request.ID = data.id
			request.Parameters = data.data

			// send request and get response
			s.bus.HandleRequest(request)
			response := <-request.ResponseChannel

			// response must contain original request
			ass.Equal(request, response.OrgRequest)

			if data.err {
				ass.Error(response.Error)
			} else {
				ass.NoError(response.Error)
				ass.Equal(data.data, response.Data)
			}
		})
	}
}
func (s *BusTestSuite) TestRequestHandleWait() {
	s.NoError(s.bus.RegisterService(s.service))

	request := Request{
		ID: "aaa.bbb#test",
	}

	response := s.bus.HandleRequestWait(request, time.Millisecond*10)
	s.Equal(request.ID, response.OrgRequest.ID)
	s.NoError(response.Error)

	// slow down service
	s.service.requestSleep = time.Millisecond * 100

	response = s.bus.HandleRequestWait(request, time.Millisecond*10)
	s.Equal(request.ID, response.OrgRequest.ID)
	s.Errorf(response.Error, "timeout for response reached")
}
func (s *BusTestSuite) TestRequestHandleNoRespChan() {
	s.NoError(s.bus.RegisterService(s.service))

	request := Request{
		ID: "aaa.bbb#test",
		Parameters: map[string]interface{}{
			"test": 42,
		},
	}

	s.bus.HandleRequest(request)

	// give the bus some time to process
	time.Sleep(time.Millisecond * 10)

	s.Equal(request.Parameters, s.service.LastData())
}
func (s *BusTestSuite) TestRequestHandleRespChanTimeout() {
	s.NoError(s.bus.RegisterService(s.service))

	request := Request{
		ID: "aaa.bbb#test",
		Parameters: map[string]interface{}{
			"test": 42,
		},
		ResponseChannel: make(chan Response),
	}

	s.bus.HandleRequest(request)

	// give the bus some time to process
	time.Sleep(time.Millisecond * 5)

	select {
	case <-request.ResponseChannel:
		s.Fail("Response was received -> timeout was not reached")
	default:
		// timeout of response delivery reached -> response dropped by bus
	}
}
func (s *BusTestSuite) TestRequestHandleBufferFull() {
	s.NoError(s.bus.RegisterService(s.service))
	// slow down service
	s.service.requestSleep = time.Millisecond * 100

	request := Request{
		ID: "aaa.bbb#test",
		Parameters: map[string]interface{}{
			"test": 42,
		},
		ResponseChannel: make(chan Response),
	}
	request2 := Request{
		ID: "aaa.bbb#test",
		Parameters: map[string]interface{}{
			"test": 42,
		},
		ResponseChannel: make(chan Response, 1),
	}

	// fill service with requests
	for i := 0; i < 101; i++ {
		s.bus.HandleRequest(request)
	}
	// buffer is full now -> service can not accept new request
	s.bus.HandleRequest(request2)

	response := <-request2.ResponseChannel

	s.Error(response.Error)
	s.Equal("service aaa.bbb is busy", response.Error.Error())
}
func (s *BusTestSuite) TestEventListenerRegister() {
	listener1 := make(chan Event, 1)
	listener2 := make(chan Event, 1)

	s.NoError(s.bus.RegisterService(s.service))

	// first register
	s.NoError(s.bus.RegisterEventListener("aaa.bbb#test", listener1))

	bus := s.bus.(*bus)

	// register the same -> ignored
	s.NoError(s.bus.RegisterEventListener("aaa.bbb#test", listener1))
	s.Len(bus.eventListeners["aaa.bbb#test"], 1)

	// register second listener
	s.NoError(s.bus.RegisterEventListener("aaa.bbb#test", listener2))
	s.Len(bus.eventListeners["aaa.bbb#test"], 2)

	// register second listener for different event
	s.NoError(s.bus.RegisterEventListener("aaa.bbb#test2", listener2))
	s.Len(bus.eventListeners["aaa.bbb#test2"], 1)

	// UNREGISTER

	// unregister second listener for different event
	s.bus.UnregisterEventListener("aaa.bbb#test2", listener2)
	s.Len(bus.eventListeners["aaa.bbb#test2"], 0)

	// unregister second listener
	s.bus.UnregisterEventListener("aaa.bbb#test", listener2)
	s.Len(bus.eventListeners["aaa.bbb#test"], 1)

	// unregister unknown listener
	s.bus.UnregisterEventListener("aaa.bbb#test3", listener1)
	s.Len(bus.eventListeners["aaa.bbb#test3"], 0)

	// unregister unknown event
	s.bus.UnregisterEventListener("aaa.ccc#test", listener1)
}

func (s *BusTestSuite) TestEventTrigger() {
	s.NoError(s.bus.RegisterService(s.service))

	listener11 := make(chan Event, 2)
	listener12 := make(chan Event, 1)
	listener13 := make(chan Event)   // blocking/full listener
	listener2 := make(chan Event, 1) // other event ID

	s.NoError(s.bus.RegisterEventListener("aaa.bbb#test", listener11))
	s.NoError(s.bus.RegisterEventListener("aaa.bbb#test", listener12))
	s.NoError(s.bus.RegisterEventListener("aaa.bbb#test", listener13))
	s.NoError(s.bus.RegisterEventListener("aaa.bbb#test2", listener2))

	s.service.Trigger("aaa.bbb#tes3", map[string]interface{}{"test": 42})
	s.service.Trigger("aaa.bbb#test", map[string]interface{}{"test": 42})

	// give the bus some time to process
	time.Sleep(time.Millisecond * 5)

	select {
	case event := <-listener11:
		s.Equal("aaa.bbb#test", event.ID)
		s.Equal(map[string]interface{}{"test": 42}, event.Data)
	default:
		s.Fail("No event received")
	}

	select {
	case <-listener11:
		s.Fail("Received second event but only one triggered")
	default:
		// no event received
	}

	select {
	case event := <-listener12:
		s.Equal("aaa.bbb#test", event.ID)
		s.Equal(map[string]interface{}{"test": 42}, event.Data)
	default:
		s.Fail("No event received")
	}

	select {
	case <-listener2:
		s.Fail("Received event on listener for different ID")
	default:
		// no event received
	}
}

func (s *BusTestSuite) TestShutdownService() {
	s.NoError(s.bus.RegisterService(s.service))

	service2 := new(TestService)
	service2.On("Key").Return("aaa.ccc")
	service2.On("Init").Return(nil)
	s.NoError(s.bus.RegisterService(service2))

	s.Equal(0, s.service.shutdownCall)
	s.Equal(0, service2.shutdownCall)
	s.bus.Shutdown()

	// shutdown in reverse registration order
	s.Equal(2, s.service.shutdownCall)
	s.Equal(1, service2.shutdownCall)
}

func (s *BusTestSuite) TestAfterShutdownService() {
	s.NoError(s.bus.RegisterService(s.service))
	s.bus.Shutdown()
	s.Equal(1, s.service.shutdownCall)

	// register service after shutdown -> fail
	service2 := new(TestService)
	service2.On("Key").Return("aaa.ccc")
	service2.On("Init").Return(nil)
	s.Error(s.bus.RegisterService(service2))

	// handle request after service is stopped
	request := Request{
		ID:              "aaa.bbb#test",
		ResponseChannel: make(chan Response, 1),
	}
	s.bus.HandleRequest(request)
	response := <-request.ResponseChannel
	s.Error(response.Error)
}

func (s *BusTestSuite) TestUpdateService() {
	s.NoError(s.bus.RegisterService(s.service))
	time.Sleep(time.Millisecond * 100)
	s.True(s.service.UpdateCall() > 5)
}

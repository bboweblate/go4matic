// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package admin

import (
	"fmt"
	"time"

	"gitlab.com/go4matic/go4matic/assets"
	"gitlab.com/go4matic/go4matic/internal/web/helper"
	"gitlab.com/go4matic/go4matic/pkg/core"
)

// handle post on plugin page
func (w *Web) adminPluginPost(ctx core.HTTPContext) {
	if ctx.PostForm("action") == "remove_entry" {
		response := w.Bus.HandleRequestWait(core.Request{
			ID: "core.plugins#config_entry_remove",
			Parameters: map[string]interface{}{
				"plugin_id":  ctx.Param("id"),
				"entry_key":  ctx.PostForm("entry_key"),
				"entry_type": ctx.PostForm("entry_type"),
			},
		}, time.Second)

		if response.Error != nil {
			helper.AddMessage(ctx, "danger", response.Error.Error())
		}
		return
	}

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.plugins#get_info",
		Parameters: map[string]interface{}{
			"plugin_id": ctx.Param("id"),
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	config, ok := response.Data["config"]
	if !ok {
		return
	}

	configDef := config.(*core.PluginConfigDef)
	if configDef.Config == nil {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("no configuration for this plugin"))
	}

	data, err := helper.FieldFormValues(ctx, configDef.Config)
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	data["plugin_id"] = ctx.Param("id")

	response = w.Bus.HandleRequestWait(core.Request{
		ID:         "core.plugins#config_change",
		Parameters: data,
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
	}
}

// render plugin page
func (w *Web) adminPlugin(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/plugin")
	ctx.Set("title", "Plugins/Admin - Go4Matic")

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.plugins#get_info",
		Parameters: map[string]interface{}{
			"plugin_id": ctx.Param("id"),
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}
	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["plugin"] = response.Data

	config, ok := response.Data["config"]
	if !ok {
		return
	}

	configDef := config.(*core.PluginConfigDef)
	if configDef.Config != nil {
		response := w.Bus.HandleRequestWait(core.Request{
			ID: "core.plugins#config_get",
			Parameters: map[string]interface{}{
				"plugin_id": ctx.Param("id"),
			},
		}, time.Second)

		if response.Error != nil {
			helper.AddMessage(ctx, "danger", response.Error.Error())
			return
		}

		configForm, err := helper.FieldForm(configDef.Config, response.Data)
		if err != nil {
			helper.AddMessage(ctx, "danger", err.Error())
			return
		}

		vars["configForm"] = configForm
	}

	vars["entries_def"] = configDef.Entries

	entries := make(map[string]interface{}, len(configDef.Entries))
	for key := range configDef.Entries {
		response := w.Bus.HandleRequestWait(core.Request{
			ID: "core.plugins#config_entry_list",
			Parameters: map[string]interface{}{
				"plugin_id":  ctx.Param("id"),
				"entry_type": key,
			},
		}, time.Second)

		if response.Error != nil {
			helper.AddMessage(ctx, "danger", response.Error.Error())
			return
		}

		entries[key] = response.Data
	}

	vars["entries"] = entries
	var f func(key string, config map[string]interface{}) error
	vars["nil_func"] = f
}

// handle post on add plugin page
func (w *Web) adminPluginAddPost(ctx core.HTTPContext) {
	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.plugins#get_info",
		Parameters: map[string]interface{}{
			"plugin_id": ctx.Param("id"),
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	config, ok := response.Data["config"]
	if !ok {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("no configuration for plugin"))
		return
	}

	configDef := config.(*core.PluginConfigDef)
	entryDef, ok := configDef.Entries[ctx.Param("entry_type")]
	if !ok {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("no configuration entry for %s", ctx.Param("entry_type")))
		return
	}

	data, err := helper.FieldFormValues(ctx, entryDef.Config)
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	data["plugin_id"] = ctx.Param("id")
	data["entry_key"] = ctx.PostForm("entry_key")
	data["entry_type"] = ctx.Param("entry_type")

	response = w.Bus.HandleRequestWait(core.Request{
		ID:         "core.plugins#config_entry_add",
		Parameters: data,
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}
	ctx.Redirect(302, "..")
	ctx.Abort()
}

// render add plugin page
func (w *Web) adminPluginAdd(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/plugin_add")
	ctx.Set("title", fmt.Sprintf("%s/%s/Plugins/Admin - Go4Matic",
		ctx.Param("id"), ctx.Param("entry_type")))

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.plugins#get_info",
		Parameters: map[string]interface{}{
			"plugin_id": ctx.Param("id"),
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}
	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["plugin"] = response.Data
	vars["entry_type"] = ctx.Param("entry_type")

	config, ok := response.Data["config"]
	if !ok {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("no configuration for plugin"))
		return
	}

	configDef := config.(*core.PluginConfigDef)
	entryDef, ok := configDef.Entries[ctx.Param("entry_type")]
	if !ok {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("no configuration entry for %s", ctx.Param("entry_type")))
		return
	}

	entryForm, err := helper.FieldForm(entryDef.Config, nil)
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}
	vars["entry_form"] = entryForm
}

// handle post on edit plugin page
func (w *Web) adminPluginEditPost(ctx core.HTTPContext) {
	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.plugins#get_info",
		Parameters: map[string]interface{}{
			"plugin_id": ctx.Param("id"),
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	config, ok := response.Data["config"]
	if !ok {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("no configuration for plugin"))
		return
	}

	configDef := config.(*core.PluginConfigDef)
	entryDef, ok := configDef.Entries[ctx.Param("entry_type")]
	if !ok {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("no configuration entry for %s", ctx.Param("entry_type")))
		return
	}

	data, err := helper.FieldFormValues(ctx, entryDef.Config)
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}

	data["plugin_id"] = ctx.Param("id")
	data["entry_type"] = ctx.Param("entry_type")
	data["entry_key"] = ctx.Param("key")

	response = w.Bus.HandleRequestWait(core.Request{
		ID:         "core.plugins#config_entry_change",
		Parameters: data,
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}
	ctx.Redirect(302, "../..")
	ctx.Abort()
}

// render edit plugin page
func (w *Web) adminPluginEdit(ctx core.HTTPContext) {
	ctx.Set("tpl", "admin/plugin_edit")
	ctx.Set("title", fmt.Sprintf("%s/%s/Plugins/Admin - Go4Matic",
		ctx.Param("id"), ctx.Param("entry_type")))

	response := w.Bus.HandleRequestWait(core.Request{
		ID: "core.plugins#get_info",
		Parameters: map[string]interface{}{
			"plugin_id": ctx.Param("id"),
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}
	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["plugin"] = response.Data
	entryType := ctx.Param("entry_type")
	vars["entry_type"] = entryType

	config, ok := response.Data["config"]
	if !ok {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("no configuration for plugin"))
		return
	}

	response = w.Bus.HandleRequestWait(core.Request{
		ID: "core.plugins#config_entry_get",
		Parameters: map[string]interface{}{
			"plugin_id":  ctx.Param("id"),
			"entry_type": entryType,
			"entry_key":  ctx.Param("key"),
		},
	}, time.Second)

	if response.Error != nil {
		helper.AddMessage(ctx, "danger", response.Error.Error())
		return
	}

	configDef := config.(*core.PluginConfigDef)
	entryDef, ok := configDef.Entries[entryType]
	if !ok {
		helper.AddMessage(ctx, "danger",
			assets.L.Get("no configuration entry for %s", entryType))
		return
	}

	entryForm, err := helper.FieldForm(entryDef.Config, response.Data)
	if err != nil {
		helper.AddMessage(ctx, "danger", err.Error())
		return
	}
	vars["entry_form"] = entryForm
}

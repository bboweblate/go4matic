// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package toolbox

import "testing"

func TestMathNumber(t *testing.T) {
	equalXML(t, `<block type="math_number">
  <field name="NUM">0</field>
</block>`, MathNumber())
}

func TestMathArithmetic(t *testing.T) {
	equalXML(t, `<block type="math_arithmetic">
  <field name="OP">ADD</field>
  <value name="A">
    <shadow type="math_number">
      <field name="NUM">1</field>
    </shadow>
  </value>
  <value name="B">
    <shadow type="math_number">
      <field name="NUM">1</field>
    </shadow>
  </value>
</block>`, MathArithmetic())
}

func TestMathSingle(t *testing.T) {
	equalXML(t, `<block type="math_single">
  <field name="OP">ROOT</field>
  <value name="NUM">
    <shadow type="math_number">
      <field name="NUM">9</field>
    </shadow>
  </value>
</block>`, MathSingle())
}

func TestMathTrig(t *testing.T) {
	equalXML(t, `<block type="math_trig">
  <field name="OP">SIN</field>
  <value name="NUM">
    <shadow type="math_number">
      <field name="NUM">45</field>
    </shadow>
  </value>
</block>`, MathTrig())
}

func TestMathConstant(t *testing.T) {
	equalXML(t, `<block type="math_constant">
  <field name="CONSTANT">PI</field>
</block>`, MathConstant())
}

func TestMathNumberProperty(t *testing.T) {
	equalXML(t, `<block type="math_number_property">
  <mutation divisor_input="false"></mutation>
  <field name="PROPERTY">EVEN</field>
  <value name="NUMBER_TO_CHECK">
    <shadow type="math_number">
      <field name="NUM">0</field>
    </shadow>
  </value>
</block>`, MathNumberProperty())
}

func TestMathRound(t *testing.T) {
	equalXML(t, `<block type="math_round">
  <field name="OP">ROUND</field>
  <value name="NUM">
    <shadow type="math_number">
      <field name="NUM">3.1</field>
    </shadow>
  </value>
</block>`, MathRound())
}

func TestMathOnList(t *testing.T) {
	equalXML(t, `<block type="math_on_list">
  <mutation op="SUM"></mutation>
  <field name="OP">SUM</field>
</block>`, MathOnList())
}

func TestMathModulo(t *testing.T) {
	equalXML(t, `<block type="math_modulo">
  <value name="DIVIDEND">
    <shadow type="math_number">
      <field name="NUM">64</field>
    </shadow>
  </value>
  <value name="DIVISOR">
    <shadow type="math_number">
      <field name="NUM">10</field>
    </shadow>
  </value>
</block>`, MathModulo())
}

func TestMathConstrain(t *testing.T) {
	equalXML(t, `<block type="math_constrain">
  <value name="VALUE">
    <shadow type="math_number">
      <field name="NUM">50</field>
    </shadow>
  </value>
  <value name="LOW">
    <shadow type="math_number">
      <field name="NUM">1</field>
    </shadow>
  </value>
  <value name="HIGH">
    <shadow type="math_number">
      <field name="NUM">100</field>
    </shadow>
  </value>
</block>`, MathConstrain())
}

func TestMathRandomInt(t *testing.T) {
	equalXML(t, `<block type="math_random_int">
  <value name="FROM">
    <shadow type="math_number">
      <field name="NUM">1</field>
    </shadow>
  </value>
  <value name="TO">
    <shadow type="math_number">
      <field name="NUM">100</field>
    </shadow>
  </value>
</block>`, MathRandomInt())
}

func TestMathRandomFloat(t *testing.T) {
	equalXML(t, `<block type="math_random_float"></block>`, MathRandomFloat())
}

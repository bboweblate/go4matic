// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package blockly

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/go4matic/go4matic/internal/web/blockly/toolbox"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

// BlockDef defines toolbox and JS for a single block
type BlockDef struct {
	name      string
	def       map[string]interface{}
	generator []string
	toolbox   toolbox.Block
	msgIdx    uint
}

// NewBlock creates a block definition from an api function
func NewBlock(name string, funcDef api.Function, moduleName string) (*BlockDef, error) {
	// only functions with one response
	if len(funcDef.Response) > 1 {
		return nil, errors.New("functions with more then one response are not supported")
	}

	def := BlockDef{
		name: moduleName + "__" + name,
		def: map[string]interface{}{
			"type":         moduleName + "__" + name,
			"inputsInline": false,
			"colour":       230,
			"tooltip":      funcDef.Description,
		},
		generator: make([]string, 0),
		toolbox: toolbox.Block{
			Type:   moduleName + "__" + name,
			Fields: make([]toolbox.Field, 0),
			Values: make([]toolbox.Value, 0),
		},
	}

	// add module import
	def.addModule(moduleName)

	// add description of function as first line in block
	def.addArg(fmt.Sprintf("%s %%1", funcDef.Description),
		Arg{Type: "input_dummy"})

	// add function arguments
	parameters := make([]string, len(funcDef.Arguments))
	for idx, key := range funcDef.Arguments.SortedKeys() {
		arg := funcDef.Arguments[key]
		if arg.Type != nil && arg.Type.TypeName() == "func" {
			def.addFunction(key, arg.Description, arg.Type.(*api.Function))
		} else {
			def.addArg(fmt.Sprintf("%s %%1", arg.Description), Arg{
				Type:      "input_value",
				Name:      key,
				Align:     "RIGHT",
				ValueType: arg.Type,
			})
			def.addJs("var " + key + " = Blockly.Lua.valueToCode(block, '" + key + "', Blockly.Lua.ORDER_ATOMIC);")
		}
		parameters[idx] = key
	}

	// add function call
	var functionParam string
	if len(parameters) > 0 {
		functionParam = strings.Join(parameters, "+','+")
	} else {
		functionParam = "''"
	}
	functionCall := "moduleName + '." + name + "('+" + functionParam + "+')'"
	def.addResponse(functionCall, funcDef.Response)
	return &def, nil
}

// addJs line for code generator
func (b *BlockDef) addJs(line ...string) {
	b.generator = append(b.generator, line...)
}

// addModule import (use provideFunction_ to only import once per script)
func (b *BlockDef) addModule(name string) {
	b.addJs("var moduleName = Blockly.Lua.provideFunction_('" + name + "_module',")
	b.addJs("  ['local ' + Blockly.Lua.FUNCTION_NAME_PLACEHOLDER_ + ' = require(\"" + name + "\")']);")
}

// Arg entry of block definition
type Arg struct {
	Type      string   `json:"type"`
	Name      string   `json:"name,omitempty"`
	Align     string   `json:"align,omitempty"`
	ValueType api.Type `json:"-"`
}

// addArg to block definition
func (b *BlockDef) addArg(msg string, args ...Arg) {
	for _, arg := range args {
		switch arg.Type {
		case "input_value":
			var shadow *toolbox.Shadow
			if arg.ValueType != nil {
				switch arg.ValueType.BaseName() {
				case "string":
					shadow = &toolbox.Shadow{
						Type: "text",
						Field: toolbox.Field{
							Name: "TEXT",
						},
					}
				case "integer", "number":
					shadow = &toolbox.Shadow{
						Type: "math_number",
						Field: toolbox.Field{
							Name: "NUM",
						},
					}
				}
			}

			b.toolbox.Values = append(b.toolbox.Values, toolbox.Value{
				Name:   arg.Name,
				Shadow: shadow,
			})
		case "field_variable":
			b.toolbox.Fields = append(b.toolbox.Fields, toolbox.Field{
				Name: arg.Name,
				Data: arg.Name,
			})
		}
	}
	b.def[fmt.Sprintf("args%d", b.msgIdx)] = args
	b.def[fmt.Sprintf("message%d", b.msgIdx)] = msg
	b.msgIdx++
}

// addFunction parameter to block definition
func (b *BlockDef) addFunction(name, desc string, funcDef *api.Function) {
	args := make([]Arg, len(funcDef.Arguments))

	var variables string
	var parameters string
	for fIdx, k := range funcDef.Arguments.SortedKeys() {
		if variables == "" {
			variables = fmt.Sprintf("%%%d", fIdx+1)
			parameters = name + "_" + k
		} else {
			variables += fmt.Sprintf(" %%%d", fIdx+1)
			parameters += "+','+" + name + "_" + k
		}
		args[fIdx] = Arg{
			Type:  "field_variable",
			Name:  k,
			Align: "RIGHT",
		}
		b.addJs("var " + name + "_" + k + " = Blockly.Lua.variableDB_.getName(block.getFieldValue('" + k + "'), Blockly.VARIABLE_CATEGORY_NAME);")
	}

	b.addArg(fmt.Sprintf("Variables: %s", variables), args...)
	b.addArg(fmt.Sprintf("%s %%1", desc),
		Arg{
			Type:  "input_statement",
			Name:  name,
			Align: "RIGHT",
		})

	b.addJs("var statements_" + name + " = Blockly.Lua.statementToCode(block,'" + name + "');")
	b.addJs("var " + name + " = 'function('+" + parameters + "+')\\n' + statements_" + name + " + 'end';")
}

// addErrorHandler for function call
func (b *BlockDef) addErrorHandler(functionCall string) {
	b.addJs("var error_handler_error = Blockly.Lua.variableDB_.getName(block.getFieldValue('error'), Blockly.VARIABLE_CATEGORY_NAME);")
	b.addArg("Variables: %1",
		Arg{
			Type:  "field_variable",
			Name:  "error",
			Align: "RIGHT",
		})
	b.addArg("On Error %1",
		Arg{
			Type:  "input_statement",
			Name:  "error_handler",
			Align: "RIGHT",
		})
	b.addJs("var statements_error_handler = Blockly.Lua.statementToCode(block,'error_handler');")
	b.addJs("if (statements_error_handler == '') {")
	b.addJs("  return " + functionCall + " + '\\n';")
	b.addJs("} else {")
	b.addJs("  var code = error_handler_error + ' = ' + " + functionCall + " + '\\n';")
	b.addJs("  code += 'if '+error_handler_error+' ~= nil then\\n' + statements_error_handler +'end\\n';")
	b.addJs("  return code;")
	b.addJs("}")
}

// addResponse of function call to generator
func (b *BlockDef) addResponse(functionCall string, response api.FieldList) {
	if len(response) == 0 {
		b.def["previousStatement"] = nil
		b.def["nextStatement"] = nil
		b.addJs("return " + functionCall + "+'\\n';")

	} else if response[0].Type == api.Error {
		b.def["previousStatement"] = nil
		b.def["nextStatement"] = nil
		b.addErrorHandler(functionCall)

	} else {
		if response[0].Type == api.String {
			b.def["output"] = "String"
		} else {
			b.def["output"] = nil
		}
		b.addJs("return [" + functionCall + ", Blockly.Lua.ORDER_ATOMIC];")
	}
}

// Toolbox returns toolbox entry for block
func (b *BlockDef) Toolbox() toolbox.Block {
	return b.toolbox
}

// JSON returns block definition
func (b *BlockDef) JSON() map[string]interface{} {
	return b.def
}

// JS returns code generator for block
func (b *BlockDef) JS() string {
	return "Blockly.Lua['" + b.name + "'] = function(block) {\n" +
		"  " + strings.Join(b.generator, "\n  ") + "\n};\n"
}

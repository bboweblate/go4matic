// Copyright 2019 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package blockly

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/go4matic/go4matic/internal/web/blockly/toolbox"
	"gitlab.com/go4matic/go4matic/pkg/api"
)

func TestNewModule(t *testing.T) {
	mod := NewModule("module", map[string]api.Function{
		"aaa": {
			Description: "aaa_desc",
		},
		"bbb": {
			Description: "bbb_desc",
			Response:    api.FieldList{{}, {}},
		},
	})
	assert.Equal(t, "module", mod.name)
	assert.Len(t, mod.blocks, 1)
}

func TestModuleDef_Toolbox(t *testing.T) {
	mod := ModuleDef{
		name: "module",
		blocks: []*BlockDef{{
			toolbox: toolbox.Block{
				Type: "aaa",
			},
		}, {
			toolbox: toolbox.Block{
				Type: "bbb",
			},
		}},
	}
	assert.Equal(t, toolbox.Category{
		Name:  "module",
		Color: "#aaaaaa",
		Entries: []interface{}{
			mod.blocks[0].Toolbox(),
			mod.blocks[1].Toolbox(),
		},
	}, mod.Toolbox())
}

func TestModuleDef_JSON(t *testing.T) {
	mod := ModuleDef{
		name: "module",
		blocks: []*BlockDef{{
			def: map[string]interface{}{
				"aaa": 111,
			},
		}, {
			def: map[string]interface{}{
				"bbb": 222,
			},
		}},
	}
	assert.Equal(t, []interface{}{
		map[string]interface{}{
			"aaa": 111,
		},
		map[string]interface{}{
			"bbb": 222,
		},
	}, mod.JSON())
}

func TestModuleDef_JS(t *testing.T) {
	mod := ModuleDef{
		name: "module",
		blocks: []*BlockDef{{
			name: "aaa",
			generator: []string{
				"bbb",
			},
		}, {
			name: "xxx",
			generator: []string{
				"zzz",
			},
		}},
	}
	assert.Equal(t, `Blockly.Lua['aaa'] = function(block) {
  bbb
};
Blockly.Lua['xxx'] = function(block) {
  zzz
};
`, mod.JS())
}

func TestModuleDefList_Add(t *testing.T) {
	mods := make(ModuleDefList, 0)
	assert.Len(t, mods, 0)
	mods.Add(new(ModuleDef))
	mods.Add(new(ModuleDef))
	assert.Len(t, mods, 2)
}

func TestModuleDefList_ExtendedDefaultToolbox(t *testing.T) {
	mods := ModuleDefList{
		&ModuleDef{
			name: "mod_a",
			blocks: []*BlockDef{{
				toolbox: toolbox.Block{
					Type: "aaa",
				},
			}},
		},
		&ModuleDef{
			name: "mod_b",
			blocks: []*BlockDef{{
				toolbox: toolbox.Block{
					Type: "bbb",
				},
			}},
		},
	}

	tb := toolbox.DefaultToolbox()
	tb.Entries = append(tb.Entries,
		new(toolbox.Separator),
		toolbox.Category{
			Name:  "mod_a",
			Color: "#aaaaaa",
			Entries: []interface{}{
				toolbox.Block{
					Type: "aaa",
				},
			},
		},
		toolbox.Category{
			Name:  "mod_b",
			Color: "#aaaaaa",
			Entries: []interface{}{
				toolbox.Block{
					Type: "bbb",
				},
			},
		})
	assert.Equal(t, tb, mods.ExtendedDefaultToolbox())
}

func TestModuleDefList_BlockJSON(t *testing.T) {
	mods := ModuleDefList{
		&ModuleDef{
			name: "mod_a",
			blocks: []*BlockDef{{
				def: map[string]interface{}{
					"aaa": 111,
				},
			}},
		},
		&ModuleDef{
			name: "mod_b",
			blocks: []*BlockDef{{
				def: map[string]interface{}{
					"bbb": 222,
				},
			}},
		},
	}

	assert.Equal(t, []interface{}{
		map[string]interface{}{
			"aaa": 111,
		},
		map[string]interface{}{
			"bbb": 222,
		},
	}, mods.BlockJSON())
}

func TestModuleDefList_BlockJS(t *testing.T) {
	mods := ModuleDefList{
		&ModuleDef{
			name: "mod_a",
			blocks: []*BlockDef{{
				name: "aaa",
				def: map[string]interface{}{
					"aaa": 111,
				},
				generator: []string{
					"xxx",
				},
			}},
		},
		&ModuleDef{
			name: "mod_b",
			blocks: []*BlockDef{{
				name: "bbb",
				def: map[string]interface{}{
					"bbb": 222,
				},
				generator: []string{
					"zzz",
				},
			}},
		},
	}

	data, err := mods.BlockJS()
	assert.NoError(t, err)
	assert.Equal(t, `Blockly.defineBlocksWithJsonArray([
  {
    "aaa": 111
  },
  {
    "bbb": 222
  }
]);
Blockly.Lua['aaa'] = function(block) {
  xxx
};
Blockly.Lua['bbb'] = function(block) {
  zzz
};
`, data)
}
